This model is free and you can use it for your projects.

This model has been exported to cryengine.
We've gave the folder: "cryengine_ready".
You must place it in:
root(cryengine folder)/gamesdk(or your game folder)

If you'll not use this model for cryengine, we're recommending to use the
.max file too which placing in the "cryengine_ready\objects\kits\street\traffic_lights\t1.max".

Best regards.

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DDSim.Settings.Enums;
using DDSim.Settings.Base;
using System.Linq;
using Vexe.Runtime.Types;

namespace DDSim.Settings
{
    [Serializable]
    public class IntersectionBuilderSettings : BaseScriptableObject
    {
        public IntersectionSettings DefaultIntersection;
        public List<IntersectionSettings> SettingsIntersections;

        public IntersectionSettings GetSettingsFor(Enum type)
        {
            var f = SettingsIntersections.FirstOrDefault(x => x.ForRoadType == (RoadType)type);
            return f ?? DefaultIntersection;
        }

        public IntersectionSettings GetSettingsFor(string type)
        {
            var f = SettingsIntersections.FirstOrDefault(x => "\"" + x.ForRoadType.ToDescriptionString() + "\"" == type);
            return f ?? DefaultIntersection;
        }

        public bool HasSettingsFor(Enum type)
        {
            return SettingsIntersections.Any(x => x.ForRoadType == (RoadType)type);
        }
    }

    [Serializable]
    public class IntersectionSettings : BaseRandomResourceSetting<ResourceAndPositioning>
    {
        public RoadType ForRoadType;
    }

    [Serializable]
    public struct ResourceAndPositioning
    {
        public GameObject Obj;
        public float ProbFrontLeft;
        public float ProbFrontRight;
        public float ProbBackLeft;
        public float ProbBackRight;
    }
}

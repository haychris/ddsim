﻿using UnityEngine;
using System.Collections;
using System;
using Vexe.Runtime.Types;
using DDSim.Settings.Base;
using DDSim.Settings.Enums;
using System.Collections.Generic;
using System.Linq;

namespace DDSim.Settings
{
    [Serializable]
    public class RoadNeighborFactorySettings : BaseScriptableObject
    {
        public RoadNeighborSettings DefaultRoadNeighbor;
        public List<RoadNeighborSettings> SettingsRoadNeighbors;

        public RoadNeighborSettings GetSettingsFor(Enum type)
        {
            var f = SettingsRoadNeighbors.FirstOrDefault(x => x.ForRoadType == (RoadType)type);
            return f ?? DefaultRoadNeighbor;
        }

        public RoadNeighborSettings GetSettingsFor(string type)
        {
            var f = SettingsRoadNeighbors.FirstOrDefault(x => "\"" + x.ForRoadType.ToDescriptionString() + "\"" == type);
            return f ?? DefaultRoadNeighbor;
        }

        public bool HasSettingsFor(Enum type)
        {
            return SettingsRoadNeighbors.Any(x => x.ForRoadType == (RoadType)type);
        }
    }

    [Serializable]
    public class RoadNeighborSettings : BaseRandomResourceSetting<Material>
    {
        public RoadType ForRoadType;
        public List<ResourceAndSpawnSettings> SpawnableObjects;

        public ResourceAndSpawnSettings GetRandomObjects()
        {
            throw new NotImplementedException();
        }
    }
    // TODO: FIGURE BETTER WAY TO ORGANIZE SETTINGS
    [Serializable]
    public struct ResourceAndSpawnSettings
    {
        public float SpawnProb;
        public float MaxPerMeter;
        public RoadSideObject Obj;
    }

    [Serializable]
    public struct RoadSideObject
    {
        public GameObject Obj;
        public bool FacesRoad;
        public float MinDistance;
        public float MaxDistance;
    }
}

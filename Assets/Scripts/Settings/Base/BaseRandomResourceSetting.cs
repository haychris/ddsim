﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Vexe.Runtime.Extensions;
using Vexe.Runtime.Helpers;
using Vexe.Runtime.Types;

namespace DDSim.Settings.Base
{
    // TODO: refactor to make single base random material/prefab setting class
    [Serializable]
    public abstract class BaseRandomResourceSetting<T> 
    {
        public List<T> Resources;
        public List<float> ResourceWeights;

        //private float totalWeight;
        //[Serialize] public List<BaseResource<T>> Resources;


        //public List<ItemTuple<T, float>> Test;

        //private List<BaseResource<T>> resources;
        //public List<BaseResource<T>> Resources
        //{
        //    get { return resources; }
        //    set { resources = value; totalWeight = resources.Select(r => r.Weight).Sum(); }
        //}

        //public void AddResource(T resource, float weight)
        //{
        //    BaseResource<T> br = new BaseResource<T>(resource, weight);
        //    //br.Resource = resource;
        //    //br.Weight = weight;

        //    this.resources.Add(br);
        //    this.totalWeight += weight;
        //}

        public T GetRandomResource()
        {
            float totalWeight = ResourceWeights.Sum();
            //float totalWeight = Resources.Select(r => r.Weight).Sum();
            float randomNum = UnityEngine.Random.Range(0f, totalWeight);

            float cumProb = 0f;
            int i;
            for (i = 0; cumProb <= randomNum; i++)
            {
                cumProb += ResourceWeights[i];
            }
            return Resources[i - 1];
        }

        //[Serializable]
        //public struct BaseResource
        //{
        //    [SerializeField]
        //    public T Resource;
        //    [SerializeField]
        //    public float Weight;

        //    public BaseResource(T resource, float weight)
        //    {
        //        this.Resource = resource;
        //        this.Weight = weight;
        //    }

        //    public override string ToString()
        //    {
        //        string classname = typeof(T).Name;
        //        string resourceStr = (Resource != null) ? Resource.ToString() : "null";
        //        string weightStr = Weight.ToString();
        //        return string.Format("{0}: {1}, Weight: {2}", classname, resourceStr, weightStr);
        //    }

        //    public override int GetHashCode()
        //    {
        //        return RuntimeHelper.CombineHashCodes(Resource, Weight);
        //    }

        //    public override bool Equals(object obj)
        //    {
        //        if (obj == null || obj.GetType() != typeof(BaseResource))
        //            return false;

        //        return (BaseResource)obj == this;
        //    }

        //    public static bool operator ==(BaseResource left, BaseResource right)
        //    {
        //        return left.Resource.GenericEquals(right.Resource) && left.Weight.GenericEquals(right.Weight);
        //    }

        //    public static bool operator !=(BaseResource left, BaseResource right)
        //    {
        //        return !(left == right);
        //    }
        //}
    }

    [Serializable]
    public struct BaseResource<T>
    {
        [Serialize]
        public T Resource;
        [Serialize]
        public float Weight;

        public BaseResource(T resource, float weight)
        {
            this.Resource = resource;
            this.Weight = weight;
        }

        public override string ToString()
        {
            string classname = typeof(T).Name;
            string resourceStr = (Resource != null) ? Resource.ToString() : "null";
            string weightStr = Weight.ToString();
            return string.Format("{0}: {1}, Weight: {2}", classname, resourceStr, weightStr);
        }

        public override int GetHashCode()
        {
            return RuntimeHelper.CombineHashCodes(Resource, Weight);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != typeof(BaseResource<T>))
                return false;

            return (BaseResource<T>)obj == this;
        }

        public static bool operator ==(BaseResource<T> left, BaseResource<T> right)
        {
            return left.Resource.GenericEquals(right.Resource) && left.Weight.GenericEquals(right.Weight);
        }

        public static bool operator !=(BaseResource<T> left, BaseResource<T> right)
        {
            return !(left == right);
        }
    }

    //public static class BaseResource
    //{
    //    public static BaseResource<T> Create<T>(T resource, float weight)
    //    {
    //        return new BaseResource<T>(resource, weight);
    //    }
    //}

    //[Serializable]
    //public class RandomMaterialSetting : BaseRandomResourceSetting<Material> { };
    //[Serializable]
    //public class RandomGameobjectSetting : BaseRandomResourceSetting<GameObject> { };
}
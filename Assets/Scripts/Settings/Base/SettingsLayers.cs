﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DDSim.Settings.Enums;
using SerializableCollections;
using UnityEngine;
using Vexe.Runtime.Types;

namespace DDSim.Settings.Base
{
    [System.Serializable]
    public abstract class SettingsLayers : BaseScriptableObject
    {
        public virtual T GetSettingsFor<T>(Enum type) where T : BaseRandomResourceSetting<Material>
        {
            return null;
        }

        public virtual T GetSettingsFor<T>(string type) where T : BaseRandomResourceSetting<Material>
        {
            return null;
        }

        public virtual bool HasSettingsFor(Enum type)
        {
            return false;
        }

        public static T GetScriptableObject<T>() where T : SettingsLayers
        {
            return Resources.Load<T>("Settings/" + (typeof(T).ToString()));
        }

    }

    public class SettingsLayersDictionary : SerializableCollections.SerializableDictionary<Enum, BaseRandomResourceSetting<Material>> { };
}
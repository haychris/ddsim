﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDSim.Settings.Enums;
using DDSim.Settings.Base;
using UnityEngine;

namespace DDSim.Settings
{
    public class BuildingFactorySettings : SettingsLayers
    {
        public BuildingSettings DefaultBuilding = new BuildingSettings();
        public List<BuildingSettings> SettingsBuildings;


        public override BuildingSettings GetSettingsFor<BuildingSettings>(Enum type)
        {
            if ((BuildingType)type == BuildingType.Unknown)
                return DefaultBuilding as BuildingSettings;
            return SettingsBuildings.FirstOrDefault(x => x.Type == (BuildingType)type) as BuildingSettings ?? DefaultBuilding as BuildingSettings;
        }

        public override BuildingSettings GetSettingsFor<BuildingSettings>(string type)
        {
            var f = SettingsBuildings.FirstOrDefault(x => "\"" + x.Type.ToDescriptionString() + "\"" == type);
            return f as BuildingSettings ?? DefaultBuilding as BuildingSettings;
        }

        public override bool HasSettingsFor(Enum type)
        {
            return SettingsBuildings.Any(x => x.Type == (BuildingType)type);
        }
    }
    [System.Serializable]
    public class BuildingSettings : BaseRandomResourceSetting<Material>
    {
        public BuildingType Type;
        public int MinimumBuildingHeight = 2;
        public int MaximumBuildingHeight = 5;
        public bool IsVolumetric = true;
    }
}
﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DDSim.Settings.Enums;
using DDSim.Settings.Base;
using System.Linq;
using Vexe.Runtime.Types;

namespace DDSim.Settings
{
    [Serializable]
    public class VehicleFactorySettings : BaseScriptableObject
    {
        public int NumRecordingVehicles = 1;
        public float PercentFramesToRecord = 0.2f;

        public float SpawnProbabilityPerSqMeter = 0.005f;
        public string SensorDataRelativePath = "../SensorData/{0}/{1}/";

        public VehicleSettings RecordingVehicleSettings;
        public VehicleSettings NormalVehicleSettings;
    }

    [Serializable]
    public class VehicleSettings : BaseRandomResourceSetting<GameObject>
    {

        // TODO: should we do this here, or in a vehicle builder?
        public Vehicle GetRandomVehicle(bool instantiate = false)
        {
            GameObject go = GetRandomResource();
            if (instantiate)
            {
                go = UnityEngine.Object.Instantiate(go, Vector3.zero, Quaternion.identity);
            }

            if (go.GetComponent<Vehicle>() == null)
            {
                return go.AddComponent<Vehicle>();
            }
        
            return go.GetComponent<Vehicle>();
        }   
    }
}

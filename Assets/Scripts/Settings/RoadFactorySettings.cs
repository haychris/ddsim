﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using UnityEngine;
//using DDSim.Settings.Base;
//using DDSim.Settings.Enums;

//namespace DDSim.Settings
//{
//    public class RoadFactorySettings : SettingsLayers
//    {
//        public RoadSettings DefaultRoad;
//        public List<RoadSettings> SettingsRoad;

//        public RoadFactorySettings()
//        {
//            DefaultRoad = new RoadSettings()
//            {
//                Material = null,
//                Type = RoadType.Path,
//                Width = 3
//            };
//            SettingsRoad = new List<RoadSettings>();
//        }

//        public override RoadSettings GetSettingsFor<RoadSettings>(Enum type)
//        {
//            var f = SettingsRoad.FirstOrDefault(x => x.Type == (RoadType)type);
//            return f as RoadSettings ?? DefaultRoad as RoadSettings;
//        }
//        //private bool help(RoadSettings rs, string type)
//        //{
//        //    string rsType = rs.Type.ToDescriptionString();
//        //    Debug.Log(type + " " + rsType);
//        //    return type == rsType || type.Replace("\"", "") == rsType;
//        //}
//        public override RoadSettings GetSettingsFor<RoadSettings>(string type)
//        {
//            //var f = SettingsRoad.FirstOrDefault(x => help(x, type));
//            var f = SettingsRoad.FirstOrDefault(x => "\""+x.Type.ToDescriptionString()+"\"" == type);
//            return f as RoadSettings ?? DefaultRoad as RoadSettings;
//        }

//        public override bool HasSettingsFor(Enum type)
//        {
//            return SettingsRoad.Any(x => x.Type == (RoadType)type);
//        }
//    }

//    [Serializable]
//    public class RoadSettings : BaseSetting
//    {
//        public RoadType Type;
//        public Material Material;
//        public float Width = 6;
//    }
//}

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DDSim.Settings.Enums;
using DDSim.Settings.Base;
using System.Linq;
using Vexe.Runtime.Types;

namespace DDSim.Settings
{
    [Serializable]
    public class RoadFactorySettings : SettingsLayers
    {
        public RoadSettings DefaultRoad;
        public List<RoadSettings> SettingsRoad;

        public override RoadSettings GetSettingsFor<RoadSettings>(Enum type)
        {
            var f = SettingsRoad.FirstOrDefault(x => x.Type == (RoadType)type);
            return f as RoadSettings ?? DefaultRoad as RoadSettings;
        }

        public override RoadSettings GetSettingsFor<RoadSettings>(string type)
        {
            var f = SettingsRoad.FirstOrDefault(x => "\"" + x.Type.ToDescriptionString() + "\"" == type);
            return f as RoadSettings ?? DefaultRoad as RoadSettings;
        }

        public override bool HasSettingsFor(Enum type)
        {
            return SettingsRoad.Any(x => x.Type == (RoadType)type);
        }
    }

    [Serializable]
    public class RoadSettings : BaseRandomResourceSetting<Material>
    {
        public RoadType Type;
        public bool Driveable = true;
        public float Width = 5;
    }
}
﻿
using System.ComponentModel;

namespace DDSim.Settings.Enums
{
    public enum RoadType
    {
        [Description("unknown")] Unknown,
        [Description("aerialway")] Aerialway,
        [Description("exit")] Exit,
        [Description("ferry")] Ferry,
        [Description("highway")] Highway,
        [Description("major_road")] Major_Road,
        [Description("minor_road")] Minor_Road,
        [Description("path")] Path,
        [Description("piste")] Piste,
        [Description("racetrack")] Racetrack,
        [Description("rail")] Rail,
    }

    public static class RoadTypeExtensions
    {
        public static string ToDescriptionString(this RoadType val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
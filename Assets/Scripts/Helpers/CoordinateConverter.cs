﻿using System;
using UnityEngine;

namespace DDSim.Helpers
{
    public class CoordinateConverter : MonoBehaviour
    {
        private const int TileSize = 256;
        private const int EARTH_RADIUS = 6378137;
        private const double InitialResolution = 2 * Math.PI * EARTH_RADIUS / TileSize;
        //private const float OriginShift = 2 * Mathf.PI * EarthRadius / 2;

        public static double Deg2Rad(double d)
        {
            return d * Math.PI / 180.0;
        }

        public static double Rad2Deg(double r)
        {
            return r * 180.0 / Math.PI;
        }

        //Converts given lat/lon in WGS84 Datum to XY in Spherical Mercator EPSG:900913
        public static Vector2d LatLon2Mercator(double lat, double lon)
        {
            double x = Deg2Rad(lon) * EARTH_RADIUS;
            double y = Math.Log(Math.Tan(Deg2Rad(lat) / 2 + Math.PI / 4)) * EARTH_RADIUS;
            return new Vector2d((float) x, (float) y);
        }

        public static Vector2 Mercator2LatLon(Vector2d m)
        {
            double lat = Rad2Deg(2 * Math.Atan(Math.Exp(m.y / EARTH_RADIUS)) - Math.PI / 2);
            double lon = Rad2Deg(m.x / EARTH_RADIUS);
            return new Vector2((float) lon, (float) lat);
        }

        //Converts pixel coordinates in given zoom level of pyramid to EPSG:900913
        public static Vector2d Pixels2Mercator(Pair<double> p, int zoom)
        {
            double res = Resolution(zoom);
            double x = (p.First * res - Mathf.PI * EARTH_RADIUS);
            double y = -(p.Second * res - Mathf.PI * EARTH_RADIUS);
            return new Vector2d((float) x, (float) y);
        }

        //Converts EPSG:900913 to pyramid pixel coordinates in given zoom level
        public static Pair<double> Mercator2Pixels(Vector2d m, int zoom)
        {
            double res = Resolution(zoom);
            double x = ((m.x + Mathf.PI * EARTH_RADIUS) / res);
            double y = ((-m.y + Mathf.PI * EARTH_RADIUS) / res);
            return new Pair<double>(x, y);
        }

        //Returns a TMS (NOT Google!) tile covering region in given pixel coordinates
        public static Pair<int> Pixels2TMS(Pair<double> p)
        {
            int x = (int) Math.Ceiling(p.First / TileSize) - 1;
            int y = (int) Math.Ceiling(p.Second / TileSize) - 1;
            return new Pair<int>(x, y);
        }

        //Returns tile for given mercator coordinates
        public static Pair<int> Mercator2TMS(Vector2d m, int zoom)
        {
            Pair<double> p = Mercator2Pixels(m, zoom);
            return Pixels2TMS(p);
        }

        public static Vector2 Pixels2Raster(Vector2 p, int zoom)
        {
            var mapSize = TileSize << zoom;
            return new Vector2(p.x, mapSize - p.y);
        }

        public static Pair<int> LatLon2TMS(Vector2 p, int zoom)
        {
            Vector2d m = LatLon2Mercator(p.x, p.y);
            Pair<int> tms = Mercator2TMS(m, zoom);
            return tms;
        }

        //Returns bounds of the given tile in EPSG:900913 coordinates
        public static RectD TMSBoundsInMercator(Pair<int> t, int zoom)
        {
            Vector2d min = Pixels2Mercator(new Pair<double>(t.First * TileSize, t.Second * TileSize), zoom);
            Vector2d max = Pixels2Mercator(new Pair<double>((t.First + 1) * TileSize, (t.Second + 1) * TileSize), zoom);
            return new RectD(min, max - min);
        }

        public static Vector2 TMS2MercatorOffset(Pair<int> p, Pair<int> origin, int zoom)
        {
            Vector2d v = TMSBoundsInMercator(p, zoom).center - TMSBoundsInMercator(origin, zoom).center;
            return new Vector2((float)v.x, (float)v.y);
        }

        //Returns bounds of the given tile in latutude/longitude using WGS84 datum
        //public static RectD TileLatLonBounds(Vector2d t, int zoom)
        //{
        //    var bound = TileBounds(t, zoom);
        //    var min = MetersToLatLon(new Vector2d(bound.Min.x, bound.Min.y));
        //    var max = MetersToLatLon(new Vector2d(bound.Min.x + bound.Size.x, bound.Min.y + bound.Size.y));
        //    return new RectD(min.x, min.y, Math.Abs(max.x - min.x), Math.Abs(max.y - min.y));
        //}

        //Resolution (meters/pixel) for given zoom level (measured at Equator)
        public static double Resolution(int zoom)
        {
            return InitialResolution / (Mathf.Pow(2, zoom));
        }

        public static float ZoomForPixelSize(float pixelSize)
        {
            for (var i = 0; i < 30; i++)
            {
                if (pixelSize > Resolution(i))
                {
                    return i != 0 ? i - 1 : 0;
                }
            }
            throw new InvalidOperationException();
        }

        private static Pair<int> flipY(Pair<int> v, int zoom)
        {
            int y2 = ((int) Mathf.Pow(2, zoom)) - v.Second - 1;
            return new Pair<int>(v.First, y2);
        }

        // Switch to Google Tile representation from TMS
        public static Pair<int> TMS2Google(Pair<int> t, int zoom)
        {
            return flipY(t, zoom);
        }

        // Switch to TMS Tile representation from Google
        public static Pair<int> Google2TMS(Pair<int> t, int zoom)
        {
            return flipY(t, zoom);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class ReloadAfterTime : MonoBehaviour
{

    public float SecondsUntilReload;
    public string ResetKey = "r";

    public bool IsTicking;
    private float elapsedTime;

    // Use this for initialization
    void Start()
    {
    }

    public void StartTimer()
    {
        IsTicking = true;
        elapsedTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(ResetKey))
        {
            Debug.Log("reset key was pressed. Reloading...", this);
            resetScene();
        }
        else if (elapsedTime > SecondsUntilReload)
        {
            Debug.Log("TIME'S UP! It's been " + elapsedTime + " seconds. Reloading...", this);
            resetScene();
        }
        else if (IsTicking)
        {
            elapsedTime += Time.deltaTime;
        }
    }

    private void resetScene()
    {
        Scene scene = SceneManager.GetActiveScene();
        List<GameObject> rootObjects = new List<GameObject>();
        scene.GetRootGameObjects(rootObjects);

        foreach (GameObject go in rootObjects)
        {
            Destroy(go);
        }
        SceneManager.LoadScene(scene.name);
    }
}

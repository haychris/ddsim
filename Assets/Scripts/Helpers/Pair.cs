﻿public class Pair<T>
{
    public Pair() { }
    public Pair(T first, T second)
    {
        First = first;
        Second = second;
    }
    public T First { get; set; }
    public T Second { get; set; }
    public override string ToString()
    {
        return "[" + First + ", " + Second + "]";
    }
}


public class Pair<T, E>
{
    public Pair() { }
    public Pair(T first, E second)
    {
        First = first;
        Second = second;
    }
    public T First { get; set; }
    public E Second { get; set; }
    public override string ToString()
    {
        return "[" + First + ", " + Second + "]";
    }
}
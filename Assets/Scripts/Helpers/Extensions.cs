﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Extensions 
{

    public static TValue GetData<TKey,TValue>(this Dictionary<TKey, TValue> d, TKey key)
    {
        TValue data;
        bool success = d.TryGetValue(key, out data);
        if (success)
        {
            return data;
        }
        else
        {
            throw new ArgumentException("Key not found: " + key);
        }
    }

    public static string PrettyString<T>(this IEnumerable<T> xs)
    {
        return "[" + string.Join(", ", xs.Select(x => x.ToString()).ToArray()) + "]";
    }

    public static float SignedFlatAngle(this Vector3 v1, Vector3 v2)
    {
        Vector2 a = new Vector2(v1.x, v1.z);
        Vector2 b = new Vector2(v2.x, v2.z);
        float sign = Vector3.Cross(a, b).z < 0 ? -1f : 1f;
        return sign * Vector2.Angle(a, b);
        //float sign = Vector3.Cross(v1, v2).y < 0 ? -1f : 1f;
        //return sign * Vector3.Angle(v1, v2);
    }

    /// Determine the signed angle between two vectors, with normal 'n'
    /// as the rotation axis.
    public static float AngleSigned(this Vector3 v1, Vector3 v2, Vector3 n)
    {
        return Mathf.Atan2(
            Vector3.Dot(n, Vector3.Cross(v1, v2)),
            Vector3.Dot(v1, v2)) * Mathf.Rad2Deg;
    }

    public static Bounds GetAllBounds(this GameObject go)
    {
        Quaternion rot = go.transform.rotation;
        go.transform.rotation = Quaternion.identity; // need to do this to get non-axis aligned bounds
        Bounds bounds = new List<Renderer> { go.GetComponent<Renderer>() }
                .Union(go.GetComponentsInChildren<Renderer>())
                .Where(rend => rend != null)
                .Select(rend => rend.bounds)
                .Where(bound => bound.center != Vector3.zero) // was giving erroneously large bounds without this
                .Aggregate((b1, b2) =>
                {
                    b1.Encapsulate(b2);
                    return b1;
                });
        go.transform.rotation = rot;
        return bounds;
    }

    public static bool ContainsPoint(this Vector2[] polyPoints, Vector2 p)
    {
        int j = polyPoints.Length - 1;
        bool inside = false;
        for (int i = 0; i < polyPoints.Length; j = i++)
        {
            if (((polyPoints[i].y <= p.y && p.y < polyPoints[j].y) || (polyPoints[j].y <= p.y && p.y < polyPoints[i].y)) &&
               (p.x < (polyPoints[j].x - polyPoints[i].x) * (p.y - polyPoints[i].y) / (polyPoints[j].y - polyPoints[i].y) + polyPoints[i].x))
                inside = !inside;
        }
        return inside;
    }

    // TODO: move somewhere more appropriate
    public static IEnumerable<Vector2> Convert2Vector2(this IEnumerable<Vector3> ps)
    {
        return ps.Select(p => new Vector2(p.x, p.z));
    }

    public static bool ContainsXZPoint(this Mesh mesh, Vector2 p)
    {
        Vector3[] verts = mesh.vertices;
        Vector2[] ps = new Vector2[verts.Length];
        int count = 0;
        for (int i = 0; i < verts.Length; i += 2)
        {
            ps[count] = new Vector2(verts[i].x, verts[i].z);
            ps[verts.Length - count - 1] = new Vector2(verts[i + 1].x, verts[i + 1].z);
            count++;
        }
        return ps.ContainsPoint(p);
    }
}
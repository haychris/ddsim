﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DDSim.Helpers;
using DDSim.Settings;
using DDSim.Settings.Enums;
using System.Linq;

namespace DDSim
{
    public class RoadFactory : BaseMapzenFactory
    {
        public bool DebugMode;

        [SerializeField] protected RoadFactorySettings RoadFactorySettings;
        [SerializeField] protected IntersectionBuilderSettings IntersectionSettings;

        private RoadNetwork roadNetwork;
        //private VehicleFactory vehicleFactory;

        private void Start()
        {
            this.roadNetwork = new RoadNetwork(IntersectionSettings, this.DebugMode);

            FindObjectOfType<FleetManager>().SetRoadNetwork(this.roadNetwork);
        }

        public override string MapzenLayer()
        {
            return "roads";
        }

        private bool isSingleRoad(JSONObject data)
        {
            return data["geometry"]["type"].str == "LineString";
        }

        private bool isMultiRoad(JSONObject data)
        {
            return data["geometry"]["type"].str == "MultiLineString";
        }

        protected override bool Makeable(JSONObject data)
        {
            return isSingleRoad(data) || isMultiRoad(data);
        }

        protected override IEnumerable<MonoBehaviour> Make(Tile tile, JSONObject data)
        {
            if (isSingleRoad(data))
            {
                return MakeSingleRoad(tile, data);
            }
            else if (isMultiRoad(data))
            {
                return MakeMultiRoad(tile, data);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private string getUniqueKey(Tile tile, JSONObject data)
        {
            return tile.Tms.First + "_" + tile.Tms.Second + "-" + data["properties"]["id"].ToString();
        }

        private IEnumerable<MonoBehaviour> MakeSingleRoad(Tile tile, JSONObject data)
        {
            RoadSettings typeSettings = RoadFactorySettings.GetSettingsFor<RoadSettings>(data["properties"]["kind"].ToString());

            RoadBuilder rBuilder = new RoadBuilder(true, this.DebugMode)
                .SetName("road-" + getUniqueKey(tile, data))
                .SetDriveable(typeSettings.Driveable)
                .SetDescription(data.ToString())
                .SetWidth(typeSettings.Width, true)
                .SetMaterial(typeSettings.GetRandomResource())
                .SetProperties(data["properties"])
                .SetType(typeSettings.Type)
                .SetTile(tile);
            for (var i = 0; i < data["geometry"]["coordinates"].list.Count; i++)
            {
                JSONObject c = data["geometry"]["coordinates"][i];
                Vector2d p = CoordinateConverter.LatLon2Mercator(c[1].n, c[0].n);
                Vector3d localp = new Vector3d(p, true) - tile.TileCenter;
                rBuilder.AddPoint((Vector3)localp);
            }
            Road road = rBuilder.Build(true);
            this.roadNetwork.AddRoad(road);
            
            yield return road;
        }

        private IEnumerable<MonoBehaviour> MakeMultiRoad(Tile tile, JSONObject data)
        {
            RoadSettings typeSettings = RoadFactorySettings.GetSettingsFor<RoadSettings>(data["properties"]["kind"].ToString());

            RoadBuilder rBuilder = new RoadBuilder(true, this.DebugMode)
                .SetDriveable(typeSettings.Driveable)
                .SetWidth(typeSettings.Width, true)
                .SetMaterial(typeSettings.GetRandomResource())
                .SetProperties(data["properties"])
                .SetType(typeSettings.Type)
                .SetTile(tile);
            for (var i = 0; i < data["geometry"]["coordinates"].list.Count; i++)
            {
                JSONObject c = data["geometry"]["coordinates"][i];
                RoadBuilder rb = rBuilder.Clone()
                    .SetName("road-" + getUniqueKey(tile, data) + "-" + i)
                    .SetDescription(c.ToString());
                for (var j = 0; j < c.list.Count; j++)
                {
                    JSONObject seg = c[j];
                    Vector2d p = CoordinateConverter.LatLon2Mercator(seg[1].n, seg[0].n);
                    Vector3d localp = new Vector3d(p, true) - tile.TileCenter;
                    rb.AddPoint((Vector3)localp);
                }
                Road road = rb.Build(true);
                this.roadNetwork.AddRoad(road);
                
                yield return road;
            }
        }


        public override void PostProcess(Tile tile)
        {
            IEnumerable waits = this.roadNetwork.ProcessTile(tile);
            foreach (var wait in waits) { } // TODO: fix hacks
        }

        public override IEnumerable<MonoBehaviour> GetFactoryOutput(Tile tile)
        {
            return roadNetwork.GetRoadsForTile(tile).Where(r => r.Tile == tile).Cast<MonoBehaviour>();
        }
    }
}

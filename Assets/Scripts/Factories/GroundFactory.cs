﻿using UnityEngine;
using System.Collections;
using System;
using UniRx;
using System.Collections.Generic;

namespace DDSim
{
    public class GroundFactory : BaseFactory
    {
        public enum TileServices
        {
            Default,
            Satellite,
            Terrain,
            Toner,
            Watercolor,
            PerlinNoise,
            UniformNoise
        }

        public TileServices TileService = TileServices.Default;
        public int TextureWidth = 256;
        public int TextureHeight = 256;
        public TextureFormat FormatForTexture = TextureFormat.ARGB4444;
        public Material BaseMaterial;

        private string[] TileServiceUrls = new string[] {
            "http://b.tile.openstreetmap.org/",
            "http://b.tile.openstreetmap.us/usgs_large_scale/",
            "http://tile.stamen.com/terrain-background/",
            "http://a.tile.stamen.com/toner/",
            "https://stamen-tiles.a.ssl.fastly.net/watercolor/"
        };

        public override void Process(Tile tile)
        {
            GameObject curMap = GameObject.CreatePrimitive(PrimitiveType.Quad);
            curMap.name = "ground";
            curMap.transform.SetParent(tile.transform, false);
            curMap.transform.localScale = new Vector3((float)tile.Rect.Width, (float)tile.Rect.Width, 1);
            curMap.transform.rotation = Quaternion.AngleAxis(90, new Vector3(1, 0, 0));
            curMap.transform.localPosition = new Vector3(0, -0.01f, 0);
            Renderer rend = curMap.transform.GetComponent<Renderer>();
            rend.material = BaseMaterial;

            if ((int)TileService < TileServiceUrls.Length)
            {
                string url = TileServiceUrls[(int)TileService] + World.Zoom + "/" + tile.Tms.First + "/" + tile.Tms.Second + ".png";

                ObservableWWW.GetWWW(url).Subscribe(
                    success =>
                    {
                        if (rend)
                        {
                            rend.material.mainTexture = new Texture2D(TextureWidth, TextureHeight, FormatForTexture, false);
                            rend.material.color = new Color(1f, 1f, 1f, 1f);
                            success.LoadImageIntoTexture((Texture2D)rend.material.mainTexture);
                        }
                    },
                    error =>
                    {
                        Debug.Log(error);
                    });
            }
            else
            {
                int scale = 4;
                Texture2D tex = new Texture2D(scale*(int)tile.Rect.Width, scale*(int)tile.Rect.Width);
                rend.material.mainTexture = tex;
                if (TileService == TileServices.PerlinNoise)
                {
                    CalcPerlinNoise(tex, curMap.transform.position.x, curMap.transform.position.y);
                }
                else if (TileService == TileServices.UniformNoise)
                {
                    CalcUniformNoise(tex);
                }

            }
        }

        void CalcPerlinNoise(Texture2D noiseTex, float xOrg, float yOrg, float scale = 1.0F)
        {
            Color[] pix = new Color[noiseTex.width * noiseTex.height];
            float y = 0.0F;
            while (y < noiseTex.height)
            {
                float x = 0.0F;
                while (x < noiseTex.width)
                {
                    float xCoord = xOrg + x / noiseTex.width * scale;
                    float yCoord = yOrg + y / noiseTex.height * scale;
                    float sample = Mathf.PerlinNoise(xCoord, yCoord);
                    pix[(int)(y * noiseTex.width + x)] = new Color(sample, sample, sample);
                    x++;
                }
                y++;
            }
            noiseTex.SetPixels(pix);
            noiseTex.Apply();
        }

        void CalcUniformNoise(Texture2D noiseTex)
        {
            Color[] pix = new Color[noiseTex.width * noiseTex.height];
            float y = 0.0F;
            while (y < noiseTex.height)
            {
                float x = 0.0F;
                while (x < noiseTex.width)
                {
                    pix[(int)(y * noiseTex.width + x)] = new Color(UnityEngine.Random.Range(0f, 1f), 
                                                                   UnityEngine.Random.Range(0f, 1f), 
                                                                   UnityEngine.Random.Range(0f, 1f));
                    x++;
                }
                y++;
            }
            noiseTex.SetPixels(pix);
            noiseTex.Apply();
        }
    }
}

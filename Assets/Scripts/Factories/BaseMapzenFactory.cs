﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace DDSim
{
    public abstract class BaseMapzenFactory : BaseFactory
    {
        public abstract string MapzenLayer(); //string for mapzen data layer
        protected abstract bool Makeable(JSONObject data);
        protected abstract IEnumerable<MonoBehaviour> Make(Tile tile, JSONObject data);

        private Dictionary<Tile, HashSet<MonoBehaviour>> outputLookup = new Dictionary<Tile, HashSet<MonoBehaviour>>();
        public override IEnumerable<MonoBehaviour> GetFactoryOutput(Tile tile)
        {
            return outputLookup.GetData(tile);
        }

        public override void Process(Tile tile)
        {
            JSONObject features;
            if (tile.Data.HasField("features"))
            {
                features = tile.Data["features"];
            }
            else
            {
                features = tile.Data[MapzenLayer()]["features"];
            }

            HashSet<MonoBehaviour> output = new HashSet<MonoBehaviour>();
            foreach (var entity in features.list.Where(x => Makeable(x)).SelectMany(geo => Make(tile, geo)))
            {
                if (entity != null)
                {
                    output.Add(entity);
                }
            }
            outputLookup.Add(tile, output);
        }
    }
}

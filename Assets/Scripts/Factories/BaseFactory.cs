﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DDSim
{
    public abstract class BaseFactory : MonoBehaviour
    {
        public virtual IEnumerable<BaseFactory> GetFactoryDependencies() { return null; }
        public abstract void Process(Tile tile);
        public virtual IEnumerable<MonoBehaviour> GetFactoryOutput(Tile tile) { return null; } // default to save memory
        public virtual void PostProcess(Tile tile) { }
    }
}
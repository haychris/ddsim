﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DDSim.Settings;
using System.Linq;

namespace DDSim
{
    public class RoadNeighborFactory : BaseFactory
    {
        [SerializeField] protected RoadNeighborFactorySettings RoadNeighborFactorySettings;
        [SerializeField] protected RoadFactory RoadFactory;

        public override IEnumerable<BaseFactory> GetFactoryDependencies()
        {
            yield return RoadFactory;
        }

        public override void Process(Tile tile)
        {
            foreach (Road r in RoadFactory.GetFactoryOutput(tile))
            {
                if (RoadNeighborFactorySettings.HasSettingsFor(r.Type))
                {
                    RoadNeighborSettings typeSettings = RoadNeighborFactorySettings.GetSettingsFor(r.Type);
                    Vector3[] verts = r.GetComponent<MeshFilter>().mesh.vertices;
                    for (int i = 1; i < r.RoadPoints.Length; i++)
                    {
                        RoadNeighborBuilder rnl = new RoadNeighborBuilder()
                           .SetRoad(r)
                           .SetWidth(3f)
                           .SetMaterial(typeSettings.GetRandomResource())
                           .SetRoadVertices(verts[2 * (i - 1)], verts[2 * (i - 1) + 1], verts[2 * i], verts[2 * i + 1]);
                        foreach (ResourceAndSpawnSettings rss in typeSettings.SpawnableObjects)
                        {
                            float dist = rss.MaxPerMeter * (r.RoadPoints[i] - r.RoadPoints[i - 1]).magnitude;
                            for (int j = 0; j < dist; j++)
                            {
                                if (UnityEngine.Random.Range(0f, 1f) < rss.SpawnProb)
                                {
                                    rnl.AddObject(rss.Obj);
                                }
                            }
                        }
                        RoadNeighborBuilder rnr = rnl.Clone();

                        rnl.SetName("road neighbor left").SetSide(true);
                        rnr.SetName("road neighbor right").SetSide(false);

                        rnl.Build(true);
                        rnr.Build(true);
                    }
                   
                }
            }
        }

        //public override void PostProcess(Tile tile)
        //{
        //    base.PostProcess(tile);

        //    foreach (Road r in RoadFactory.GetFactoryOutput(tile))
        //    {
        //        r.ClearNonVehicles();
        //    }
        //}
    }
}
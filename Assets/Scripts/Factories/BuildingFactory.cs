﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using DDSim.Settings;
using DDSim.Helpers;

namespace DDSim
{
    public class BuildingFactory : BaseMapzenFactory
    {
        public bool Debug;

        [SerializeField] protected BuildingFactorySettings BuildingFactorySettings;
        private HashSet<string> buildingIds = new HashSet<string>();

        public override string MapzenLayer()
        {
            return "buildings";
        }

        protected override bool Makeable(JSONObject data)
        {
            return data["geometry"]["type"].str == "Polygon";
        }

        protected override IEnumerable<MonoBehaviour> Make(Tile tile, JSONObject data)
        {
            string id = data["properties"]["id"].ToString();
            if (buildingIds.Contains(id))
            {
                yield return null;
            }
            else
            {
                buildingIds.Add(id);

                BuildingSettings typeSettings = BuildingFactorySettings.GetSettingsFor<BuildingSettings>((data["properties"]["landuse_kind"] ?? new JSONObject()).ToString());

                BuildingBuilder bBuilder = new BuildingBuilder()
                    .SetName("building_" + data["properties"]["id"])
                    .SetDescription(data.ToString())
                    .SetMaterial(typeSettings.GetRandomResource())
                    .SetProperties(data["properties"])
                    .SetType(typeSettings.Type)
                    .SetIsVolumetric(typeSettings.IsVolumetric)
                    .SetTile(tile);

                //float minx = float.MaxValue, miny = float.MaxValue, maxx = float.MinValue, maxy = float.MinValue;
                var bb = data["geometry"]["coordinates"].list[0]; //this is wrong but cant fix it now
                for (int i = 0; i < bb.list.Count - 1; i++)
                {
                    JSONObject c = bb.list[i];
                    Vector2d p = CoordinateConverter.LatLon2Mercator(c[1].n, c[0].n);
                    Vector3d localp = new Vector3d(p, true) - tile.TileCenter;

                    //if (localp.x < minx) minx = (float)localp.x;
                    //if (localp.y < miny) miny = (float)localp.y;
                    //if (localp.x > maxx) maxx = (float)localp.x;
                    //if (localp.y > maxy) maxy = (float)localp.y;

                    bBuilder.AddBuildingCorner((Vector3)localp);
                }

                float height = 0f;
                float minHeight = 0f;
                if (typeSettings.IsVolumetric)
                {
                    if (data["properties"].HasField("height"))
                    {
                        height = data["properties"]["height"].f;
                    }
                    else
                    {
                        height = UnityEngine.Random.Range(typeSettings.MinimumBuildingHeight, typeSettings.MaximumBuildingHeight);
                    }
                    if (data["properties"].HasField("min_height"))
                    {
                        minHeight = data["properties"]["min_height"].f;
                    }
                }

                bBuilder.SetHeight(height).SetMinHeight(minHeight);

                yield return bBuilder.Build(true);
            }
        }
    }
}

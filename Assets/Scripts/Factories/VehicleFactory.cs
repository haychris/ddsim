﻿using UnityEngine;
using System.Collections;
using DDSim.Settings;
using System;
using System.Collections.Generic;

namespace DDSim
{
    public class VehicleFactory : BaseFactory
    {
        public bool DebugMode;
        [SerializeField] protected VehicleFactorySettings FactorySettings;
        [SerializeField] protected RoadFactory RoadFactory;

        protected FleetManager fleetManager;
        private int recordersCreated;

        public override IEnumerable<BaseFactory> GetFactoryDependencies()
        {
            yield return RoadFactory;
        }

        public override void Process(Tile tile)
        {
            foreach (Road r in this.RoadFactory.GetFactoryOutput(tile))
            {
                float area = r.DistanceCovered * r.Width;
                if (r.Driveable && UnityEngine.Random.Range(0f, 1f) < FactorySettings.SpawnProbabilityPerSqMeter * area)
                {
                    int i = UnityEngine.Random.Range(0, r.RoadPoints.Length - 1);
                    Vector3 next = r.RoadPoints[i + 1] + r.Tile.transform.position;
                    Vector3 start = Vector3.Lerp(r.RoadPoints[i] + r.Tile.transform.position, next, 0.1f);
                    VehicleBuilder vb = new VehicleBuilder()
                        .SetPoint(start)
                        .LookTowards(next);

                    Vehicle vehicle;
                    if (recordersCreated < FactorySettings.NumRecordingVehicles)
                    {
                        vehicle = vb.SetName("recording-vehicle")
                            .SetDebugMode(DebugMode)
                            .SetPrefab(FactorySettings.RecordingVehicleSettings.GetRandomResource())
                            .MakeRecordSensors()
                            .SetSensorRecordingPath(FactorySettings.SensorDataRelativePath)
                            .SetPercentFramesToRecord(FactorySettings.PercentFramesToRecord)
                            .Build();
                        recordersCreated++;
                    }
                    else
                    {
                        vehicle = vb.SetName("vehicle")
                            .SetDebugMode(DebugMode)
                            .SetPrefab(FactorySettings.NormalVehicleSettings.GetRandomResource())
                            .Build();
                    }
                }
            }
        }
    }
}

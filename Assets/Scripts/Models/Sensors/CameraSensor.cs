﻿using UnityEngine;
using System.Collections;
using System;

namespace DDSim
{
    public class CameraSensor : BaseSensor<byte[]>
    {
        public int Width = 280;
        public int Height = 210;
        public Camera SensingCamera;

        private RenderTexture rt;

        void Start()
        {
            rt = new RenderTexture(Width, Height, 24);
        }

        public override SensedData<byte[]> Sense(int id)
        {
            SensingCamera.targetTexture = rt;
            Texture2D screenShot = new Texture2D(Width, Height, TextureFormat.RGB24, false);
            SensingCamera.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, Width, Height), 0, 0);
            SensingCamera.targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            //Destroy(rt);

            byte[] bytes = screenShot.EncodeToPNG();
            return new CameraData(bytes, id, Width, Height);
        }
    }

    public class CameraData : SensedData<byte[]>
    {
        public int Width;
        public int Height;

        public CameraData(byte[] data, int id, int width, int height) : base(data, id)
        {
            this.Width = width;
            this.Height = height;
            this.Name = string.Format("Image{0}x{1}", Width, Height);
            this.FileFormat = "png";
        }

        public override void Save(string rootPath)
        {
            System.IO.File.WriteAllBytes(rootPath + Filename(), Data);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DDSim
{
    public class VehicleSensor : BaseSensor<Dictionary<string, float>>
    {
        public bool DistanceInFront = true;
        [Range(0f, 360f)] public int StartAngle;
        [Range(0f, 360f)] public int EndAngle;

        private Lidar lidar;

        private void Start()
        {
            lidar = GetComponentInChildren<Lidar>();
        }

        public override SensedData<Dictionary<string, float>> Sense(int id)
        {
            Dictionary<string, float> data = new Dictionary<string, float>();

            //Road nearestRoad = GetComponent<Vehicle>().NearestRoad;
            //Vector3 norm = nearestRoad.ToCenterLine(this.transform);
            //float angle = angleSigned(this.transform.forward, norm, Vector3.up);
            //float halfWidth = nearestRoad.Width / 2f; //TODO: swap with lane width
            Dictionary<int, Vector3> lane2vehicle = new Dictionary<int, Vector3>();
            foreach (var entry in lidar.SenseObject<Vehicle>(id, StartAngle, EndAngle))
            {
                Vector3 p = entry.Key.transform.position - this.transform.position;

                int lane = entry.Key.CurLane;
                if (lane2vehicle.ContainsKey(lane))
                {
                    if (lane2vehicle[lane].sqrMagnitude > p.sqrMagnitude)
                        lane2vehicle[lane] = p;
                }
                else
                {
                    lane2vehicle.Add(lane, p);
                }
            }
            foreach (var entry in lane2vehicle)
            {
                string lane = "lane" + entry.Key;

                Vector3 z = Vector3.Project(entry.Value, this.transform.forward);
                Vector3 x = Vector3.Project(entry.Value, this.transform.right);
                //float distForward = Vector3.Project(entry.Value, this.transform.forward).magnitude; // TODO: determine distance around road to vehicle
                data.Add(lane + "_dist", entry.Value.magnitude);
                //data.Add(lane + "_distForward", distForward);
                data.Add(lane + "_x", x.magnitude);
                data.Add(lane + "_y", entry.Value.y);
                data.Add(lane + "_z", z.magnitude);
                data.Add(lane + "_angle", this.transform.forward.AngleSigned(entry.Value, Vector3.up));
            }
            return new VehicleData(data, id);
        }

        public class VehicleData : NamedNumericData
        {
            public VehicleData(Dictionary<string, float> data, int id) : base(data, id)
            {
                this.Name = "Vehicle";
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace DDSim
{
    public class VehicleBoundingBoxSensor : BaseSensor<Dictionary<string, float>>
    {
        public Camera SensingCamera;
        [Range(0f, 360f)]
        public int StartAngle;
        [Range(0f, 360f)]
        public int EndAngle;

        private Lidar lidar;

        public bool CalculateOcclusion;

        private void Start()
        {
            lidar = GetComponentInChildren<Lidar>();
        }

        private Pair<Vector2> pixelBounds(Bounds bounds, Quaternion rot)
        {
            Vector3[] boundsPoints = new Vector3[8];
       
            boundsPoints[0] = bounds.min;
            boundsPoints[1] = bounds.max;
            boundsPoints[2] = new Vector3(boundsPoints[0].x, boundsPoints[0].y, boundsPoints[1].z);
            boundsPoints[3] = new Vector3(boundsPoints[0].x, boundsPoints[1].y, boundsPoints[0].z);
            boundsPoints[4] = new Vector3(boundsPoints[1].x, boundsPoints[0].y, boundsPoints[0].z);
            boundsPoints[5] = new Vector3(boundsPoints[0].x, boundsPoints[1].y, boundsPoints[1].z);
            boundsPoints[6] = new Vector3(boundsPoints[1].x, boundsPoints[0].y, boundsPoints[1].z);
            boundsPoints[7] = new Vector3(boundsPoints[1].x, boundsPoints[1].y, boundsPoints[0].z);

            Vector2 min = new Vector2(float.MaxValue, float.MaxValue);
            Vector2 max = new Vector2(float.MinValue, float.MinValue);
            foreach (Vector3 p in boundsPoints)
            {
                Vector3 rotatedP = rot * (p - bounds.center) + bounds.center; // due to bounds being axis-aligned
                Vector3 screenP = SensingCamera.WorldToScreenPoint(rotatedP);
                min.x = Mathf.Min(min.x, screenP.x);
                min.y = Mathf.Min(min.y, screenP.y);
                max.x = Mathf.Max(max.x, screenP.x);
                max.y = Mathf.Max(max.y, screenP.y);
            }
            return new Pair<Vector2>(min, max);
        }

        private float percentTruncated(float xMin, float xMax, float yMin, float yMax)
        {
            //// completely off screen
            //if (xMax < 0 || SensingCamera.pixelWidth < xMin || yMax < 0 || yMin < SensingCamera.pixelHeight)
            //{
            //    return 1f;
            //}

            //// completely on screen
            //if (0 < xMin && xMax < SensingCamera.pixelWidth && 0 < yMin && yMax < SensingCamera.pixelHeight)
            //{
            //    return 0f;
            //}

            float minX = Mathf.Max(0, xMin);
            float minY = Mathf.Max(0, yMin);
            float maxX = Mathf.Min(SensingCamera.pixelWidth, xMax);
            float maxY = Mathf.Min(SensingCamera.pixelHeight, yMax);
            float area = (maxX - minX) * (maxY - minY);
            float maxPossibleArea = (xMax - xMin) * (yMax - yMin);
            return 1 - area / maxPossibleArea;
        }

        public override SensedData<Dictionary<string, float>> Sense(int id)
        {
            Dictionary<string, float> data = new Dictionary<string, float>();

            int count = 0;
            foreach (var entry in lidar.SenseObject<Vehicle>(id, StartAngle, EndAngle))
            {
                string vehicleNum = "vehicle" + count;
                count++;

                Pair<Vector2> minMaxPixels = pixelBounds(entry.Key.GetBoundingCube(true), entry.Key.transform.rotation);
                data.Add(vehicleNum + "_min_x", minMaxPixels.First.x);
                data.Add(vehicleNum + "_min_y", minMaxPixels.First.y);
                data.Add(vehicleNum + "_max_x", minMaxPixels.Second.x);
                data.Add(vehicleNum + "_max_y", minMaxPixels.Second.y);

                float truncated = percentTruncated(minMaxPixels.First.x, minMaxPixels.Second.x, minMaxPixels.First.y, minMaxPixels.Second.y);
                data.Add(vehicleNum + "_truncated", truncated);

                if (CalculateOcclusion)
                {
                    //TODO: detect if occluded or not w/a bunch of raycasts
                    data.Add(vehicleNum + "_occluded", 3);
                }
            }
            if (count > 0)
            {
                data.Add("camera_width", SensingCamera.pixelWidth);
                data.Add("camera_height", SensingCamera.pixelHeight);
                data.Add("count", count);
            }

            return new VehicleBoundingBoxData(data, id);
        }
    }

    public class VehicleBoundingBoxData : NamedNumericData
    {
        public VehicleBoundingBoxData(Dictionary<string, float> data, int id) : base(data, id)
        {
            this.Name = "VehicleBoundingBox";
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DDSim
{
    public class StopObjectSensor : BaseSensor<Dictionary<string, float>>
    {
        public bool DistanceInFront = true;
        public bool OnlyThoseFacingUs = false;
        [Range(0f, 360f)]
        public int StartAngle;
        [Range(0f, 360f)]
        public int EndAngle;

        private Lidar lidar;

        private void Start()
        {
            lidar = GetComponentInChildren<Lidar>();
        }

        public override SensedData<Dictionary<string, float>> Sense(int id)
        {
            Dictionary<string, float> data = new Dictionary<string, float>();

            StopObject closest = null;
            List<StopObject> allStopObjs = new List<StopObject>();
            foreach (var entry in lidar.SenseObject<StopObject>(id, StartAngle, EndAngle))
            {
                Vector3 p = entry.Key.transform.position - this.transform.position;
                bool facingUs = Vector3.Angle(p, entry.Key.transform.forward) > 90f;

                if (!OnlyThoseFacingUs || facingUs)
                {
                    allStopObjs.Add(entry.Key);

                    if (closest == null)
                    {
                        closest = entry.Key;
                    }
                    else
                    {
                        if ((closest.transform.position - this.transform.position).sqrMagnitude > p.sqrMagnitude)
                            closest = entry.Key;
                    }
                }

            }

            int count = 0;
            foreach (StopObject stopObj in allStopObjs)
            {
                string stop = "stopobj" + count;
                count++;

                Vector3 p = stopObj.transform.position - this.transform.position;
                Vector3 z = Vector3.Project(p, this.transform.forward);
                Vector3 x = Vector3.Project(p, this.transform.right);
                //float distForward = z.magnitude; // TODO: determine distance around road to vehicle
                data.Add(stop + "_dist", p.magnitude);
                //data.Add(stop + "_distForward", distForward);
                data.Add(stop + "_x", x.magnitude);
                data.Add(stop + "_y", p.y);
                data.Add(stop + "_z", z.magnitude);
                data.Add(stop + "_angle", this.transform.forward.AngleSigned(p, Vector3.up));
            }

            if (count > 0)
            {
                data.Add("count", count);
            }
            return new VehicleData(data, id);
        }

        public class VehicleData : NamedNumericData
        {
            public VehicleData(Dictionary<string, float> data, int id) : base(data, id)
            {
                this.Name = "StopObject";
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace DDSim
{
    public class Lidar : MonoBehaviour
    {
        public bool ConstantlySensing;
        public bool DebugMode;
        public bool Cylinder;
        public bool Simultaneous;
        [Range(0, 180)]public int FieldOfView = 45;
        public float MaxRange = 100f;
        public int DegreesPerFrame = 10;

        public Vector3[] Rays { get; private set; }
        public RaycastHit[] Hits { get; private set; }
        public bool[] ValidHits { get; private set; }
        private int curSenseId;
        public int PhiMax { get; private set; }
        private int numPhis;
        private int thetaSweep = 0;

        void Start()
        {
            init();
        }

        private void init()
        {
            PhiMax = FieldOfView / 2;
            numPhis = PhiMax * 2 + 1;
            Rays = new Vector3[360 * numPhis];
            Hits = new RaycastHit[Rays.Length];
            ValidHits = new bool[Rays.Length];

            if (Cylinder)
            {
                initCylinder();
            }
            else
            {
                initSphere();
            }
        }

        private Vector3 initialForwardVector()
        {
            return Rays[PhiMax];
        }

        private void initCylinder()
        {
            for (int theta = 0; theta < 360; theta++)
            {
                float x = Mathf.Cos(theta * Mathf.Deg2Rad);
                float z = -Mathf.Sin(theta * Mathf.Deg2Rad);
                for (int phi = -PhiMax; phi <= PhiMax; phi++)
                {
                    float y = Mathf.Sin(phi * Mathf.Deg2Rad);
                    Rays[theta * numPhis + phi + PhiMax] = new Vector3(x, y, z);
                }
            }
        }

        private void initSphere()
        {
            for (int theta = 0; theta < 360; theta++)
            {
                float sintheta = Mathf.Sin(theta * Mathf.Deg2Rad);
                float costheta = Mathf.Cos(theta * Mathf.Deg2Rad); 
                for (int phi = -PhiMax; phi <= PhiMax; phi++)
                {
                    float sinphi = Mathf.Sin(phi * Mathf.Deg2Rad);
                    float cosphi = Mathf.Cos(phi * Mathf.Deg2Rad);
                    Rays[theta * numPhis + phi + PhiMax] = new Vector3(costheta * cosphi, sinphi, sintheta * cosphi);
                }
            }
        }

        void Update()
        {
            if (PhiMax != FieldOfView / 2)
            {
                init();
            }
            if (thetaSweep >= 360)
            {
                thetaSweep = thetaSweep % 360;
            }
            int num = thetaSweep + DegreesPerFrame;

            if (Simultaneous)
            {
                thetaSweep = 0;
                num = 360;
            }

            if (ConstantlySensing)
            {
                sensePartial(thetaSweep, num);
                thetaSweep = num;
                //Debug.Log("" + ValidHits.Count(h => h) + ", " + Hits.Length);
            }
        }

        public void Sense(int id)
        {
            if (id != curSenseId)
            {
                sensePartial(0, 360);
                curSenseId = id;
            }
        }


        public Dictionary<T, List<Vector3>> SenseObject<T>(int id, int StartAngle, int EndAngle)
        {

            Sense(id);
            //int angleOffset = (int)Mathf.Round(Vector3.Angle(initialForwardVector(), transform.right));
            int angleOffset = (int)Mathf.Round(transform.right.AngleSigned(initialForwardVector(), Vector3.up));
            angleOffset += (angleOffset < 0) ? 360 : 0;
            Dictionary<T, List<Vector3>> things = new Dictionary<T, List<Vector3>>();
            int start = StartAngle + angleOffset;
            int end = EndAngle + angleOffset;
            int wrapAround = (start > end) ? 360 : 0;
            //Debug.Log("initialForward" + initialForwardVector() + ", right: " + transform.right + ", offset: " + angleOffset + ", start: " + start + ", end: " + end);
            for (int theta = start; theta <= end + wrapAround; theta++)
            {
                for (int phi = -PhiMax; phi <= PhiMax; phi++)
                {
                    int index = indexOfAngle(theta, phi);
                    if (ValidHits[index])
                    {
                        T thing = Hits[index].transform.GetComponentInParent<T>();
                        if (thing != null)
                        {
                            Vector3 v = Rays[index] * Hits[index].distance;
                            if (!things.ContainsKey(thing))
                            {
                                List<Vector3> vs = new List<Vector3>();
                                vs.Add(v);
                                things.Add(thing, vs);
                            }
                            else
                            {
                                things.GetData(thing).Add(v);
                            }
                        }
                    }
                }
            }
            return things;
        }

        private int indexOfAngle(int horizontalAngle, int verticalAngle)
        {
            return (horizontalAngle % 360) * numPhis + verticalAngle + PhiMax;
        }

        private void sensePartial(int thetaStart, int thetaEnd)
        {
            for (int i = thetaStart; i < thetaEnd; i++)
            {
                for (int phi = -PhiMax; phi <= PhiMax; phi++)
                {
                    int index = (i % 360) * numPhis + phi + PhiMax;
                    bool hit = Physics.Raycast(transform.position, Rays[index], out Hits[index], MaxRange);
                    ValidHits[index] = hit;
                    if (hit)
                    {
                        if (DebugMode)
                        {
                            //Debug.DrawRay(transform.position, sweepVec * hitInfo.distance, Color.red);
                            DebugX.DrawPoint(Hits[index].point, Color.red, 0.05f);
                        }
                    }
                    else
                    {
                        if (DebugMode)
                        {
                            //Debug.DrawRay(transform.position, sweepVec * MaxRange, Color.green);
                            DebugX.DrawPoint(transform.position + Rays[index] * MaxRange, Color.green, 0.05f);
                        }
                    }
                }
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DDSim
{
    public abstract class BaseSensor<T> : MonoBehaviour
    {
        public bool Verbose;

        abstract public SensedData<T> Sense(int id);

        public void Record(int id, string rootPath)
        {
            if (Verbose)
            {
                SensedData<T> data = Sense(id);
                Debug.Log("Sensed: " + data.ToString() + ", saving to: " + data.Filename());
                data.Save(rootPath);
            }
            else
            {
                Sense(id).Save(rootPath);
            }
        }
    }


    public abstract class SensedData<T>
    {
        public T Data;
        public int Id;
        public double Timestamp;
        public string Name;
        public string FileFormat;

        public SensedData(T data, int id)
        {
            this.Data = data;
            this.Id = id;
            this.Timestamp = currentTime();
        }

        protected double currentTime()
        {
            //return DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;
            return Time.time;
        }
   
        public string Filename()
        {
            return string.Format("{0}_{1}_{2}.{3}", Id, Name, Timestamp, FileFormat);
        }
        abstract public void Save(string rootPath);
    }

    public class NamedNumericData : SensedData<Dictionary<string, float>>
    {
        public NamedNumericData(Dictionary<string, float> data, int id) : base(data, id)
        {
            this.FileFormat = "txt";
        }

        public override string ToString()
        {
            string[] dataEntries = Data.Select(kv => string.Format("\"{0}\":{1}", kv.Key, kv.Value)).ToArray();
            string dataStr = "{" + string.Join(", ", dataEntries) + "}";
            return dataStr;
        }

        public override void Save(string rootPath)
        {
            System.IO.File.WriteAllText(rootPath + Filename(), ToString());
        }
    }
}

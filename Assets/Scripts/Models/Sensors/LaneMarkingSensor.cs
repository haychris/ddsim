﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

namespace DDSim
{
    public class LaneMarkingSensor : BaseSensor<Dictionary<string, float>>
    {
        public override SensedData<Dictionary<string, float>> Sense(int id)
        {
            Dictionary<string, float> data = new Dictionary<string, float>();
            Road nearestRoad = GetComponent<Vehicle>().NearestRoad;
            if (nearestRoad == null)
            {
                return new LaneMarkingData(data, id);
            }

            Vector3 norm = nearestRoad.ToCenterLine(this.transform);
            float angle = this.transform.forward.AngleSigned(norm, Vector3.up);

            // TODO: check if needs to be flipped under certain cases (use velocity vector?)
            float halfWidth = nearestRoad.Width / 2f; //TODO: swap with lane width
            int offroad = (norm.magnitude > halfWidth) ? 1 : -1;
            data.Add("offroad", offroad);

            if (angle > 0)
            { // to the right of center
                data.Add("dist_RR", halfWidth - norm.magnitude);
                data.Add("dist_LL", -halfWidth - norm.magnitude);
            }
            else
            { // to the left of center
                data.Add("dist_RR", halfWidth + norm.magnitude);
                data.Add("dist_LL", -halfWidth + norm.magnitude);
            }
            //data.Add ("to_centerline", norm);
            //Debug.Log("To centerline: " + norm);
            data.Add("dist_to_centerline", norm.magnitude);
            data.Add("road_angle", Mathf.Abs(angle) - 90);
            data.Add("num_lanes", nearestRoad.NumLanes);

            return new LaneMarkingData(data, id);
        }
    }

    public class LaneMarkingData : NamedNumericData
    {
        public LaneMarkingData(Dictionary<string, float> data, int id) : base(data, id)
        {
            this.Name = "LaneMarking";
        }
    }
}
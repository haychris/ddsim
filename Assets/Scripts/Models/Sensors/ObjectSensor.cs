﻿//using UnityEngine;
//using System.Collections;
//using System;
//using System.Collections.Generic;

//namespace DDSim
//{
//    public abstract class ObjectSensor<T> : BaseSensor<Dictionary<T, List<Vector3>>>
//    {
//        public float MaxRange;
//        [Range(0f, 360f)] public int StartAngle;
//        [Range(0f, 360f)] public int EndAngle;


//        private Lidar lidar;

//        private void Start()
//        {
//            lidar = GetComponentInChildren<Lidar>();
//        }

//        public override SensedData<Dictionary<T, List<Vector3>>> Sense(int id)
//        {
//            lidar.Sense(id);
//            Dictionary<T, List<Vector3>> things = new Dictionary<T, List<Vector3>>();
//            for (int theta = StartAngle; theta <= EndAngle; theta++)
//            {
//                for (int phi = -lidar.PhiMax; phi <= lidar.PhiMax; phi++)
//                {
//                    int index = lidar.IndexOfAngle(theta, phi);
//                    if (lidar.ValidHits[index])
//                    {
//                        T thing = lidar.Hits[index].transform.GetComponentInParent<T>();
//                        if (thing != null && lidar.Hits[index].distance < MaxRange)
//                        {
//                            Vector3 v = lidar.Rays[index] * lidar.Hits[index].distance;
//                            if (things.ContainsKey(thing))
//                            {
//                                List<Vector3> vs = new List<Vector3>();
//                                vs.Add(v);
//                                things.Add(thing, vs);
//                            }
//                            else
//                            {
//                                things.GetData(thing).Add(v);
//                            }
//                        }
//                    }
//                }
//            }

//            return things;
//        }
//    }
//}

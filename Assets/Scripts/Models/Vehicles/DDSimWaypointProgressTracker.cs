﻿using System;
using UnityEngine;

namespace DDSim
{
    public class DDSimWaypointProgressTracker : MonoBehaviour
    {
        // This script can be used with any object that is supposed to follow a
        // route marked out by waypoints.

        // This script manages the amount to look ahead along the route,
        // and keeps track of progress and laps.

        [SerializeField]
        public WaypointRoute route; // A reference to the waypoint-based route we should follow

        [SerializeField]
        private float lookAheadForTargetOffset = 5;
        // The offset ahead along the route that the we will aim for

        [SerializeField]
        private float lookAheadForTargetFactor = .05f;
        // A multiplier adding distance ahead along the route to aim for, based on current speed

        [SerializeField]
        private float lookAheadForSpeedOffset = 30;
        // The offset ahead only the route for speed adjustments (applied as the rotation of the waypoint target transform)

        [SerializeField]
        private float lookAheadForSpeedFactor = .5f;
        // A multiplier adding distance ahead along the route for speed adjustments

        [SerializeField]
        private ProgressStyle progressStyle = ProgressStyle.SmoothAlongRoute;
        // whether to update the position smoothly along the route (good for curved paths) or just when we reach each waypoint.

        [SerializeField]
        private float pointToPointThreshold = 4;
        // proximity to waypoint which must be reached to switch target to next waypoint : only used in PointToPoint mode.

        [SerializeField]
        private float resetPercentage = 0.75f;
        public bool MadeSufficientProgress = true;

        public enum ProgressStyle
        {
            SmoothAlongRoute,
            PointToPoint,
        }

        // these are public, readable by other objects - i.e. for an AI to know where to head!
        public WaypointRoute.RoutePoint targetPoint { get; private set; }
        public WaypointRoute.RoutePoint speedPoint { get; private set; }
        public WaypointRoute.RoutePoint progressPoint { get; private set; }

        public Transform target;

        private float progressDistance; // The progress round the route, used in smooth mode.
        public int progressNum; // the current waypoint number, used in point-to-point mode.
        private Vector3 lastPosition; // Used to calculate current speed (since we may not have a rigidbody component)
        private float speed; // current speed of this object (calculated from delta since last frame)

        // setup script properties
        private void Start()
        {
            // we use a transform to represent the point to aim for, and the point which
            // is considered for upcoming changes-of-speed. This allows this component
            // to communicate this information to the AI without requiring further dependencies.

            // You can manually create a transform and assign it to this component *and* the AI,
            // then this component will update it, and the AI can read it.
            if (target == null)
            {
                target = new GameObject(name + " Waypoint Target").transform;
            }

            Reset();
        }


        // reset the object to sensible values
        public void Reset()
        {
            progressDistance = 0;
            progressNum = 0;
            if (progressStyle == ProgressStyle.PointToPoint && route.Waypoints.Count > 0)
            {
                target.position = route.Waypoints[progressNum].position;
                target.rotation = route.Waypoints[progressNum].rotation;
            }
        }

        // TODO: if looking past end, add new roads
        private void Update()
        {
            if (route.Waypoints.Count == 0)
                return;
            if (progressStyle == ProgressStyle.SmoothAlongRoute)
            {
                // determine the position we should currently be aiming for
                // (this is different to the current progress position, it is a a certain amount ahead along the route)
                // we use lerp as a simple way of smoothing out the speed over time.
                if (Time.deltaTime > 0)
                {
                    //speed = Mathf.Lerp(speed, (lastPosition - transform.position).magnitude / Time.deltaTime,
                    //    Time.deltaTime);
                    speed = GetComponent<DDSimCarController>().CurrentSpeed;
                }
                target.position =
                    route.GetRoutePoint(progressDistance + lookAheadForTargetOffset + lookAheadForTargetFactor * speed)
                        .position;

                Vector3 direction = route.GetRoutePoint(progressDistance + lookAheadForSpeedOffset + lookAheadForSpeedFactor * speed)
                        .direction;

                if (direction != Vector3.zero)
                {
                    target.rotation = Quaternion.LookRotation(direction);
                }


                // get our current progress along the route
                progressPoint = route.GetRoutePoint(progressDistance);
                Vector3 progressDelta = progressPoint.position - transform.position;
                if (Vector3.Dot(progressDelta, progressPoint.direction) < 0)
                {
                    progressDistance += progressDelta.magnitude * 0.5f;
                }

                lastPosition = transform.position;
            }
            else
            {
                // point to point mode. Just increase the waypoint if we're close enough:

                Vector3 targetDelta = target.position - transform.position;
                if (targetDelta.magnitude < pointToPointThreshold)
                {
                    //progressNum = (progressNum + 1) % route.Waypoints.Count;
                    progressNum++;
                }


                target.position = route.Waypoints[progressNum].position;
                target.rotation = route.Waypoints[progressNum].rotation;

                // get our current progress along the route
                progressPoint = route.GetRoutePoint(progressDistance);
                Vector3 progressDelta = progressPoint.position - transform.position;
                if (Vector3.Dot(progressDelta, progressPoint.direction) < 0)
                {
                    progressDistance += progressDelta.magnitude;
                }
                lastPosition = transform.position;
            }

            if (progressDistance / route.Length > resetPercentage || (float)progressNum / route.Waypoints.Count > resetPercentage)
            {
                MadeSufficientProgress = true;
            }
        }


        private void OnDrawGizmos()
        {
            if (Application.isPlaying && route != null && route.Waypoints.Count > 0)
            {
                Gizmos.color = Color.green;
                Gizmos.DrawLine(transform.position, target.position);
                Gizmos.DrawWireSphere(route.GetRoutePosition(progressDistance), 1);
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(target.position, target.position + target.forward);
            }
        }
    }
}

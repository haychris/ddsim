﻿using System;
using System.Collections;
using UnityEngine;
using DDSim;
using System.Linq;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;

#endif

namespace DDSim
{
    public class WaypointRoute : MonoBehaviour
    { 
        public float editorVisualisationSubsteps = 1000;
        [SerializeField] private float _length;
        public float Length { get { return _length; } private set { _length = value; } }
        [SerializeField] private List<Transform> _waypoints = new List<Transform>();
        public List<Transform> Waypoints { get { return _waypoints; } private set { _waypoints = value; } }

        [SerializeField] public bool SmoothRoute = true;
        private int numPoints;
        private Vector3[] points;
        private float[] distances;

        private float i;
        private Vector3 P0;
        private Vector3 P1;
        private Vector3 P2;
        private Vector3 P3;

        // Use this for initialization
        private void Awake()
        {
            updateCache();
        }

        private void updateCache()
        {
            CachePositionsAndDistances();
            Length = (distances.Length > 0) ? distances[distances.Length - 1] : 0;
            numPoints = Waypoints.Count;
        }

        public void ResetRoute()
        {
            Waypoints = new List<Transform>();
            updateCache();
        }


        public void AddWaypoints(IEnumerable<Transform> transforms)
        {
            Waypoints.AddRange(transforms);
            updateCache();
        }
        

        public RoutePoint GetRoutePoint(float dist)
        {
            // position and direction
            Vector3 p1 = GetRoutePosition(dist);
            Vector3 p2 = GetRoutePosition(dist + 1f);
            Vector3 delta = p2 - p1;
            return new RoutePoint(p1, delta.normalized);
        }


        public Vector3 GetRoutePosition(float dist)
        {
            if (dist == 0 || Waypoints.Count == 1)
            {
                return points[0];
            }
            if (Length == 0)
            {
                throw new InvalidOperationException("Length cannot be 0\n" + "dist: " + dist + ", points" + Waypoints.PrettyString());
            }
            if (dist > Length)
            {
                Debug.LogWarning("Requesting distance greater than length of track: dist: " + dist + ", Length: " + Length, this);
                return points[points.Length - 1];
                //Debug.LogError("dist: " + dist + ", Length: " + Length + "\n" + points.PrettyString() + "\n" + distances.PrettyString(), this);
                //throw new InvalidOperationException("Cannot get point for dist greater than Length\ndist: " + dist + ", Length" + Length);
            }

            int point = 0;
            while (distances[point] < dist)
            {
                point++;
            }

            // found point numbers, now find interpolation value between the two middle points
            i = Mathf.InverseLerp(distances[point - 1], distances[point], dist);

            if (SmoothRoute && 1 < point && point < numPoints - 1)
            {
                // smooth catmull-rom calculation between the two relevant points
                P0 = points[point - 2];
                P1 = points[point - 1];
                P2 = points[point];
                P3 = points[point + 1];

                return CatmullRom(P0, P1, P2, P3, i);
            }
            else
            {
                // simple linear lerp between the two points:
                return Vector3.Lerp(points[point - 1], points[point], i);
            }
        }


        private Vector3 CatmullRom(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float i)
        {
            // comments are no use here... it's the catmull-rom equation.
            // Un-magic this, lord vector!
            return 0.5f *
                ((2 * p1) + (-p0 + p2) * i + (2 * p0 - 5 * p1 + 4 * p2 - p3) * i * i +
                    (-p0 + 3 * p1 - 3 * p2 + p3) * i * i * i);
        }


        private void CachePositionsAndDistances()
        {
            // transfer the position of each point and distances between points to arrays for
            // speed of lookup at runtime
            points = Waypoints.Select(wp => wp.position).ToArray();
            distances = new float[Waypoints.Count];

            if (Waypoints.Count == 0) { return; }

            float accumulateDistance = 0f;
            distances[0] = 0f;
            for (int i = 1; i < points.Length; i++)
            {
                accumulateDistance += (points[i] - points[i-1]).magnitude;
                distances[i] = accumulateDistance;
            }
        }


        private void OnDrawGizmos()
        {
            DrawGizmos(false);
        }


        private void OnDrawGizmosSelected()
        {
            DrawGizmos(true);
        }


        private void DrawGizmos(bool selected)
        {
            if (Waypoints.Count > 1)
            {
                updateCache();

                Gizmos.color = selected ? Color.yellow : new Color(1, 1, 0, 0.5f);
                Vector3 prev = Waypoints[0].position;
                if (SmoothRoute)
                {
                    float stepSize = Length / editorVisualisationSubsteps;
                    for (float dist = stepSize; dist <= Length; dist += stepSize)
                    {
                        //Vector3 next = GetRoutePosition(dist + 1);
                        Vector3 cur = GetRoutePosition(dist);
                        Gizmos.DrawLine(prev, cur);
                        prev = cur;
                    }

                    for (int n = 0; n < Waypoints.Count; n++)
                    {
                        Gizmos.DrawWireSphere(Waypoints[n].position, 0.5f);
                    }
                }
                else
                {
                    for (int n = 1; n < Waypoints.Count; n++)
                    {
                        Vector3 cur = Waypoints[n].position;
                        Gizmos.DrawLine(prev, cur);
                        prev = cur;
                    }
                }
            }
        }

        public struct RoutePoint
        {
            public Vector3 position;
            public Vector3 direction;


            public RoutePoint(Vector3 position, Vector3 direction)
            {
                this.position = position;
                this.direction = direction;
            }
        }
    }
}
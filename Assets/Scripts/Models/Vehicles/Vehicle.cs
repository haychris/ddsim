﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityStandardAssets.Vehicles.Car;

namespace DDSim
{
    public class Vehicle : MonoBehaviour
    {
        public bool Recording;
        public string SensorDataPath;

        public bool DebugMode;
        public bool CanPlanRoute = true;
        //public Road nearestRoad; // TODO: make private after debugging
        public Road NearestRoad { get; private set; }
        public int CurLane;

        public int NumFramesPerRecording;
        private int recordingCount;
        private int frameCount;

        private DDSimWaypointProgressTracker waypointTracker;
        private Bounds initialBounds;
        private Vector3 boundsOffset;

        // Use this for initialization
        void Start()
        {
            initialBounds = GetBoundingCube(false);
            boundsOffset = initialBounds.center - this.transform.position;
            waypointTracker = GetComponent<DDSimWaypointProgressTracker>();
            planRoute(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (CanPlanRoute && (waypointTracker.MadeSufficientProgress || waypointTracker.route.Waypoints.Count < 2))
            {
                planRoute(true);
            }

            if (this.Recording)
            {
                updateNearestRoad();
            }
        }

        public Bounds GetBoundingCube(bool useInitial)
        {
            if (useInitial)
            {
                initialBounds.center = this.transform.position + boundsOffset;
                return initialBounds;
            }

            //Quaternion rot = this.transform.rotation;
            //this.transform.rotation = Quaternion.identity; // need to do this to get non-axis aligned bounds
            //Bounds bounds = new List<Renderer> { GetComponent<Renderer>() }
            //        .Union(GetComponentsInChildren<Renderer>())
            //        .Where(rend => rend != null)
            //        .Select(rend => rend.bounds)
            //        .Where(bound => bound.center != Vector3.zero) // was giving erroneously large bounds without this
            //        .Aggregate((b1, b2) =>
            //        {
            //            b1.Encapsulate(b2);
            //            return b1;
            //        });
            //this.transform.rotation = rot;
            //return bounds;
            return this.gameObject.GetAllBounds();
        }

        //public Bounds GetMeshBoundingCube()
        //{
        //    return new List<MeshFilter> { GetComponent<MeshFilter>() }
        //            .Union(GetComponentsInChildren<MeshFilter>())
        //            .Where(mf => mf != null)
        //            .Select(mf => mf.mesh.bounds)
        //            .Where(bound => bound.center != Vector3.zero) // was giving erroneously large bounds without this
        //            .Aggregate((b1, b2) =>
        //            {
        //                b1.Encapsulate(b2);
        //                return b1;
        //            });
        //}

        private void OnDrawGizmos()
        {
            if (DebugMode && !Recording)
            {
                Bounds combinedBounds = GetBoundingCube(true);
                //Gizmos.matrix = transform.localToWorldMatrix;
                //Gizmos.DrawWireCube(combinedBounds.center + transform.position, combinedBounds.size);

                // combinedBounds is defined in global space as an axis-aligned cube
                // and Gizmos doesn't let you rotate a cube, so we need to shift the space
                // Gizmos operates in from global to local, and correct the position of 
                // combinedBounds accordingly
                Gizmos.matrix = transform.localToWorldMatrix;
                Gizmos.DrawWireCube(combinedBounds.center - transform.position, combinedBounds.size);
            }
        }

        void LateUpdate()
        {
            if (this.Recording)
            {
                if (frameCount % NumFramesPerRecording == 0)
                {
                    StartCoroutine(this.recordSensors());
                }
                frameCount++;
            }
        }

        private IEnumerator recordSensors()
        {
            yield return new WaitForEndOfFrame();

            // most in-game sensors
            foreach (var sensor in GetComponents<BaseSensor<Dictionary<string, float>>>())
            {
                sensor.Record(recordingCount, SensorDataPath);
            }

            // camera sensors
            foreach (var sensor in GetComponents<BaseSensor<byte[]>>())
            {
                sensor.Record(recordingCount, SensorDataPath);
            }

            recordingCount++;
        }

        private void updateNearestRoad()
        {
            //TODO: use SphereCollider to try and find road, if none found then use more expensive way via fleet manager            
            List<Road> roads = Physics.OverlapSphere(this.transform.position, 2f).Select(c => c.GetComponent<Road>()).Where(r => r != null).ToList();
            if (roads.Count == 1)
            {
                this.NearestRoad = roads[0];
                return;
            }
            else if (roads.Count > 1)
            {
                Debug.LogWarning("Within range of too many roads (" + roads.Count + "), using more precise method...\n"
                    + string.Join(", ", roads.Select(r => r.name).ToArray()), this);
            }

            FleetManager fm = GetComponentInParent<FleetManager>();
            if (fm != null)
            {
                this.NearestRoad = fm.NearestRoad(this);
            }
        }

        private void planRoute(bool extendExisting)
        {
            updateNearestRoad();
            if (this.NearestRoad == null)
            {
                return;
            }

            List<Transform> newWaypoints = new List<Transform>();
            GameObject curway = new GameObject("temp");
            curway.transform.position = this.transform.position + this.transform.forward * 8;
            newWaypoints.Add(curway.transform);

            int desiredLane = 0;

            Pair<int> rpAhead = this.NearestRoad.FirstRoadPointAhead(this.transform);
            int direction = rpAhead.Second;

            // TODO: add point lerped to current location?

            newWaypoints.AddRange(this.NearestRoad.GetWaypoints(desiredLane, rpAhead.First, direction));

            // get next road from head
            Road curRoad = this.NearestRoad;
            for (int i = 0; i < 10; i++)
            {
                Intersection intersect = curRoad.GetIntersection(direction < 0);
                if (intersect == null)
                {
                    Debug.LogWarning("Out of intersections/roads, uh oh...");
                    break;
                }

                //List<Road> roads = intersect.Roads;
                //curRoad = roads[Random.Range(0, roads.Count)];
                curRoad = intersect.GetRandomRoad(curRoad, true);
                bool startAtHd = intersect.AtHd(curRoad);
                int startIndex = (startAtHd) ? 0 : curRoad.RoadPoints.Length - 1;
                direction = (startAtHd) ? 1 : -1;
                newWaypoints.AddRange(curRoad.GetWaypoints(desiredLane, startIndex, direction));
            }

            waypointTracker.route.ResetRoute();
            waypointTracker.route.AddWaypoints(newWaypoints);
            waypointTracker.Reset();
            waypointTracker.MadeSufficientProgress = false;
        }
    }
}

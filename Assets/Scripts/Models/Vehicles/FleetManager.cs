﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DDSim
{
    public class FleetManager : MonoBehaviour
    {
        private RoadNetwork roadNetwork;

        private List<Pair<Vehicle, bool>> vehicles;
        [SerializeField] private Vehicle mainRecordingVehicle;
        [SerializeField] private int vehicleCount = 0;
        [SerializeField] private int activeVehicleCount = 0;

        public float OFF_RANGE = 500f;
        public float ON_RANGE = 400f;

        public float MIN_VELOCITY = 0.5f;

        public int NUM_FRAMES_PER_UDPATE = 100;
        private int updateFrameCounter = 0;

        // Use this for initialization
        void Start()
        {
            vehicles = new List<Pair<Vehicle, bool>>();
        }

        // Update is called once per frame
        void Update()
        {
            int index = updateFrameCounter % NUM_FRAMES_PER_UDPATE;
            if (index == 1 || (Time.deltaTime > 1f && index > 50) || (Time.deltaTime > 2.5f))
            {
                toggleVehiclesByRanges(ON_RANGE * ON_RANGE, OFF_RANGE * OFF_RANGE);
                teleportIfStuck(MIN_VELOCITY);
            }
            updateFrameCounter++;
        }

        private void teleportIfStuck(float minVelocity)
        {
            foreach (Pair<Vehicle, bool> v in vehicles.Where(v => v.Second))
            {
                if (v.First.GetComponent<Rigidbody>().velocity.magnitude < minVelocity)
                {
                    //if (v.First.NearestRoad == null)
                    //{
                        Debug.Log("teleporting to waypoint", v.First);
                        v.First.transform.position = v.First.GetComponent<DDSimWaypointProgressTracker>().target.position;
                    //}
                    //else
                    //{
                    //    Debug.Log("teleporting to center of nearest road", v.First); 
                    //    // TODO: should it be += ?
                    //    v.First.transform.localPosition -= v.First.NearestRoad.ToCenterLine(v.First.transform);
                    //}
                }
            }
        }
        // onRange should be less than offRange to prevent thrashing
        private void toggleVehiclesByRanges(float onRangeSqr, float offRangeSqr)
        {
            foreach (Pair<Vehicle, bool> v in vehicles)
            {
                float sqrDist = (v.First.transform.position - mainRecordingVehicle.transform.position).sqrMagnitude;

                // currently enabled vehicles
                if (v.Second)
                {
                    if (sqrDist > offRangeSqr)
                    {
                        v.Second = false;
                        v.First.gameObject.SetActive(false);
                        activeVehicleCount--;
                    }
                }
                else // currently disabled vehicles
                {
                    if (sqrDist < onRangeSqr)
                    {
                        v.Second = true;
                        v.First.gameObject.SetActive(true);
                        activeVehicleCount++;
                    }
                }
            }
        }

        public void SetRoadNetwork(RoadNetwork roadNetwork)
        {
            this.roadNetwork = roadNetwork;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            vehicles.Add(new Pair<Vehicle, bool>(vehicle, true));
            if (vehicle.Recording && mainRecordingVehicle == null)
            {
                mainRecordingVehicle = vehicle;
            }

            vehicle.name += "-" + vehicleCount;
            vehicleCount++;
            activeVehicleCount++;
        }

        public Road NearestRoad(Vehicle vehicle)
        {
            return this.roadNetwork.NearestRoad(vehicle.transform);
        }
    }
}
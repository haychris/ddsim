﻿using UnityEngine;
using System;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using DDSim.Settings.Enums;

namespace DDSim
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
    public class Road : RoadBase
    {
        public RoadType Type;
        public bool Driveable;
        public int NumLanes;
        public float Width;
        public float DistanceCovered;
        public string MapzenID;
        [TextArea(3, 10)] public string Description;
        public Vector3[] RoadPoints;

        private Intersection hdIntersection;
        private Intersection tlIntersection;
        //public Vector3d[] RoadPointsMercator;

        public Vector3[] Vertices;
        public int[] Triangles;

        public bool mergeable = true;
        private bool acquired = false;

        public void Acquire()
        {
            while (acquired)
            {
                Thread.Sleep(10);
            }
            acquired = true;
        }
        public void Release()
        {
            acquired = false;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public Vector3 Hd()
        {
            return this.RoadPoints[0];
        }

        public Vector3 Tl()
        {
            return this.RoadPoints[this.RoadPoints.Length - 1];
        }

        //private Pair<Vector3> verticesForRoadpoint(int i)
        //{
        //    Mesh mesh = GetComponent<MeshFilter>().mesh;
        //    return new Pair<Vector3>(mesh.vertices[i*2], mesh.vertices[i*2+1]);
        //}

        public List<Transform> GetWaypoints(int lane, int startingIndex, int direction)
        {
            List<Transform> roadPath = new List<Transform>();
            float halfLaneOffset = 1f / (2 * this.NumLanes);
            float offset = (direction < 0) ? (2*lane + 1) * halfLaneOffset : (2*(this.NumLanes - 1 - lane) + 1) * halfLaneOffset;

            Vector3[] verts = GetComponent<MeshFilter>().mesh.vertices;
            for (int i = startingIndex; -1 < i && i < this.RoadPoints.Length; i += direction)
            {
                //Pair<Vector3> vv = verticesForRoadpoint(i);
                Vector3 p = Vector3.Lerp(verts[i*2], verts[i*2 + 1], offset);
                //Debug.Log("a: " + verts[i * 2] + ", b: " + verts[i * 2 + 1] + ", offset: " + offset + "result: " + p, this);

                Transform t = new GameObject("lane" + lane + "_waypoint" + i).transform;
                t.localPosition = p + Vector3.up;
                t.SetParent(this.transform, false);
                roadPath.Add(t);
            }
            return roadPath;
        }

        private Vector3 toRoadPoint(Transform transform, Vector3 rp)
        {
            return transform.position - (this.transform.position + rp);
        }

        public Pair<int, Vector3> ClosestRoadpoint(Transform transform)
        {
            int best = 0;
            float bestDistance = float.MaxValue;
            for (int i = 0; i < RoadPoints.Length; i++)
            {
                float dist = toRoadPoint(transform, RoadPoints[i]).sqrMagnitude;
                if (dist < bestDistance)
                {
                    bestDistance = dist;
                    best = i;
                }
            }

            return new Pair<int, Vector3>(best, this.RoadPoints[best]);
        }

        public void AddIntersection(Intersection intersection, bool atHd)
        {
            if (atHd)
            {
                this.hdIntersection = intersection;
            }
            else
            {
                this.tlIntersection = intersection;
            }
        }

        public Intersection GetIntersection(bool atHd)
        {
            return (atHd) ? hdIntersection : tlIntersection;
        }

        // TODO: needed?
        public double ApproximateDistanceTo(Transform transform)
        {
            Vector3 rp = ClosestRoadpoint(transform).Second;
            return toRoadPoint(transform, rp).magnitude;
        }

        public int forwardDirection(Transform t, int rpIndex)
        {
            int direction;

            // WRONG, what if facing end of road
            //if (rpIndex == 0)
            //{
            //    direction = 1;
            //}
            //else if (rpIndex == this.RoadPoints.Length - 1)
            //{
            //    direction = -1;
            //}
            //else
            //{
            //    // determine which direction of iterating through road points is more in line with the vehicle's current direction

            //    //Vector3 a = this.nearestRoad.RoadPoints[closestRp.First - 1];
            //    Vector3 b = this.RoadPoints[rpIndex];
            //    Vector3 c = this.RoadPoints[rpIndex + 1];

            //    //Vector3 ab = a - b;
            //    Vector3 cb = c - b;
            //    float angle = Vector3.Angle(t.forward, cb);
            //    direction = (angle < 90) ? 1 : -1; // needs to account for road's internal direction
            //}

            if (rpIndex == this.RoadPoints.Length - 1)
            {
                float angle = Vector3.Angle(t.forward, this.RoadPoints[rpIndex - 1] - this.RoadPoints[rpIndex]);
                direction = (angle < 90) ? -1 : 1;
            }
            else
            {
                float angle = Vector3.Angle(t.forward, this.RoadPoints[rpIndex + 1] - this.RoadPoints[rpIndex]);
                direction = (angle < 90) ? 1 : -1;
            }
            
            return direction;
        }

        // Returns index and direction of first road point forward of t
        public Pair<int> FirstRoadPointAhead(Transform t)
        {
            Pair<int, Vector3> closestRp = this.ClosestRoadpoint(t);
            int direction = forwardDirection(t, closestRp.First);

            Vector3 t2Rp = closestRp.Second + this.transform.position - t.position;
            int offset = (Vector3.Angle(t.forward, t2Rp) > 90) ? direction : 0;

            return new Pair<int>(closestRp.First + offset, direction);
        }

        public Vector3 ToCenterLine(Transform t)
        {
            //Vector3d pos = new Vector3d(t.localPosition);
            //Vector3d norm = this.ToCenterLine(pos);
            //return (Vector3)norm;
            //return ToCenterLine(t.position);

            Pair<int> rpAhead = FirstRoadPointAhead(t);
            int ahead = rpAhead.First;
            int behind = rpAhead.First - rpAhead.Second;

            if (ahead < 0 || behind < 0)
            {
               return toRoadPoint(t, RoadPoints[0]);
            }
            if (ahead > RoadPoints.Length - 1 || behind > RoadPoints.Length - 1)
            {
                return toRoadPoint(t, RoadPoints[RoadPoints.Length - 1]);
            }

            if (behind < 0 || behind > RoadPoints.Length - 1)
            {
                Debug.LogError("Index behind vehicle is out of bounds: " + behind + "\n" + t, this);
            }

            Vector3 p1 = this.transform.position + RoadPoints[ahead];
            Vector3 p2 = this.transform.position + RoadPoints[behind];

            Vector3 posProject = Vector3.Project(t.position, p1 - p2);
            Vector3 p2Project = Vector3.Project(p2, p1 - p2);
            Vector3 posInline = posProject - p2Project + p2;

            return t.position - posInline;
        }

        //public Vector3 ToCenterLine(Vector3 pos)
        //{
        //    Pair<int, Vector3> rp = ClosestRoadpoint();
        //}

        public bool Contains(Transform transform)
        {
            Vector3[] verts = GetComponent<MeshFilter>().mesh.vertices;
            Vector3 pLocal = transform.position - this.transform.position;
            Vector2 p = new Vector2(pLocal.x, pLocal.z);
            //return containsPoint(convert2Vector2(verts).ToArray(), p);
            bool oldContains = verts.Convert2Vector2().ToArray().ContainsPoint(p);
            return GetComponent<MeshFilter>().mesh.ContainsXZPoint(p) || oldContains;

            //Vector3 prevA = verts[0];
            //Vector3 prevB = verts[1];
            //for (int i = 2; i < verts.Length; i += 2)
            //{
            //    Vector3 a = verts[i];
            //    Vector3 b = verts[i + 1];


            //}
            //throw new NotImplementedException();
        }

        //public void ClearNonVehicles()
        //{
        //    GetComponent<MeshCollider>().bounds.;
        //    Vector3[] verts = GetComponent<MeshFilter>().mesh.vertices;
        //    Vector3 backL = verts[0];
        //    Vector3 backR = verts[1];
        //    Vector3 frontL, frontR;
        //    for (int i = 3; i < verts.Length; i += 2)
        //    {
        //        frontL = verts[i];
        //        frontR = verts[i + 1];



        //        backL = frontL;
        //        backR = frontR;
        //    }
        //}

        // Connected if they share an endpoint
        public bool IsConnected(Road other)
        {
            return ConnectedWhere(other) >= 0;
        }


        // Gives location of connection, if one exists
        // Returns -1 if the roads are not connected
        //      A     B
        // 0: TL-HD HD-TL
        // 1: TL-HD TL-HD
        // 2: HD-TL HD-TL
        // 3: HD-TL TL-HD
        private int ConnectedWhere(Road other)
        {
            float epsilon = 0.05f;
            Vector3 tileDiff = TileDiff(other);
            if (Vector3.Distance(this.Hd(), other.Hd() + tileDiff) < epsilon)
            {
                return 0;
            }
            if (Vector3.Distance(this.Hd(), other.Tl() + tileDiff) < epsilon)
            {
                return 1;
            }
            if (Vector3.Distance(this.Tl(), other.Hd() + tileDiff) < epsilon)
            {
                return 2;
            }
            if (Vector3.Distance(this.Tl(), other.Tl() + tileDiff) < epsilon)
            {
                return 3;
            }

            return -1;
        }

        private static bool vectorsIntersect(Vector3 a, Vector3 b, float minIntersectingAngle = 20f)
        {
            double angle = Vector3.Angle(a, b); // angle between a and b, in range [0, 180]
            return minIntersectingAngle < angle && angle < 180f - minIntersectingAngle;
        }

        private bool intersects(Road other)
        {
            int whereConnected = ConnectedWhere(other);

            Vector3 a, b; // TODO: add check for zero vector in a/b
            switch (whereConnected)
            {
                case 0:
                    a = this.RoadPoints[1] - this.Hd();
                    b = other.RoadPoints[1] - other.Hd();
                    return vectorsIntersect(a, b);
                case 1:
                    a = this.RoadPoints[1] - this.Hd();
                    b = other.RoadPoints[other.RoadPoints.Length - 2] - other.Tl();
                    return vectorsIntersect(a, b);
                case 2:
                    a = this.RoadPoints[this.RoadPoints.Length - 2] - this.Tl();
                    b = other.RoadPoints[1] - other.Hd();
                    return vectorsIntersect(a, b);
                case 3:
                    a = this.RoadPoints[this.RoadPoints.Length - 2] - this.Tl();
                    b = other.RoadPoints[other.RoadPoints.Length - 2] - other.Tl();
                    return vectorsIntersect(a, b);
                default: // TODO: handle T-Intersects
                    a = this.RoadPoints[1] - this.Hd();
                    b = this.RoadPoints[this.RoadPoints.Length - 2] - this.Tl();
                    for (int i = 1; i < other.RoadPoints.Length; i++)
                    {
                        Vector3 c = other.RoadPoints[i] - other.RoadPoints[i - 1];
                        if (vectorsIntersect(a, c) || vectorsIntersect(b, c))
                        {
                            return true;
                        }
                    }

                    return false;
            }
        }

        public bool IsIntersecting(Road other)
        {
            return this.intersects(other) || other.intersects(this);
        }

        public bool CanIntersect(Road other)
        {
            //bool sameWidth = this.Width == other.Width;
            bool sameWidth = true;
            return this.IsIntersecting(other) && sameWidth;
        }

        public Pair<int> FindWhere(Road other)
        {
            int whereConnected = ConnectedWhere(other);

            int start, direction;
            switch (whereConnected)
            {
                case 0:
                    direction = 1;
                    start = 0;
                    return new Pair<int>(start, direction);
                case 1:
                    direction = -1;
                    start = other.RoadPoints.Length - 1;
                    return new Pair<int>(start, direction);
                case 2:
                    direction = -1;
                    start = this.RoadPoints.Length - 1;
                    return new Pair<int>(start, direction);
                case 3:
                    direction = 1;
                    start = this.RoadPoints.Length - other.RoadPoints.Length;
                    return new Pair<int>(start, direction);
                default:
                    throw new ArgumentException("Must be connected to be subset");
            }
        }

        public bool CanMerge(Road other)
        {
            //bool sameSortRank = this.transform.localPosition.y == other.transform.localPosition.y;
            bool sameWidth = this.Width == other.Width;
            return this.mergeable && this != other && IsConnected(other) && !IsIntersecting(other) && sameWidth;
        }

        public static RoadBuilder GetMergedBuilder(Road a, Road b)
        {
            int whereConnected = a.ConnectedWhere(b);
            if (whereConnected < 0)
            {
                throw new ArgumentException("Roads must be connected in order to merge");
            }
            if (!a.CanMerge(b))
            {
                throw new ArgumentException("Roads unable to be merged");
            }
            a.mergeable = false;
            b.mergeable = false;

            RoadBuilder rbA = RoadBuilder.UnBuild(a);
            RoadBuilder rbB = RoadBuilder.UnBuild(b);
            switch (whereConnected)
            {
                case 0:
                    rbA.ReverseRoad();
                    rbA.Append(rbB);
                    return rbA;
                case 1:
                    rbB.Append(rbA);
                    return rbB;
                case 2:
                    rbA.Append(rbB);
                    return rbA;
                case 3:
                    rbB.ReverseRoad();
                    rbA.Append(rbB);
                    return rbA;
                default:
                    throw new ArgumentException("Invalid connection between roads");
            }
        }

        public static Road Merge(Road a, Road b)
        {
            return GetMergedBuilder(a, b).Build(true);
        }

        //public Road Merge(Road b)
        //{
        //    return GetMergedBuilder(this, b).Build(this, true);
        //}

        public Pair<Road> Split(int i)
        {
            return Split(RoadPoints[i]);
        }
        public Pair<Road> Split(Vector3 v)
        {
            RoadBuilder rba = RoadBuilder.UnBuild(this);
            rba.ResetRoadPoints();
            RoadBuilder rbb = rba.Clone();
            String name = rba.name;
            rba.SetName(name + "_a");
            rbb.SetName(name + "_b");

            bool foundV = false;
            foreach (Vector3 p in RoadPoints)
            {
                if (p == v || (p - v).magnitude < 0.05) // TODO: fix, hacks
                {
                    foundV = true;
                    rba.AddPoint(p);
                    rbb.AddPoint(p);
                }
                else if (foundV)
                {
                    rbb.AddPoint(p);
                }
                else
                {
                    rba.AddPoint(p);
                }
            }

            if (rbb.GetNumPoints() < 2)
            {
                Debug.LogWarning("v: " + v.ToString("G6") + ", hd: " + Hd().ToString("G6") + ", tl: " + Tl().ToString("G6"), this);
                throw new ArgumentException("Invalid Vector3 v used to split");
            }

            return new Pair<Road>(rba.Build(true), rbb.Build(true));
        }

        // TODO: refactor, shouldn't transform road once created
        // TODO: update UVs to reflect change
        // TODO: delete points if moving back far enough
        //public void MoveEndpointBack(float depth, bool hd)
        //{
        //    Mesh mesh = GetComponent<MeshFilter>().mesh;
        //    Vector3[] verts = mesh.vertices;
        //    if (hd)
        //    {
        //        Vector3 offset = (RoadPoints[1] - RoadPoints[0]).normalized * depth;
        //        verts[0] += offset;
        //        verts[1] += offset;
        //        //RoadPoints[0] += offset;
        //    }
        //    else
        //    {
        //        Vector3 offset = (RoadPoints[RoadPoints.Length - 2] - RoadPoints[RoadPoints.Length - 1]).normalized * depth;
        //        verts[verts.Length - 1] += offset;
        //        verts[verts.Length - 2] += offset;
        //        //RoadPoints[RoadPoints.Length - 1] += offset;
        //    }
        //    mesh.vertices = verts;
        //    mesh.RecalculateBounds();
        //    mesh.RecalculateNormals();
        //}

        public void MoveForIntersection(List<Vector3> endpoints, bool atHd)
        {
            //Debug.Log("Hd: " + Hd() + ", Tl: " + Tl() + ", endpoints: " + "[" + String.Join(", ", endpoints.Select(p => p.ToString()).ToArray()) + "]", this);
            List<float> dists = endpoints.Select(p => (atHd) ? (RoadPoints[1] - p).sqrMagnitude : (RoadPoints[RoadPoints.Length - 2] - p).sqrMagnitude).ToList();
            float minDist = float.MaxValue, minDist2 = float.MaxValue;
            Vector3 min = endpoints[0];
            Vector3 min2 = min;
            for (int i = 0; i < endpoints.Count; i++)
            {
                if (dists[i] < minDist)
                {
                    minDist2 = minDist;
                    min2 = min;
                    minDist = dists[i];
                    min = endpoints[i];
                }
                else if (dists[i] < minDist2)
                {
                    minDist2 = dists[i];
                    min2 = endpoints[i];
                }
            }
            //Debug.Log("min: " + min + ", min2" + min2, this);

            bool maintainRoadWidth = false; // TODO: refactor
            if (maintainRoadWidth)
            {
                float proposedWidth = (min - min2).magnitude;
                float correction = this.Width / proposedWidth;
                Vector3 center = Vector3.Lerp(min, min2, 0.5f);
                min = (min - center) * correction + center;
                min2 = (min2 - center) * correction + center;
            }

            Mesh mesh = GetComponent<MeshFilter>().mesh;
            Vector3[] verts = mesh.vertices;
            if (atHd)
            {
                if ((verts[0] - min).sqrMagnitude < (verts[0] - min2).sqrMagnitude)
                {
                    verts[0] = min;
                    verts[1] = min2;
                }
                else
                {
                    verts[0] = min2;
                    verts[1] = min;
                } 
            }
            else
            {
                if ((verts[verts.Length - 1] - min).sqrMagnitude < (verts[verts.Length - 1] - min2).sqrMagnitude)
                {
                    verts[verts.Length - 1] = min;
                    verts[verts.Length - 2] = min2;
                }
                else
                {
                    verts[verts.Length - 1] = min2;
                    verts[verts.Length - 2] = min;
                }
            }
            mesh.vertices = verts;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
        }
    }
}

﻿////using UnityEngine;
////using System.Collections;
////using System.Collections.Generic;
////using System;
////using DDSim.Helpers;
////using DDSim.Settings;
////using DDSim.Settings.Enums;

////namespace DDSim
////{
////    //[Serializable]
////    //public class RoadSettings
////    //{
////    //    public Material Material;
////    //    public RoadType RoadType;
////    //    public float Width = 6;
////    //}

////    //public enum RoadType
////    //{
////    //    Unknown,
////    //    Aerialway,
////    //    Exit,
////    //    Ferry,
////    //    Highway,
////    //    Major_Road,
////    //    Minor_Road,
////    //    Path,
////    //    Piste,
////    //    Racetrack,
////    //    Rail,
////    //}


////    public class RoadFactory : BaseMapzenFactory
////    {
////        //public List<RoadSettings> RoadSettings;
////        [SerializeField]
////        protected RoadFactorySettings FactorySettings;
////        public bool MergeConnectedRoads;

////        //private Dictionary<Vector3, List<Road>> RoadHds;
////        //private Dictionary<Vector3, List<Road>> RoadTls;

////        private List<Pair<Road, bool>> allRoads;
////        private Dictionary<Vector3, List<Pair<Road, int>>> allRoadPoints;

////        private object dataLock = new object();

////        private void Start()
////        {
////            //RoadHds = new Dictionary<Vector3, List<Road>>();
////            //RoadTls = new Dictionary<Vector3, List<Road>>();

////            allRoads = new List<Pair<Road, bool>>();
////            allRoadPoints = new Dictionary<Vector3, List<Pair<Road, int>>>();
////        }

////        //public void AddRoadOrMerge(Vector3 key, Road value, bool hd)
////        //{
////        //    Dictionary<Vector3, List<Road>> FactoryData = hd ? RoadHds : RoadTls;
////        //    Dictionary<Vector3, List<Road>> OtherFactoryData = !hd ? RoadHds : RoadTls;
////        //    if (FactoryData.ContainsKey(key))
////        //    {
////        //        List<Road> curValue = FactoryData.GetData(key);
////        //        foreach (Road r in curValue)
////        //        {
////        //            if (r.CanMerge(value))
////        //            {
////        //                Road newRoad = Road.Merge(r, value);
////        //                curValue.Remove(r);

////        //                List<Road> otherValue;
////        //                if (r.ToMercator(r.Hd()) == key)
////        //                {
////        //                    otherValue = RoadTls.GetData(r.ToMercator(r.Tl()));
////        //                }
////        //                else
////        //                {
////        //                    otherValue = RoadHds.GetData(r.ToMercator(r.Hd()));
////        //                }
////        //                otherValue.Remove(r);
////        //                Destroy(r);
////        //                Destroy(value);
////        //                AddRoadOrMerge(newRoad.ToMercator(newRoad.Hd()), newRoad, true);
////        //                AddRoadOrMerge(newRoad.ToMercator(newRoad.Tl()), newRoad, false);
////        //                return;
////        //            }
////        //        }
////        //        curValue.Add(value);
////        //    }
////        //    else
////        //    {
////        //        List<Road> newVal = new List<Road>();
////        //        newVal.Add(value);
////        //        FactoryData.Add(key, newVal);
////        //    }
////        //}

////        private void AddRoad(Road road)
////        {
////            for (int i = 0; i < road.RoadPoints.Length; i++)
////            {
////                Vector3 p = road.ToMercator(road.RoadPoints[i]);
////                if (allRoadPoints.ContainsKey(p))
////                {
////                    List<Pair<Road, int>> roads = allRoadPoints.GetData(p);
////                    foreach (Pair<Road, int> roadP in roads)
////                    {
////                        if (road.CanIntersect(roadP.First))
////                        {
////                            // Determine roads at intersection
////                            List<Road> iRoads = new List<Road>();
////                            foreach (Pair<Road, int> rp in roads)
////                            {
////                                // Split if point at Vector3 p is not endpoint
////                                if (rp.Second == 0 || rp.Second == rp.First.RoadPoints.Length)
////                                {
////                                    iRoads.Add(rp.First);
////                                }
////                                else
////                                {
////                                    Pair<Road> r2 = rp.First.Split(rp.Second);
////                                    iRoads.Add(r2.First);
////                                    iRoads.Add(r2.Second);

////                                    // Remove old road from every entry, and add in new roads as appropriate
////                                    for (int j = 0; j < rp.First.RoadPoints.Length; j++)
////                                    {
////                                        Vector3 p2 = rp.First.ToMercator(rp.First.RoadPoints[j]);
////                                        List<Pair<Road, int>> roads2 = allRoadPoints.GetData(p);
////                                        roads2.Remove(rp);
////                                        if (j < r2.First.RoadPoints.Length)
////                                        {
////                                            roads2.Add(new Pair<Road, int>(r2.First, j));
////                                        }
////                                        else
////                                        {
////                                            roads2.Add(new Pair<Road, int>(r2.Second, j - r2.First.RoadPoints.Length));
////                                        }

////                                    }
////                                }
////                            }

////                            // Modify roads, and make intersection (but do not merge/split roads)
////                            Intersection output = Intersection.MakeIntersection(iRoads);

////                            // Add intersection to allRoadPoints somehow (leave roads, don't add intersection, but instead add check for pre-existing intersection before making new one?)
////                        }

////                    }
////                    foreach (Pair<Road, int> roadP in roads)
////                    {
////                        if (road.CanMerge(roadP.First))
////                        {

////                        }
////                    }
////                    roads.Add(new Pair<Road, int>(road, i));
////                }
////                else
////                {
////                    List<Pair<Road, int>> roads = new List<Pair<Road, int>>();
////                    roads.Add(new Pair<Road, int>(road, i));
////                    allRoadPoints.Add(p, roads);
////                }
////            }
////        }

////        public void LogRoads()
////        {
////            foreach (List<Pair<Road, int>> roads in allRoadPoints.Values)
////            {
////                Debug.Log(roads);
////            }
////        }

////        public void AddRoadOrMerge(Road road)
////        {
////            Road r;
////            for (int i = 0; i < allRoads.Count; i++)
////            {
////                r = allRoads[i].First;
////                if (allRoads[i].Second && r.CanMerge(road))
////                {
////                    Road newRoad;
////                    //lock (allRoads[i])
////                    //{
////                    //if (!(allRoads[i].Second && r.CanMerge(road))) continue; 
////                    newRoad = Road.Merge(r, road);
////                    allRoads[i].Second = false;
////                    //}
////                    AddRoadOrMerge(newRoad);
////                    Destroy(r.gameObject);
////                    Destroy(road.gameObject);
////                    return;
////                }
////            }
////            allRoads.Add(new Pair<Road, bool>(road, true));
////        }

////        public override string MapzenLayer()
////        {
////            return "roads";
////        }

////        private bool isSingleRoad(JSONObject data)
////        {
////            return data["geometry"]["type"].str == "LineString";
////        }

////        private bool isMultiRoad(JSONObject data)
////        {
////            return data["geometry"]["type"].str == "MultiLineString";
////        }

////        public override bool Makeable(JSONObject data)
////        {
////            return isSingleRoad(data) || isMultiRoad(data);
////        }

////        public override IEnumerable<MonoBehaviour> Make(Tile tile, JSONObject data)
////        {
////            if (isSingleRoad(data))
////            {
////                return MakeSingleRoad(tile, data);
////            }
////            else if (isMultiRoad(data))
////            {
////                return MakeMultiRoad(tile, data);
////            }
////            else
////            {
////                throw new NotImplementedException();
////            }
////        }

////        private string getUniqueKey(Tile tile, JSONObject data)
////        {
////            return tile.Tms.First + "_" + tile.Tms.Second + "-" + data["properties"]["id"].ToString();
////        }

////        //private string getID(JSONObject data)
////        //{
////        //    return data["properties"]["id"].ToString();
////        //}

////        //private string getKind(JSONObject data)
////        //{
////        //    return data["properties"]["kind"].ToString();
////        //}

////        private IEnumerable<MonoBehaviour> MakeSingleRoad(Tile tile, JSONObject data)
////        {
////            RoadSettings typeSettings = FactorySettings.GetSettingsFor<RoadSettings>(data["properties"]["kind"].ToString());

////            RoadBuilder rBuilder = new RoadBuilder(true)
////                .SetName("road-" + getUniqueKey(tile, data))
////                .SetDescription(data.ToString())
////                .SetWidth(typeSettings.Width, true)
////                .SetMaterial(typeSettings.Material)
////                .SetProperties(data["properties"])
////                .SetTile(tile);
////            for (var i = 0; i < data["geometry"]["coordinates"].list.Count; i++)
////            {
////                JSONObject c = data["geometry"]["coordinates"][i];
////                Vector2 p = CoordinateConverter.LatLon2Mercator(c[1].f, c[0].f);
////                Vector3 localp = new Vector3(p.x, 0, p.y) - tile.TileCenter;
////                rBuilder.AddPoint(localp);
////            }
////            Road road = rBuilder.Build(true);
////            if (MergeConnectedRoads)
////            {
////                //AddRoadOrMerge(road.ToMercator(road.Hd()), road, true);
////                //AddRoadOrMerge(road.ToMercator(road.Tl()), road, false);
////                AddRoadOrMerge(road);
////            }
////            TrackRoad(road);
////            yield return road;
////        }

////        private IEnumerable<MonoBehaviour> MakeMultiRoad(Tile tile, JSONObject data)
////        {
////            RoadSettings typeSettings = FactorySettings.GetSettingsFor<RoadSettings>(data["properties"]["kind"].ToString());

////            RoadBuilder rBuilder = new RoadBuilder(true)
////                .SetWidth(typeSettings.Width, true)
////                .SetMaterial(typeSettings.Material)
////                .SetProperties(data["properties"])
////                .SetTile(tile);
////            for (var i = 0; i < data["geometry"]["coordinates"].list.Count; i++)
////            {
////                JSONObject c = data["geometry"]["coordinates"][i];
////                RoadBuilder rb = rBuilder.Clone()
////                    .SetName("road-" + getUniqueKey(tile, data) + "-" + i)
////                    .SetDescription(c.ToString());
////                for (var j = 0; j < c.list.Count; j++)
////                {
////                    JSONObject seg = c[j];
////                    Vector2 p = CoordinateConverter.LatLon2Mercator(seg[1].f, seg[0].f);
////                    Vector3 localp = new Vector3(p.x, 0, p.y) - tile.TileCenter;
////                    rb.AddPoint(localp);
////                }
////                Road road = rb.Build(true);
////                if (MergeConnectedRoads)
////                {
////                    //AddRoadOrMerge(road.ToMercator(road.Hd()), road, true);
////                    //AddRoadOrMerge(road.ToMercator(road.Tl()), road, false);
////                    AddRoadOrMerge(road);
////                }
////                TrackRoad(road);
////                yield return road;
////            }
////        }
////    }
////}






//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System;
//using DDSim.Helpers;
//using DDSim.Settings;
//using DDSim.Settings.Enums;
//using System.Linq;

//namespace DDSim
//{
//    //[Serializable]
//    //public class RoadSettings
//    //{
//    //    public Material Material;
//    //    public RoadType RoadType;
//    //    public float Width = 6;
//    //}

//    //public enum RoadType
//    //{
//    //    Unknown,
//    //    Aerialway,
//    //    Exit,
//    //    Ferry,
//    //    Highway,
//    //    Major_Road,
//    //    Minor_Road,
//    //    Path,
//    //    Piste,
//    //    Racetrack,
//    //    Rail,
//    //}


//    public class RoadFactory : BaseMapzenFactory
//    {
//        //public List<RoadSettings> RoadSettings;
//        [SerializeField]
//        protected RoadFactorySettings FactorySettings;
//        public bool MergeConnectedRoads;

//        //private Dictionary<Vector3, List<Road>> RoadHds;
//        //private Dictionary<Vector3, List<Road>> RoadTls;

//        private List<Pair<Road, bool>> allRoads;
//        private Dictionary<Vector3, List<RoadBase>> allRoadPoints;

//        private object dataLock = new object();

//        private void Start()
//        {
//            //RoadHds = new Dictionary<Vector3, List<Road>>();
//            //RoadTls = new Dictionary<Vector3, List<Road>>();

//            allRoads = new List<Pair<Road, bool>>();
//            allRoadPoints = new Dictionary<Vector3, List<RoadBase>>();
//        }

//        //public void AddRoadOrMerge(Vector3 key, Road value, bool hd)
//        //{
//        //    Dictionary<Vector3, List<Road>> FactoryData = hd ? RoadHds : RoadTls;
//        //    Dictionary<Vector3, List<Road>> OtherFactoryData = !hd ? RoadHds : RoadTls;
//        //    if (FactoryData.ContainsKey(key))
//        //    {
//        //        List<Road> curValue = FactoryData.GetData(key);
//        //        foreach (Road r in curValue)
//        //        {
//        //            if (r.CanMerge(value))
//        //            {
//        //                Road newRoad = Road.Merge(r, value);
//        //                curValue.Remove(r);

//        //                List<Road> otherValue;
//        //                if (r.ToMercator(r.Hd()) == key)
//        //                {
//        //                    otherValue = RoadTls.GetData(r.ToMercator(r.Tl()));
//        //                }
//        //                else
//        //                {
//        //                    otherValue = RoadHds.GetData(r.ToMercator(r.Hd()));
//        //                }
//        //                otherValue.Remove(r);
//        //                Destroy(r);
//        //                Destroy(value);
//        //                AddRoadOrMerge(newRoad.ToMercator(newRoad.Hd()), newRoad, true);
//        //                AddRoadOrMerge(newRoad.ToMercator(newRoad.Tl()), newRoad, false);
//        //                return;
//        //            }
//        //        }
//        //        curValue.Add(value);
//        //    }
//        //    else
//        //    {
//        //        List<Road> newVal = new List<Road>();
//        //        newVal.Add(value);
//        //        FactoryData.Add(key, newVal);
//        //    }
//        //}

//        private void replaceRoadAtPoint(Road rOld, Road rNew, Vector3 v)
//        {
//            List<RoadBase> roadables = allRoadPoints.GetData(v);
//            if (rOld != null)
//            {
//                roadables.Remove(rOld);
//                foreach (RoadBase rb in roadables.FindAll(rb => rb is Intersection))
//                {
//                    ((Intersection)rb).ReplaceRoad(rOld, rNew);
//                }
//            }
//            if (rNew != null)
//            {
//                roadables.Add(rNew);
//            }
//        }

//        // Assumes Vector3 p is in mercator.
//        // Splits every roadable at p, removes old entries from allRoadPoints and adds new ones
//        private void splitAndUpdate(Road r, Vector3 p)
//        {
//            bool pAtEndpoint = (p == r.ToMercator(r.Hd()) || p == r.ToMercator(r.Tl()));
//            if (!pAtEndpoint)
//            {
//                Pair<Road> rr = r.Split(r.FromMercator(p));

//                // Remove old road from every entry, and add in new roads as appropriate
//                for (int i = 0; i < r.RoadPoints.Length; i++)
//                {
//                    Vector3 p2 = r.ToMercator(r.RoadPoints[i]);
//                    if (i < rr.First.RoadPoints.Length)
//                    {
//                        replaceRoadAtPoint(r, rr.First, p2);
//                    }
//                    else
//                    {
//                        replaceRoadAtPoint(r, rr.Second, p2);
//                    }

//                }
//                Destroy(r.gameObject);
//            }
//        }

//        private void splitAndUpdateAll(List<RoadBase> roadables, Vector3 v)
//        {
//            List<Road> roads = roadables.Cast<Road>().ToList();
//            foreach (Road r in roads)
//            {
//                lock (r)
//                {
//                    splitAndUpdate(r, v);
//                }
//            }
//        }

//        // Assumes 'a' is already in allRoadPoints while 
//        // newRoad is not. Returns merged Road
//        private Road mergeAndUpdate(Road a, Road newRoad)
//        {
//            Road mergedRoad = Road.Merge(a, newRoad);
//            foreach (Vector3 p in a.RoadPoints)
//            {
//                Vector3 p2 = a.ToMercator(p);
//                replaceRoadAtPoint(a, mergedRoad, p2);
//            }

//            //foreach (Vector3 p in newRoad.RoadPoints)
//            //{
//            //    Vector3 p2 = newRoad.ToMercator(p);
//            //    replaceRoadAtPoint(null, mergedRoad, p2);
//            //}

//            return mergedRoad;
//        }

//        private void AddRoad(Road newRoad)
//        {
//            Road road = newRoad;
//            int stop = road.RoadPoints.Length;
//            int delta = 1;
//            for (int i = 0; (delta > 0) ? i < stop : i > stop; i += delta)
//            {
//                //string debugstr = "i: " + i + ", stop: " + stop + ", delta: " + delta;
//                //Debug.Log(debugstr);

//                Vector3 p = road.ToMercator(road.RoadPoints[i]);
//                if (allRoadPoints.ContainsKey(p))
//                {
//                    List<RoadBase> roadables = allRoadPoints.GetData(p);
//                    lock (roadables)
//                    {
//                        bool hasIntersection = roadables.Exists(rb => rb is Intersection);
//                        if (hasIntersection)
//                        {
//                            bool pAtEndpoint = (p == road.ToMercator(road.Hd()) || p == road.ToMercator(road.Tl()));
//                            if (pAtEndpoint)
//                            {
//                                roadables.Add(road);
//                            }
//                            else
//                            {
//                                lock (road)
//                                {
//                                    Pair<Road> rr = road.Split(road.FromMercator(p));
//                                    roadables.Add(rr.First);
//                                    roadables.Add(rr.Second);

//                                    int start = (delta > 0) ? 0 : road.RoadPoints.Length - 1;
//                                    for (int j = start; j != i; j += delta)
//                                    {
//                                        Vector3 p2 = road.ToMercator(road.RoadPoints[j]);
//                                        replaceRoadAtPoint(road, (delta > 0) ? rr.First : rr.Second, p2);
//                                    }
//                                    Destroy(road.gameObject);

//                                    // hacks, should refactor
//                                    if (delta > 0)
//                                    {
//                                        road = rr.Second;
//                                        i = 0;
//                                        stop = road.RoadPoints.Length;
//                                    }
//                                    else
//                                    {
//                                        road = rr.First;
//                                        i = road.RoadPoints.Length - 1;
//                                        stop = -1;
//                                    }
//                                }
//                            }
//                        }
//                        else if (IntersectionBuilder.CanMakeIntersection(roadables, road))
//                        {
//                            splitAndUpdateAll(roadables, p);
//                            bool pAtEndpoint = (p == road.ToMercator(road.Hd()) || p == road.ToMercator(road.Tl()));
//                            if (pAtEndpoint)
//                            {
//                                roadables.Add(road);
//                            }
//                            else
//                            {
//                                lock (road)
//                                {
//                                    Pair<Road> rr = road.Split(road.FromMercator(p));
//                                    roadables.Add(rr.First);
//                                    roadables.Add(rr.Second);

//                                    int start = (delta > 0) ? 0 : road.RoadPoints.Length - 1;
//                                    for (int j = start; j != i; j += delta)
//                                    {
//                                        Vector3 p2 = road.ToMercator(road.RoadPoints[j]);
//                                        replaceRoadAtPoint(road, (delta > 0) ? rr.First : rr.Second, p2);
//                                    }
//                                    Destroy(road.gameObject);

//                                    // hacks, should refactor
//                                    if (delta > 0)
//                                    {
//                                        road = rr.Second;
//                                        i = 0;
//                                        stop = road.RoadPoints.Length;
//                                    }
//                                    else
//                                    {
//                                        road = rr.First;
//                                        i = road.RoadPoints.Length - 1;
//                                        stop = -1;
//                                    }
//                                }
//                            }

//                            List<Road> iRoads = roadables.Cast<Road>().ToList();

//                            // Modify roads, and make intersection (but do not merge/split roads)
//                            Intersection output = new IntersectionBuilder()
//                                .SetName("INTERSECTION")
//                                .SetPoint(p)
//                                .AddRoads(iRoads)
//                                .Build(true);

//                            // Add intersection to allRoadPoints somehow (leave roads, don't add intersection, but instead add check for pre-existing intersection before making new one?)
//                            roadables.Add(output);
//                        }
//                        else
//                        {
//                            bool atEndPoint = i == 0 || i == stop - delta;
//                            if (!atEndPoint) continue;
//                            foreach (RoadBase roadB in roadables)
//                            {
//                                lock (roadB)
//                                {
//                                    if (roadB == null) continue; //TODO: FIX ASYNC MERGING ISSUE (make merge modify instead of create new?)

//                                    Road rb = (Road)roadB;
//                                    lock (rb)
//                                    {
//                                        if (roadB is Road && road.CanMerge(rb) && rb != road)
//                                        {
//                                            //Road r = (Road)roadB;
//                                            //for (int j = 0; j < r.RoadPoints.Length; j++)
//                                            //{
//                                            //    if (!allRoadPoints.ContainsKey(r.ToMercator(r.RoadPoints[j])))
//                                            //    {
//                                            //        Debug.LogWarning("DOESN'T HAVE INDEX " + j + " OF " + (r.RoadPoints.Length - 1));
//                                            //    }
//                                            //}

//                                            try
//                                            {
//                                                Road mergedRoad = mergeAndUpdate(rb, road);
//                                                Pair<int> help = mergedRoad.FindWhere(road);
//                                                int start = help.First;
//                                                int direction = help.Second;
//                                                stop = start + direction * road.RoadPoints.Length;
//                                                delta *= direction;
//                                                i = (p == road.ToMercator(road.Hd())) ? start : stop;
//                                                Destroy(road.gameObject);
//                                                Destroy(rb.gameObject);
//                                                road = mergedRoad;
//                                                break;
//                                            }
//                                            catch (ArgumentException)
//                                            {
//                                                Debug.LogError("AHHH, unable to merge " + rb + " and " + road);
//                                            }




//                                            //int i0 = -1, icur = -1, istop = -1;
//                                            //Vector3 p0 = road.ToMercator(road.Hd());
//                                            //Vector3 pStop = road.ToMercator(road.Tl());
//                                            //for (int j = 0; j < mergedRoad.RoadPoints.Length; j++)
//                                            //{
//                                            //    Vector3 pCur = mergedRoad.ToMercator(mergedRoad.RoadPoints[j]);
//                                            //    if (pCur == p)
//                                            //    {
//                                            //        icur = j;
//                                            //    }
//                                            //    else if (pCur == p0)
//                                            //    {
//                                            //        i0 = j;
//                                            //    }
//                                            //    else if (pCur == pStop)
//                                            //    {
//                                            //        istop = j;
//                                            //    }
//                                            //}
//                                            //if (i0 == -1) { Debug.LogWarning(roadPointsToString(road.RoadPoints) + "\n " + roadPointsToString(mergedRoad.RoadPoints)); }
//                                            //Debug.LogWarning("i0: " + i0 + ", istop: " + istop + ", icur: " + icur);

//                                            //delta = Math.Sign(icur - i0);
//                                            //if (delta == 0) { throw new UnityException("DELTA IS 0!!!!!!!!!!"); }
//                                            //i = icur;
//                                            //stop = istop + delta;
//                                            //road = mergedRoad;
//                                            //break;
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//                else
//                {
//                    List<RoadBase> roadables = new List<RoadBase>();
//                    roadables.Add(road);
//                    allRoadPoints.Add(p, roadables);
//                }
//            }
//        }

//        private string roadPointsToString(Vector3[] rps)
//        {
//            return String.Join("; ", rps.Select(x => x.ToString()).ToArray());
//        }
//        //private void AddRoad(Road newRoad)
//        //{
//        //    Road road = newRoad;
//        //    for (int i = 0; i < road.RoadPoints.Length; i++)
//        //    {
//        //        Vector3 p = road.ToMercator(road.RoadPoints[i]);
//        //        if (allRoadPoints.ContainsKey(p))
//        //        {
//        //            List<RoadBase> roadables = allRoadPoints.GetData(p);
//        //            lock (roadables)
//        //            {
//        //                bool hasIntersection = roadables.Exists(rb => rb is Intersection);
//        //                if (hasIntersection)
//        //                {
//        //                    bool pAtEndpoint = (p == road.ToMercator(road.Hd()) || p == road.ToMercator(road.Tl()));
//        //                    if (pAtEndpoint)
//        //                    {
//        //                        roadables.Add(road);
//        //                    }
//        //                    else
//        //                    {
//        //                        Pair<Road> rr = road.Split(p);
//        //                        roadables.Add(rr.First);
//        //                        roadables.Add(rr.Second);

//        //                        for (int j = 0; j < i; j++)
//        //                        {
//        //                            Vector3 p2 = road.ToMercator(road.RoadPoints[j]);
//        //                            replaceRoadAtPoint(road, rr.First, p2);
//        //                        }
//        //                        Destroy(road.gameObject);
//        //                        road = rr.Second; i = 0;  // hacks, should refactor
//        //                    }
//        //                }
//        //                else if (IntersectionBuilder.CanMakeIntersection(roadables, road))
//        //                {
//        //                    splitAndUpdateAll(roadables, p);
//        //                    bool pAtEndpoint = (p == road.ToMercator(road.Hd()) || p == road.ToMercator(road.Tl()));
//        //                    if (pAtEndpoint)
//        //                    {
//        //                        roadables.Add(road);
//        //                    }
//        //                    else
//        //                    {
//        //                        Pair<Road> rr = road.Split(p);
//        //                        roadables.Add(rr.First);
//        //                        roadables.Add(rr.Second);

//        //                        for (int j = 0; j < i; j++)
//        //                        {
//        //                            Vector3 p2 = road.ToMercator(road.RoadPoints[j]);
//        //                            replaceRoadAtPoint(road, rr.First, p2);
//        //                        }
//        //                        Destroy(road.gameObject);
//        //                        road = rr.Second; i = 0;  // hacks, should refactor
//        //                    }

//        //                    List<Road> iRoads = roadables.Cast<Road>().ToList();

//        //                    // Modify roads, and make intersection (but do not merge/split roads)
//        //                    Intersection output = new IntersectionBuilder()
//        //                        .SetName("INTERSECTION")
//        //                        .SetPoint(p)
//        //                        .AddRoads(iRoads)
//        //                        .Build(true);

//        //                    // Add intersection to allRoadPoints somehow (leave roads, don't add intersection, but instead add check for pre-existing intersection before making new one?)
//        //                    roadables.Add(output);
//        //                }
//        //                else
//        //                {
//        //                    foreach (RoadBase roadB in roadables)
//        //                    {
//        //                        if (roadB is Road && road.CanMerge((Road)roadB))
//        //                        {

//        //                        }
//        //                    }
//        //                }
//        //            }
//        //        }
//        //        else
//        //        {
//        //            List<RoadBase> roadables = new List<RoadBase>();
//        //            roadables.Add(road);
//        //            allRoadPoints.Add(p, roadables);
//        //        }
//        //    }
//        //}

//        public void LogRoads()
//        {
//            foreach (List<RoadBase> roads in allRoadPoints.Values)
//            {
//                Debug.Log(roads);
//            }
//        }

//        public void AddRoadOrMerge(Road road)
//        {
//            Road r;
//            for (int i = 0; i < allRoads.Count; i++)
//            {
//                r = allRoads[i].First;
//                if (allRoads[i].Second && r.CanMerge(road))
//                {
//                    Road newRoad;
//                    //lock (allRoads[i])
//                    //{
//                    //if (!(allRoads[i].Second && r.CanMerge(road))) continue; 
//                    newRoad = Road.Merge(r, road);
//                    allRoads[i].Second = false;
//                    //}
//                    AddRoadOrMerge(newRoad);
//                    Destroy(r.gameObject);
//                    Destroy(road.gameObject);
//                    return;
//                }
//            }
//            allRoads.Add(new Pair<Road, bool>(road, true));
//        }

//        public override string MapzenLayer()
//        {
//            return "roads";
//        }

//        private bool isSingleRoad(JSONObject data)
//        {
//            return data["geometry"]["type"].str == "LineString";
//        }

//        private bool isMultiRoad(JSONObject data)
//        {
//            return data["geometry"]["type"].str == "MultiLineString";
//        }

//        public override bool Makeable(JSONObject data)
//        {
//            return isSingleRoad(data) || isMultiRoad(data);
//        }

//        public override IEnumerable<MonoBehaviour> Make(Tile tile, JSONObject data)
//        {
//            if (isSingleRoad(data))
//            {
//                return MakeSingleRoad(tile, data);
//            }
//            else if (isMultiRoad(data))
//            {
//                return MakeMultiRoad(tile, data);
//            }
//            else
//            {
//                throw new NotImplementedException();
//            }
//        }

//        private string getUniqueKey(Tile tile, JSONObject data)
//        {
//            return tile.Tms.First + "_" + tile.Tms.Second + "-" + data["properties"]["id"].ToString();
//        }

//        //private string getID(JSONObject data)
//        //{
//        //    return data["properties"]["id"].ToString();
//        //}

//        //private string getKind(JSONObject data)
//        //{
//        //    return data["properties"]["kind"].ToString();
//        //}

//        private IEnumerable<MonoBehaviour> MakeSingleRoad(Tile tile, JSONObject data)
//        {
//            RoadSettings typeSettings = FactorySettings.GetSettingsFor<RoadSettings>(data["properties"]["kind"].ToString());

//            RoadBuilder rBuilder = new RoadBuilder(true)
//                .SetName("road-" + getUniqueKey(tile, data))
//                .SetDescription(data.ToString())
//                .SetWidth(typeSettings.Width, true)
//                .SetMaterial(typeSettings.Material)
//                .SetProperties(data["properties"])
//                .SetTile(tile);
//            for (var i = 0; i < data["geometry"]["coordinates"].list.Count; i++)
//            {
//                JSONObject c = data["geometry"]["coordinates"][i];
//                Vector2 p = CoordinateConverter.LatLon2Mercator(c[1].f, c[0].f);
//                Vector3 localp = new Vector3(p.x, 0, p.y) - tile.TileCenter;
//                rBuilder.AddPoint(localp);
//            }
//            Road road = rBuilder.Build(true);
//            if (MergeConnectedRoads)
//            {
//                //AddRoadOrMerge(road.ToMercator(road.Hd()), road, true);
//                //AddRoadOrMerge(road.ToMercator(road.Tl()), road, false);
//                AddRoad(road);
//            }
//            //TrackRoad(road);
//            yield return road;
//        }

//        private IEnumerable<MonoBehaviour> MakeMultiRoad(Tile tile, JSONObject data)
//        {
//            RoadSettings typeSettings = FactorySettings.GetSettingsFor<RoadSettings>(data["properties"]["kind"].ToString());

//            RoadBuilder rBuilder = new RoadBuilder(true)
//                .SetWidth(typeSettings.Width, true)
//                .SetMaterial(typeSettings.Material)
//                .SetProperties(data["properties"])
//                .SetTile(tile);
//            for (var i = 0; i < data["geometry"]["coordinates"].list.Count; i++)
//            {
//                JSONObject c = data["geometry"]["coordinates"][i];
//                RoadBuilder rb = rBuilder.Clone()
//                    .SetName("road-" + getUniqueKey(tile, data) + "-" + i)
//                    .SetDescription(c.ToString());
//                for (var j = 0; j < c.list.Count; j++)
//                {
//                    JSONObject seg = c[j];
//                    Vector2 p = CoordinateConverter.LatLon2Mercator(seg[1].f, seg[0].f);
//                    Vector3 localp = new Vector3(p.x, 0, p.y) - tile.TileCenter;
//                    rb.AddPoint(localp);
//                }
//                Road road = rb.Build(true);
//                if (MergeConnectedRoads)
//                {
//                    //AddRoadOrMerge(road.ToMercator(road.Hd()), road, true);
//                    //AddRoadOrMerge(road.ToMercator(road.Tl()), road, false);
//                    AddRoad(road);
//                }
//                //TrackRoad(road);
//                yield return road;
//            }
//        }
//    }
//}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DDSim {
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Intersection : RoadBase
    {
        private List<Road> Roads;
        public Vector3d v; // intersection center in mercator
        public List<Vector3> IntersectionPoints; // vertices used for mesh
        public bool Fake; // TODO: HACKS, FIX

        public bool MoveRoadsAway; // TODO: make argument to AddRoad?

        public void ReplaceRoad(Road rOld, Road rNew)
        {
            this.Roads.Remove(rOld);
            AddRoad(rNew);
        }

        public bool Contains(Transform transform)
        {
            Vector3[] verts = GetComponent<MeshFilter>().mesh.vertices;
            Vector3 pLocal = transform.position - this.transform.position;
            Vector2 p = new Vector2(pLocal.x, pLocal.z);
            bool oldContains = verts.Convert2Vector2().ToArray().ContainsPoint(p);
            return GetComponent<MeshFilter>().mesh.ContainsXZPoint(p) || oldContains;
        }

        public bool AtHd(Road r)
        {
            double distHd = (r.ToMercator(r.Hd()) - v).sqrMagnitude;
            double distTl = (r.ToMercator(r.Tl()) - v).sqrMagnitude;
            bool atHd = distHd < distTl;
            return atHd;
        }

        // TODO: should intersections be immutable?
        public void AddRoad(Road r)
        {
            this.Roads.Add(r);
            bool atHd = AtHd(r);

            //if (!this.Fake)
            if (this.MoveRoadsAway)
            {       
                //r.MoveEndpointBack(r.Width / 2.0f, atHd);
                r.MoveForIntersection(this.IntersectionPoints, atHd);
            }

            r.AddIntersection(this, atHd);
        }
        public void AddRoads(IEnumerable<Road> roads)
        {
            this.Roads = new List<Road>();
            foreach (Road r in roads)
            {
                this.AddRoad(r);
            }
        }

        private Road randomRoad(List<Road> roads)
        {
            if (roads.Count == 1) { return roads[0]; }
            //this.Roads[UnityEngine.Random.Range(0, this.Roads.Count)];
            List<float> roadWeights = roads.Select(r => r.Width * r.DistanceCovered).ToList();

            float totalProb = roadWeights.Sum();
            float randomNum = UnityEngine.Random.Range(0f, totalProb);

            float cumProb = 0f;
            int i;
            for (i = 0; cumProb < randomNum; i++)
            {
                cumProb += roadWeights[i];
            }
            //Debug.Log("i: " + i + "\n" + roads.PrettyString() + "\n" + roadWeights.PrettyString());
            return roads[i - 1];
        }

        public Road GetRandomRoad(Road notThisOne = null, bool mustBeDriveable = false)
        {
            IEnumerable<Road> newRoads = this.Roads.Where(r => r != notThisOne);
            IEnumerable<Road> driveableNewRoads = newRoads.Where(r => r.Driveable || !mustBeDriveable);
            if (driveableNewRoads.Any())
            {
                return randomRoad(driveableNewRoads.ToList());
            }
            return randomRoad(newRoads.ToList());
        }

    }
}

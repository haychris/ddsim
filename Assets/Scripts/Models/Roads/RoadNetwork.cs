﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System;
using DDSim.Helpers;
using DDSim.Settings;

namespace DDSim
{
    public class RoadNetwork
    {
        public const bool DebugPrint = false;
        private bool debug;

        IntersectionBuilderSettings intersectionSettings;

        private Dictionary<Vector3d, List<RoadBase>> rp2Roadables;
        private Dictionary<Tile, HashSet<Vector3d>> tile2Rps;

        private Dictionary<Tile, HashSet<Vector3d>> periphery;
        private Dictionary<Tile, HashSet<Tile>> waitingOn;
        private HashSet<Tile> finishedMainProcessing;
        private int processingPeriphery = -1;

        private Dictionary<Tile, HashSet<Road>> tile2Roads;

        public RoadNetwork(IntersectionBuilderSettings intersectionSettings, bool debug = false)
        {
            this.intersectionSettings = intersectionSettings;

            rp2Roadables = new Dictionary<Vector3d, List<RoadBase>>();
            tile2Rps = new Dictionary<Tile, HashSet<Vector3d>>();

            periphery = new Dictionary<Tile, HashSet<Vector3d>>();
            waitingOn = new Dictionary<Tile, HashSet<Tile>>();
            finishedMainProcessing = new HashSet<Tile>();

            tile2Roads = new Dictionary<Tile, HashSet<Road>>();
            this.debug = debug;
        }

        public Road NearestRoad(Transform transform)
        {
            Tile tile = World.GetTile(transform);
            if (tile == null)
            {
                return null;
            }
            //HashSet<Road> roads = tile2Roads.GetData(tile);
            foreach (Road r in GetRoadsForTile(tile))
            {
                if (r.Contains(transform))
                {
                    return r;
                }
            }
            return null;
        }

        public IEnumerable<Road> GetRoadsForTile(Tile tile)
        {
            return tile2Roads.GetData(tile);
        }

        private Vector3d roundV(Vector3d v)
        {
            int dec = 4;
            return new Vector3d(Math.Round(v.x, dec),
                                Math.Round(v.y, dec),
                                Math.Round(v.z, dec));
        }

        private void addRoadPoint(Vector3d v, Road road)
        {
            // Add roadpoint to road mapping
            Vector3d roundedV = roundV(v);
            if (rp2Roadables.ContainsKey(roundedV))
            {
                List<RoadBase> roadables = rp2Roadables.GetData(roundedV);
                roadables.Add(road);
            }
            else
            {
                List<RoadBase> roadables = new List<RoadBase>();
                roadables.Add(road);
                rp2Roadables.Add(roundedV, roadables);
            }

            // add tile to roadpoint mapping
            if (tile2Rps.ContainsKey(road.Tile))
            {
                HashSet<Vector3d> rps = tile2Rps.GetData(road.Tile);
                rps.Add(roundedV);
            }
            else
            {
                HashSet<Vector3d> rps = new HashSet<Vector3d>();
                rps.Add(roundedV);
                tile2Rps.Add(road.Tile, rps);
            }
        }

        public void AddRoad(Road road)
        {
            foreach (Vector3 p in road.RoadPoints)
            {
                addRoadPoint(road.ToMercator(p), road);
            }
        }

        public IEnumerable ProcessTile(Tile tile)
        {
            HashSet<Vector3d> rps = tile2Rps.GetData(tile);
            foreach (Vector3d v in rps)
            {
                processRoadPoint(v);
            }

            if (!tile2Roads.ContainsKey(tile))
            {
                tile2Roads.Add(tile, new HashSet<Road>());
            }
            HashSet<Road> roads = tile2Roads.GetData(tile);
            foreach (Vector3d v in rps)
            {
                foreach (Road r in rp2Roadables.GetData(v).Where(rb => rb is Road))
                {
                    roads.Add(r);
                }
            }

            finishedMainProcessing.Add(tile);

            // process periphery after waiting on relevent tiles
            if (periphery.ContainsKey(tile))
            {
                // if nobody is processing periphery and waitinOn tiles have finished normal processing
                // claim the periphery and process
                HashSet<Tile> waiting = waitingOn.GetData(tile);
                String debugstr = "Tile: " + tile + " finished waiting on " + waiting.Count + ": " + String.Join(", ", waiting.Select(tl => tl.name).ToArray());
                while (!waiting.All(tl => finishedMainProcessing.Contains(tl)) || processingPeriphery >= 0)
                {
                    yield return new WaitForSeconds(0.1f);
                }
                processingPeriphery = (tile.Tms.First % 100)*100 + (tile.Tms.Second % 100);

                Debug.Log(debugstr, tile);
                HashSet<Vector3d> rps2 = periphery.GetData(tile);
                foreach (Vector3d v in rps2)
                {
                    processRoadPoint(v, true);
                }

                processingPeriphery = -1;
            }
        }

        private void processRoadPoint(Vector3d v, bool force = false)
        {
            List<RoadBase> roadables = rp2Roadables.GetData(v);
            bool hasIntersection = roadables.Exists(rb => rb is Intersection);
            List<Tile> tilesPresent = roadables.Select(rb => rb.Tile).Distinct().ToList();
            bool mixedTiles = tilesPresent.Count > 1;

            if (mixedTiles && !force)
            {
                // TODO: FIX HACKS AHHHHHHHHH
                Intersection output = new IntersectionBuilder(false, this.debug)
                    .SetName("FAKE INTERSECTION")
                    .MakeFake()
                    .SetPoint(v)
                    .AddRoads(roadables.Cast<Road>().ToList())
                    .Build(true);
                roadables.Add(output);
                //Debug.LogWarning("MIXED TILES: " + String.Join(", ", tilesPresent.Select(tl => tl.name).ToArray()) + " AHHHHH, SKIPPING " + v);
                //for (int i = 0; i < tilesPresent.Count; i++)
                //{
                //    Tile a = tilesPresent[i];
                //    if (!periphery.ContainsKey(a))
                //    {
                //        periphery.Add(a, new HashSet<Vector3d>());
                //    }
                //    HashSet<Vector3d> tilePeriphery = periphery.GetData(a);
                //    tilePeriphery.Add(v);

                //    if (!waitingOn.ContainsKey(a))
                //    {
                //        waitingOn.Add(a, new HashSet<Tile>());
                //    }
                //    HashSet<Tile> tileDependsA = waitingOn.GetData(a);

                //    for (int j = i + 1; j < tilesPresent.Count; j++)
                //    {
                //        Tile b = tilesPresent[j];
                //        if (!waitingOn.ContainsKey(b))
                //        {
                //            waitingOn.Add(b, new HashSet<Tile>());
                //        }
                //        HashSet<Tile> tileDependsB = waitingOn.GetData(b);
                //        tileDependsA.Add(b);
                //        tileDependsB.Add(a);

                //        //Debug.Log(a + " depends on " + tileDependsA.Count, a);
                //        //Debug.Log(b + " depends on " + tileDependsB.Count, b);
                //    }
                //}
            }
            else if (hasIntersection)
            {
                Debug.LogWarning("ALREADY HAS INTERSECTION, AHHHHHHHH " + v);
                // Should only happen when merging between tiles
            }
            else if (IntersectionBuilder.CanMakeIntersection(roadables))
            {
                List<Road> roads = roadables.Cast<Road>().ToList();
                foreach (Road r in roads)
                {
                    splitRoadIfNeededAndReplace(r, v);
                }

                List<Road> iRoads = roadables.Cast<Road>().ToList();

                IntersectionBuilder ib = new IntersectionBuilder(false, this.debug)
                    .SetName("intersection-" + String.Join("__", iRoads.Select(r => r.name).ToArray()))
                    .SetPoint(v)
                    .AddRoads(iRoads);
                if (intersectionSettings.HasSettingsFor(iRoads[0].Type))
                {
                    ResourceAndPositioning a = intersectionSettings.GetSettingsFor(iRoads[0].Type).GetRandomResource();
                    ib.AddStopObject(a, iRoads[0].Width);
                }
                Intersection intersect = ib.Build(true);
                roadables.Add(intersect);
            }
            else
            {
                mergeAllIfNeeded(roadables);
            }
        }

        private void replaceRoad(Road rOld, Road rNew, bool atOld)
        {
            if (DebugPrint)
            {
                String debugstr = "Replacing " + rOld.name + " with " + rNew.name + " at " + ((atOld) ? "first" : "second");
                Debug.Log(debugstr, (atOld) ? rOld : rNew);
            }
            List<Vector3d> roundedVs;
            if (atOld)
            {
                roundedVs = rOld.RoadPoints.Select(rp => roundV(rOld.ToMercator(rp))).ToList();
            }
            else
            {
                roundedVs = rNew.RoadPoints.Select(rp => roundV(rNew.ToMercator(rp))).ToList();
            }


            foreach (Vector3d roundedV in roundedVs)
            {
                List<RoadBase> roadables = rp2Roadables.GetData(roundedV);
                roadables.Remove(rOld);
                foreach (RoadBase rb in roadables.FindAll(rb => rb is Intersection))
                {
                    ((Intersection)rb).ReplaceRoad(rOld, rNew);
                }
                roadables.Add(rNew);
            }
            UnityEngine.Object.Destroy(rOld.gameObject);
        }

        private bool atEndpoint(Road r, Vector3d v)
        {
            double tolerance = 0.01 / Mathd.DoublePrecision;
            Vector3d vHd = r.ToMercator(r.Hd());
            Vector3d vTl = r.ToMercator(r.Tl());
            bool atHd = Vector3d.ApproximatelyFixed(v, vHd, tolerance);
            bool atTl = Vector3d.ApproximatelyFixed(v, vTl, tolerance);
            return atHd || atTl;
        }

        private Pair<Road> splitRoadIfNeededAndReplace(Road road, Vector3d v)
        {
            if (DebugPrint)
            {
                String debugstr = "Splitting if needed " + road + " at " + v;
                Debug.Log(debugstr);
            }
            if (atEndpoint(road, v)) return new Pair<Road>(road, null);

            Pair<Road> rr = road.Split(road.FromMercator(v));
            replaceRoad(road, rr.First, false);
            replaceRoad(road, rr.Second, false);
            return rr;
        }

        private void mergeRoadsAndReplace(Road a, Road b)
        {
            if (DebugPrint)
            {
                String debugstr = "Merging " + a + b;
                Debug.Log(debugstr);
            }
            Road mergedRoad = Road.Merge(a, b);
            replaceRoad(a, mergedRoad, true);
            replaceRoad(b, mergedRoad, true);
        }

        private void mergeAllIfNeeded(List<RoadBase> roadables)
        {
            if (DebugPrint)
            {
                String debugstr = "Attempting merge for " + roadables.Count;
                Debug.Log(debugstr);
            }
            for (int i = 0; i < roadables.Count; i++)
            {
                Road a = (Road)roadables[i];
                for (int j = i + 1; j < roadables.Count; j++)
                {
                    Road b = (Road)roadables[j];
                    if (a.CanMerge(b))
                    {
                        mergeRoadsAndReplace(a, b);
                        mergeAllIfNeeded(roadables);
                    }
                }
            }
        }
    }
}

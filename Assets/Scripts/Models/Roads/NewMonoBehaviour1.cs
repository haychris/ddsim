﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.Threading;
//using System.Linq;
//using System;

//namespace DDSim
//{
//    public class RoadNetwork
//    {
//        private Dictionary<Vector3d, List<RoadBase>> rpToRoadables;
//        private Dictionary<Road, bool> banList;
//        private Dictionary<Road, Road> roadLookup;

//        public RoadNetwork()
//        {
//            rpToRoadables = new Dictionary<Vector3d, List<RoadBase>>();
//            banList = new Dictionary<Road, bool>();
//            roadLookup = new Dictionary<Road, Road>();
//        }

//        // Removes rOld and adds rNew from/to the entry for v in rpToRoadables
//        // Only removes/adds if non-null
//        private void removeOldAddNew(Vector3d v, Road rOld, Road rNew)
//        {
//            if (banList.ContainsKey(rOld) || banList.ContainsKey(rNew)) { Debug.LogWarning("Attempting to remove banned roads"); return; }

//            Vector3d roundedV = roundV(v);
//            if (!rpToRoadables.ContainsKey(roundedV))
//            {
//                Debug.LogError(rOld.Tile + " " + rNew.Tile + "\n" + rOld.name + " " + rNew.name);
//                Debug.LogError("v: " + v + "rounded v: " + roundedV + "\n"
//                + "[" + String.Join("; ", rOld.RoadPoints.Select(x => rOld.ToMercator(x).ToString("F4")).ToArray()) + "]" + "\n"
//                + "[" + String.Join("; ", rNew.RoadPoints.Select(x => rNew.ToMercator(x).ToString("F4")).ToArray()) + "]");
//            }
//            List<RoadBase> roadables = rpToRoadables.GetData(roundedV);
//            lock (roadables)
//            {
//                if (rOld != null)
//                {
//                    roadables.Remove(rOld);
//                    foreach (RoadBase rb in roadables.FindAll(rb => rb is Intersection))
//                    {
//                        // TODO: need lock on intersection?
//                        ((Intersection)rb).ReplaceRoad(rOld, rNew);
//                    }
//                }
//                if (rNew != null)
//                {
//                    roadables.Add(rNew);
//                }
//            }
//        }

//        // Replaces rOld with rNew in the entry for v in rpToRoadables.
//        // Returns true if successfully replaced, returns false o.w.
//        private bool replaceRoadPoint(Vector3d v, Road rOld, Road rNew)
//        {
//            if (banList.ContainsKey(rOld) || banList.ContainsKey(rNew)) { Debug.LogWarning("Attempting to replace banned roads"); return false; }

//            Vector3d roundedV = roundV(v);
//            if (!rpToRoadables.ContainsKey(roundedV)) return false;

//            List<RoadBase> roadables = rpToRoadables.GetData(roundedV);
//            lock (roadables)
//            {
//                if (roadables.Contains(rOld))
//                {
//                    roadables.Remove(rOld);
//                    foreach (RoadBase rb in roadables.FindAll(rb => rb is Intersection))
//                    {
//                        // TODO: need lock on intersection?
//                        ((Intersection)rb).ReplaceRoad(rOld, rNew);
//                    }
//                    roadables.Add(rNew);
//                    return true;
//                }
//                else
//                {
//                    return false;
//                }
//            }
//        }

//        private Vector3d roundV(Vector3d v)
//        {
//            int dec = 4;
//            return new Vector3d(Math.Round(v.x, dec),
//                                Math.Round(v.y, dec),
//                                Math.Round(v.z, dec));
//        }
//        // TODO
//        // TODO 
//        // TODO: ADD BATCH PROCESSING TO ADD ALL ROADS IN A TILE, THEN MERGE/SPLIT/ADD INTERSECTIONS
//        // TODO: ADD GENERIC POST-PROCESSING FUNCTION FOR FACTORIES, WHICH IS CALLED FOR EVERY FACTORY
//        // TODO:    AFTER FINISHING BUILDING EVERY FACTORY FOR A GIVEN TILE
//        // TODO
//        // TODO
//        private Road addRoadPoint(Vector3d v, Road road)
//        {
//            Vector3d roundedV = roundV(v);
//            if (!road.RoadPoints.Contains(road.FromMercator(v))) return road;

//            lock (road)
//            {
//                Monitor.Enter(rpToRoadables); // lock rpToRoadables incase two threads try to add v for the first time
//                if (rpToRoadables.ContainsKey(roundedV))
//                {
//                    Monitor.Exit(rpToRoadables); // immediately unlock once we're in the clear

//                    List<RoadBase> roadables = rpToRoadables.GetData(roundedV);
//                    lock (roadables)
//                    {
//                        bool hasIntersection = roadables.Exists(rb => rb is Intersection);
//                        if (hasIntersection)
//                        {
//                            Road split = splitRoadIfNeeded(road, v);
//                            roadables.Add(split);
//                            return split;
//                        }
//                        else if (IntersectionBuilder.CanMakeIntersection(roadables, road))
//                        {
//                            List<Road> roads = roadables.Cast<Road>().ToList();
//                            foreach (Road r in roads)
//                            {
//                                splitRoadIfNeeded(r, v);
//                            }
//                            Road cur = splitRoadIfNeeded(road, v);
//                            roadables.Add(cur);

//                            List<Road> iRoads = roadables.Cast<Road>().ToList();
//                            Intersection output = new IntersectionBuilder()
//                                    .SetName("INTERSECTION")
//                                    .SetPoint(v)
//                                    .AddRoads(iRoads)
//                                    .Build(true);
//                            roadables.Add(output);
//                            return cur;
//                        }
//                        else
//                        {
//                            Road cur = road;

//                            Road test = (Road)roadables.Find(rb => (rb is Road) && road.Tile != rb.Tile);
//                            if (test != null)
//                            {
//                                Debug.LogWarning("Tile diff merge? : " +
//                                    road.name + ", v: " + v + ", " + road.ToMercator(road.Hd()) + ", " + road.ToMercator(road.Tl()) + "\n"
//                                  + test.name + ", v: " + v + ", " + test.ToMercator(test.Hd()) + ", " + test.ToMercator(test.Tl()));
//                            }

//                            Road b = (Road)roadables.Find(rb => (rb is Road) && road.CanMerge((Road)rb));
//                            if (b != null)
//                            {
//                                lock (b)
//                                {
//                                    cur = mergeRoads(b, road);
//                                }
//                            }

//                            roadables.Add(cur);
//                            return cur;
//                        }
//                    }
//                }
//                else
//                {
//                    List<RoadBase> roadables = new List<RoadBase>();
//                    roadables.Add(road);
//                    rpToRoadables.Add(roundedV, roadables);
//                    Monitor.Exit(rpToRoadables);
//                    return road;
//                }
//            }
//        }

//        private Road mergeRoads(Road a, Road b)
//        {
//            a.Acquire(); b.Acquire();
//            if (banList.ContainsKey(a) || banList.ContainsKey(b)) { Debug.LogWarning("Attempting to merge banned roads"); a.Release(); b.Release(); return a; }
//            if (a == b) { Debug.LogWarning("Attempting to merge self"); a.Release(); b.Release(); return a; }

//            Debug.Log("Merging: " + a.name + ", " + b.name);

//            lock (a)
//            {
//                lock (b)
//                {
//                    if (!a.CanMerge(b)) { Debug.LogWarning("Attempted merge, no longer able to"); a.Release(); b.Release(); return b; }


//                    Road mergedRoad = Road.Merge(a, b);

//                    lock (mergedRoad)
//                    {
//                        foreach (Vector3 v in a.RoadPoints)
//                        {
//                            removeOldAddNew(a.ToMercator(v), a, mergedRoad);
//                            //replaceRoadPoint(a.ToMercator(v), a, mergedRoad);
//                        }
//                        banList.Add(a, true);
//                        roadLookup.Add(a, mergedRoad);
//                        UnityEngine.Object.Destroy(a.gameObject);

//                        // replace roadpoints in b that were added pre-merge
//                        // TODO: make faster by finding way to remove roadables.Contains() 
//                        // check and by not bothering to attempt those not yet added
//                        foreach (Vector3 v in b.RoadPoints)
//                        {
//                            replaceRoadPoint(b.ToMercator(v), b, mergedRoad);
//                        }
//                        banList.Add(b, true);
//                        roadLookup.Add(b, mergedRoad);
//                        UnityEngine.Object.Destroy(b.gameObject);

//                        a.Release(); b.Release();
//                        return mergedRoad;
//                    }
//                }
//            }
//        }

//        // TODO: check for merge at ends after splitting?
//        private Road splitRoadIfNeeded(Road r, Vector3d v)
//        {
//            if (banList.ContainsKey(r)) { Debug.LogWarning("Attempting to split banned road"); return r; }

//            lock (r)
//            {
//                //bool pAtEndpoint = (v == r.ToMercator(r.Hd()) || v == r.ToMercator(r.Tl()));
//                //TODO: fix, hacks
//                bool pAtEndpoint = (Vector3d.ApproximatelyFixed(v, r.ToMercator(r.Hd()), 0.01 / Mathd.DoublePrecision) || Vector3d.ApproximatelyFixed(v, r.ToMercator(r.Tl()), 0.01 / Mathd.DoublePrecision));
//                //                if (r.name.Contains("road-19177_24727-297987939__road-19177_24727-297987939"))
//                //                { Debug.LogWarning(r.name + ", v: " + v + ", " + r.ToMercator(r.Hd()) + ", " + r.ToMercator(r.Tl()));
//                //Debug.LogWarning("" + Vector3d.ApproximatelyFixed(v, r.ToMercator(r.Tl()), 0.001/Mathd.DoublePrecision) + "" + Vector3d.ApproximatelyFixed(v, r.ToMercator(r.Tl()), 0.01 / Mathd.DoublePrecision) +"" + Vector3d.ApproximatelyFixed(v, r.ToMercator(r.Tl()), 0.1 / Mathd.DoublePrecision));
//                //                }
//                if (pAtEndpoint) return r;

//                string rpstr = "[" + String.Join("; ", r.RoadPoints.Select(x => x.ToString()).ToArray()) + "]";
//                string rpstr2 = "[" + String.Join("; ", r.RoadPoints.Select(x => r.ToMercator(x).ToString()).ToArray()) + "]";
//                Debug.Log("Splitting: " + rpstr2 + "at " + v + "\n" + rpstr, r);

//                Pair<Road> rr = r.Split(r.FromMercator(v));
//                int firstReplaced = 0;
//                int secondReplaced = 0;

//                lock (rr.First)
//                {
//                    lock (rr.Second)
//                    {
//                        // Remove old road from every entry, and add in new roads as appropriate
//                        for (int i = 0; i < r.RoadPoints.Length; i++)
//                        {
//                            Vector3d v2 = r.ToMercator(r.RoadPoints[i]);
//                            if (i < rr.First.RoadPoints.Length)
//                            {
//                                bool replaced = replaceRoadPoint(v2, r, rr.First);
//                                firstReplaced += (replaced) ? 1 : 0;
//                            }
//                            else
//                            {
//                                bool replaced = replaceRoadPoint(v2, r, rr.Second);
//                                secondReplaced += (replaced) ? 1 : 0;
//                            }
//                        }
//                        banList.Add(r, true);
//                        UnityEngine.Object.Destroy(r.gameObject);
//                        return (firstReplaced >= secondReplaced) ? rr.Second : rr.First;
//                    }
//                }
//            }
//        }

//        //private void addIntersection(Vector3 v, List<Road> iRoads)
//        //{
//        //    Intersection output = new IntersectionBuilder()
//        //                        .SetName("INTERSECTION")
//        //                        .SetPoint(v)
//        //                        .AddRoads(iRoads)
//        //                        .Build(true);
//        //}

//        public void AddRoad(Road road)
//        {
//            List<Road> lockedRoads = new List<Road>();
//            //road.Acquire();
//            lockedRoads.Add(road);

//            if (road.name.Contains("61598710"))
//            { Debug.LogWarning(road.name + ", " + road.ToMercator(road.Hd()) + ", " + road.ToMercator(road.Tl())); }
//            Vector3d[] points = road.RoadPoints.Select(v => road.ToMercator(v)).ToArray();
//            foreach (Vector3d v in points)
//            {
//                Debug.Log(road.name);
//                if (banList.ContainsKey(road)) return;
//                Road newRoad = addRoadPoint(v, road);
//                if (newRoad != road)
//                {
//                    //newRoad.Acquire();
//                    lockedRoads.Add(road);
//                }
//                road = newRoad;
//            }
//            foreach (Road r in lockedRoads)
//            {
//                //road.Release();
//            }
//        }
//    }
//}

﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DDSim
{
    public class Tile : MonoBehaviour
    {
        public delegate void DestroyedEventHandler(Tile sender, EventArgs e);
        public event DestroyedEventHandler Destroyed;

        public JSONObject Data { get; set; }
        public Dictionary<string, Dictionary<string, MonoBehaviour>> objectData;
        [SerializeField] public RectD Rect;
        //public int Zoom { get; set; }
        public Pair<int> Tms { get; set; }
        public Vector3d TileCenter { get; set; }
        public bool UseLayers { get; set; }
        public Material Material { get; set; }
        public void OnDestroy()
        {
            if (Destroyed != null)
            {
                Destroyed(this, null);
            }
        }

    }
}

﻿using UnityEngine;
using System.Collections;

namespace DDSim
{
    public abstract class TiledMonoBehavior : MonoBehaviour
    {
        public Tile Tile;
        public Vector3d ToMercator(Vector3 p)
        {
            return new Vector3d(p) + Tile.TileCenter;
        }

        public Vector3 FromMercator(Vector3d p)
        {
            return (Vector3)(p - Tile.TileCenter);
        }

        public Vector3 TileDiff(TiledMonoBehavior other)
        {
            return (Vector3)(other.Tile.TileCenter - this.Tile.TileCenter);
        }
    }
}

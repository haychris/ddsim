﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using UniRx;
using DDSim.Helpers;

namespace DDSim
{
    public class World : MonoBehaviour
    {
        [SerializeField]
        public float Latitude = 40.35047f;
        [SerializeField]
        public float Longitude = -74.65945f;
        [SerializeField]
        public int TileRange = 3;
        [HideInInspector]
        public const int Zoom = 16;
        [SerializeField]
        protected string _key = "mapzen-Uc5ujkW"; // May need to create your own

        protected readonly string MAPZEN_URL = "http://tile.mapzen.com/mapzen/vector/v1/{0}/{1}/{2}/{3}.{4}?api_key={5}";
        protected string _mapzenLayers;
        protected readonly string MAPZEN_FORMAT = "json";

        public string RelativeCachePath = "../CachedMapzenData/zoom_{0}/";
        protected string CacheFolderPath;

        protected static Dictionary<Pair<int>, Tile> TMSTileDict; // TODO: needed?
        private List<BaseFactory> _factories;


        private static bool initialized;
        private static World INSTANCE;

        // Use this for initialization
        void Start()
        {
            if (!initialized)
            {
                Initialize();
            }
        }

        public void Initialize()
        {
            INSTANCE = this;
            initialized = true;

            findFactories();
            findLayers();

#if UNITY_ANDROID || UNITY_IPHONE
            CacheFolderPath = Path.Combine(Application.persistentDataPath, RelativeCachePath);
#else
            CacheFolderPath = Path.Combine(Application.dataPath, RelativeCachePath);
#endif
            CacheFolderPath = string.Format(CacheFolderPath, Zoom);
            if (!Directory.Exists(CacheFolderPath))
                Directory.CreateDirectory(CacheFolderPath);

            TMSTileDict = new Dictionary<Pair<int>, Tile>();


            Build();
        }

        private static IEnumerator waitUntilInitialized()
        {
            yield return new WaitUntil(() => initialized);
        }

        public static World GetInstance()
        {
            if (!initialized)
            {
                waitUntilInitialized().ToYieldInstruction();
            }
            return INSTANCE;
        }

        // Update is called once per frame
        void Update()
        {

        }

        public string GetMapzenURL(Pair<int> tms, int zoom)
        {
            return string.Format(MAPZEN_URL, _mapzenLayers, Zoom, tms.First, tms.Second, MAPZEN_FORMAT, _key);
        }
        public string GetTileCachePath(Pair<int> tms)
        {
            return Path.Combine(CacheFolderPath, _mapzenLayers.Replace(',', '_') + "_" + tms.First + "_" + tms.Second);
        }

        private void orderFactoriesHelper(BaseFactory factory, List<BaseFactory> orderedFactories)
        {
            if (orderedFactories.Contains(factory))
            {
                return;
            }

            if (factory.GetFactoryDependencies() == null)
            {
                orderedFactories.Add(factory);
                return;
            }

            foreach (BaseFactory fac in factory.GetFactoryDependencies())
            {
                if (!orderedFactories.Contains(fac))
                {
                    orderFactoriesHelper(fac, orderedFactories);
                }
            }
            orderedFactories.Add(factory);
        }

        private List<BaseFactory> orderFactories(List<BaseFactory> factories)
        {
            List<BaseFactory> orderedFactories = new List<BaseFactory>();

            foreach (BaseFactory factory in factories)
            {
                orderFactoriesHelper(factory, orderedFactories);
            }

            return orderedFactories;
        }

        private void findFactories()
        {
            List<BaseFactory> factories = new List<BaseFactory>(GetComponentsInChildren<BaseFactory>());

            _factories = orderFactories(factories);
            Debug.Log("Processing factories in following order:\n" + _factories.PrettyString());
        }

        private void findLayers()
        {
            var layers = new List<string>();
            foreach (var factory in _factories.OfType<BaseMapzenFactory>())
            {
                string layer = factory.MapzenLayer();
                if (layers.Contains(layer)) continue;
                layers.Add(layer);
            }
            _mapzenLayers = string.Join(",", layers.ToArray());
        }

        private IEnumerable<Pair<int>> spiralTms(Pair<int> tms, int range)
        {
            int width = 2 * range + 1;

            int x = 0;
            int y = 0;
            int dx = 0;
            int dy = -1;
            for (int i = 0; i < width * width; i++)
            {
                yield return new Pair<int>(tms.First + x, tms.Second + y);

                if (x == y || (x < 0 && x == -y) || (x > 0 && x == 1 - y))
                {
                    int tmp = dx;
                    dx = -dy;
                    dy = tmp;
                }
                x += dx;
                y += dy;
            }
        }

        protected void LoadTiles(Pair<int> tms, int tmsRange)
        {
            foreach (var v in spiralTms(tms, tmsRange))
            {
                if (TMSTileDict.ContainsKey(v))
                    continue;

                Tile tile = new TileBuilder()
                    .SetOriginTMS(tms)
                    .SetTMS(v)
                    .SetFactories(_factories)
                    .SetZoom(Zoom)
                    .Build();
                TMSTileDict.Add(v, tile);
            }
        }

        public static Tile GetTile(Transform transform)
        {
            World world = GetInstance();
            Vector2 pOrigin = new Vector2(world.Latitude, world.Longitude);
            Pair<int> centerTMS = CoordinateConverter.LatLon2TMS(pOrigin, Zoom);
            Vector2d offset = CoordinateConverter.TMSBoundsInMercator(centerTMS, Zoom).center;

            Vector3 p = transform.position;
            Vector2d m = new Vector2d(p.x, p.y) + offset;
            Pair<int> tms = CoordinateConverter.Mercator2TMS(m, World.Zoom);
            return GetTile(tms);
        }

        public static Tile GetTile(Pair<int> tms)
        {
            if (!TMSTileDict.ContainsKey(tms))
            {
                return null;
            }
            return TMSTileDict.GetData(tms);
        }

        protected void Build()
        {
            Vector2 p = new Vector2(Latitude, Longitude);
            Pair<int> centerTMS = CoordinateConverter.LatLon2TMS(p, Zoom);
            LoadTiles(centerTMS, TileRange);
            GetComponent<ReloadAfterTime>().StartTimer();
        }

        public void OnDestroy()
        {
            initialized = false;
            INSTANCE = null;
        }
    }
}

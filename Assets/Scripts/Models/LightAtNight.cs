﻿using UnityEngine;
using System.Collections;

public class LightAtNight : MonoBehaviour
{
    private DayNightController timeController;
    private Light[] lights;
    private bool lightsOn;
    // Use this for initialization
    void Start()
    {
        timeController = FindObjectOfType<DayNightController>();
        lights = GetComponentsInChildren<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!timeController) return;

        bool night = (timeController.currentTime >= 18.0f || timeController.currentTime <= 5.5f);
        if (night)
        {
            if (!lightsOn)
            {
                foreach (Light light in lights)
                {
                    light.enabled = true;
                }
                lightsOn = true;
            }
        }
        else
        {
            if (lightsOn)
            {
                foreach (Light light in lights)
                {
                    light.enabled = false;
                }
                lightsOn = false;
            }
        }
    }
}

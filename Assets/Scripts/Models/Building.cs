﻿using UnityEngine;
using System.Collections;
using DDSim.Settings.Enums;

namespace DDSim
{
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class Building : TiledMonoBehavior
    {

        public BuildingType Type;
        public string MapzenID;
        [TextArea(3, 10)] public string Description;
        public string BuildingName;
        public float Height;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DDSim.Settings.Enums;


namespace DDSim
{
    public class RoadBuilder
    {
        public static float UV_ADJUSTMENT = 0.1f;
        public static float AVERAGE_LANE_WIDTH = 5f;

        // IF ADDING NEW PROPERTY, NEED TO ADD LOGIC TO UnBuild()
        // Road properties
        public string name; // gameobject name
        private string mapzenId;
        private string kind;
        private RoadType type;
        //private string type;
        private float sortRank;
        private string streetName;
        private bool driveable;
        private float width;
        private float halfWidth;
        private int numLanes;
        private string description;
        private List<Vector3> roadPoints;
        private Material material;
        private Tile tile;

        // Mesh properties
        MeshData md;
        float uv;

        // Builder properties
        bool buildEagerly;
        bool debug;

        public RoadBuilder(bool buildEagerly = false, bool debug = false)
        {
            roadPoints = new List<Vector3>();
            md = new MeshData();
            this.buildEagerly = buildEagerly;
            this.debug = debug;
        }

        // Creates a new RoadBuilder and copies each instance variable for this builder
        public RoadBuilder Clone()
        {
            RoadBuilder clone = (RoadBuilder) this.MemberwiseClone();
            clone.md = new MeshData();
            clone.roadPoints = new List<Vector3>();
            foreach (Vector3 p in this.roadPoints)
            {
                clone.AddPoint(p);
            }
            return clone;
        }

        public RoadBuilder SetName(string name)
        {
            this.name = name;
            return this;
        }

        public RoadBuilder SetType(RoadType type)
        {
            this.type = type;
            return this;
        }

        public RoadBuilder SetWidth(float width, bool guessNumLanes = false)
        {
            this.width = width;
            this.halfWidth = width / 2.0f;

            if (guessNumLanes)
            {
                int numLanes = Math.Max((int)(width / AVERAGE_LANE_WIDTH), 1);
                SetNumLanes(numLanes);
            }
            return this;
        }

        public RoadBuilder SetNumLanes(int numLanes)
        {
            this.numLanes = numLanes;
            return this;
        }

        public RoadBuilder SetDescription(string description)
        {
            this.description = description;
            return this;
        }

        public RoadBuilder SetDriveable(bool driveable)
        {
            this.driveable = driveable;
            return this;
        }

        public RoadBuilder SetMaterial(Material material)
        {
            this.material = material;
            return this;
        }

        public RoadBuilder SetMapzenID(string mapzenID)
        {
            this.mapzenId = mapzenID;
            return this;
        }

        public RoadBuilder SetTile(Tile tile)
        {
            this.tile = tile;
            return this;
        }

        public RoadBuilder SetProperties(JSONObject properties)
        {
            this.SetMapzenID(properties["id"].ToString());
            this.kind = properties["kind"].ToString();
            this.sortRank = properties["sort_rank"].f;
            if (properties.HasField("name"))
            {
                this.streetName = properties["name"].str;
            }
            return this;
        }

        // Get the normal for the transition between (cur - prev) and (next - cur)
        private Vector3 getNormal(Vector3 prev, Vector3 cur, Vector3 next)
        {
            Vector3 a = (prev - cur).normalized;
            Vector3 b = (next - cur).normalized;
            Vector3 p = (b - a).normalized;
            Vector3 norm = new Vector3(-p.z, 0f, p.x);

            // correction allows road to maintain constant width for previous segment
            float angle = Vector3.Angle(a, norm);
            float correction = 1f / Mathf.Sin(angle * Mathf.PI / 180f); 
            return norm * correction;
        }

        // TODO: check if clock is needed
        // Creates triangles for the most recent 4 vertices by adding the relevant indices
        private void addLatestVertices()
        {
            int v = this.md.Vertices.Count - 1;
            this.md.Indices.Add(v - 3);
            this.md.Indices.Add(v - 1);
            this.md.Indices.Add(v - 2);

            this.md.Indices.Add(v - 2);
            this.md.Indices.Add(v - 1);
            this.md.Indices.Add(v);
        }

        public void makeIncrementalMeshBuild()
        {
            int n = this.roadPoints.Count - 1;
            if (n > 0)
            {
                Vector3 cur = this.roadPoints[n];
                Vector3 prev = this.roadPoints[n - 1];

                // If greater than 2 points, need to rewind previous one
                if (n > 1)
                {
                    this.md.Vertices.RemoveRange(this.md.Vertices.Count - 2, 2);
                    this.md.UV.RemoveRange(this.md.UV.Count - 2, 2);
                    this.md.Indices.RemoveRange(this.md.Indices.Count - 6, 6);

                    // TODO: make width larger to account for angle
                    Vector3 betterNorm = getNormal(this.roadPoints[n - 2], prev, cur) * this.halfWidth;
                    this.md.Vertices.Add(prev + betterNorm);
                    this.md.Vertices.Add(prev - betterNorm);
                    this.md.UV.Add(new Vector2(0, this.uv));
                    this.md.UV.Add(new Vector2(1, this.uv));

                    addLatestVertices();
                }

                this.uv += Vector3.Distance(cur, prev) * UV_ADJUSTMENT;
                Vector3 tangent = (cur - prev).normalized;
                Vector3 norm = new Vector3(-tangent.z, 0f, tangent.x) * this.halfWidth;

                // Once we get the second point, we can place vertices 
                // for the first (since we need two to calculate the normal)
                if (n == 1)
                {
                    this.md.Vertices.Add(prev + norm);
                    this.md.Vertices.Add(prev - norm);
                    this.md.UV.Add(new Vector2(0, 0f));
                    this.md.UV.Add(new Vector2(1, 0f));
                }
                this.md.Vertices.Add(cur + norm);
                this.md.Vertices.Add(cur - norm);
                this.md.UV.Add(new Vector2(0, this.uv));
                this.md.UV.Add(new Vector2(1, this.uv));

                addLatestVertices();
            }
        }

        public void oneshotMeshBuild()
        {
            int n = this.roadPoints.Count - 1;
            Vector3 next;
            Vector3 cur = this.roadPoints[1];
            Vector3 prev = this.roadPoints[0];

            Vector3 tangent = (cur - prev).normalized;
            Vector3 norm = new Vector3(-tangent.z, 0f, tangent.x) * this.halfWidth;
            this.md.Vertices.Add(prev + norm);
            this.md.Vertices.Add(prev - norm);
            this.md.UV.Add(new Vector2(0, 0f));
            this.md.UV.Add(new Vector2(1, 0f));

            for (int i = 1; i <= n - 1; i++)
            {
                next = this.roadPoints[i + 1];
                cur = this.roadPoints[i];
                prev = this.roadPoints[i - 1];
                this.uv += Vector3.Distance(cur, prev) * UV_ADJUSTMENT;

                Vector3 betterNorm = getNormal(prev, cur, next) * this.halfWidth;
                this.md.Vertices.Add(cur + betterNorm);
                this.md.Vertices.Add(cur - betterNorm);
                this.md.UV.Add(new Vector2(0, this.uv));
                this.md.UV.Add(new Vector2(1, this.uv));

                addLatestVertices();
            }

            cur = this.roadPoints[n];
            prev = this.roadPoints[n - 1];
            this.uv += Vector3.Distance(cur, prev) * UV_ADJUSTMENT;

            tangent = (cur - prev).normalized;
            norm = new Vector3(-tangent.z, 0f, tangent.x) * this.halfWidth;
            this.md.Vertices.Add(cur + norm);
            this.md.Vertices.Add(cur - norm);
            this.md.UV.Add(new Vector2(0, this.uv));
            this.md.UV.Add(new Vector2(1, this.uv));
            addLatestVertices();
        }

        // TODO: make function to more efficiently add multiple points. 
        public RoadBuilder AddPoint(Vector3 p)
        {
            // TODO: FIX VECTORS TO USE MORE PRECISION
            // then roadPoints.Count > 0
            if (roadPoints.Count > 0 && p == roadPoints[roadPoints.Count - 1])
            {
                string rpstr = "[" + String.Join("; ", roadPoints.Select(x => x.ToString()).ToArray()) + "]";
                Debug.LogWarning("Trying to add duplicate road point, ignoring: " + p + "\n" + rpstr);
                return this;
            }
            this.roadPoints.Add(p);
            if (this.buildEagerly)
            {
                makeIncrementalMeshBuild();
            }

            return this;
        }

        public int GetNumPoints()
        {
            return this.roadPoints.Count;
        }

        // TODO: add vertices/triangles here if not pre-building
        private void buildMesh(Road road)
        {
            if (!this.buildEagerly)
            {
                oneshotMeshBuild();
            }
            Mesh mesh = road.GetComponent<MeshFilter>().mesh;
            mesh.vertices = this.md.Vertices.ToArray();
            mesh.triangles = this.md.Indices.ToArray();
            mesh.SetUVs(0, this.md.UV);
            mesh.RecalculateNormals();

            road.GetComponent<MeshRenderer>().material = this.material;
            road.GetComponent<MeshCollider>().sharedMesh = mesh;
            road.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }

        // Places a sphere on each roadpoint
        private void visualizeRoadPoints(Road road)
        {
            foreach (Vector3 p in this.roadPoints)
            {
                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                sphere.transform.SetParent(road.transform);
                sphere.transform.localPosition = road.transform.position + p + Vector3.up * 0.5f;
            }
        }

        // Builds the Road corresponding to the properties of this builder
        public Road Build(bool setParent = false)
        {
            if (roadPoints.Count < 2)
            {
                throw new InvalidOperationException("Cannot build Road with less than two road points: " + name + "; " + description);
            }
            Road road = new GameObject(this.name).AddComponent<Road>();
            road.Type = this.type;
            road.Driveable = this.driveable;
            road.Width = this.width;
            road.NumLanes = this.numLanes;
            road.MapzenID = this.mapzenId;
            road.Description = this.description;
            road.RoadPoints = this.roadPoints.ToArray();
            road.Tile = this.tile;
            buildMesh(road);
            road.DistanceCovered = this.uv / UV_ADJUSTMENT;

            road.transform.localPosition += Vector3.up * (float) this.sortRank / 10000f;

            // For debugging:
            road.Vertices = md.Vertices.ToArray();
            road.Triangles = md.Indices.ToArray();

            if (this.debug)
            {
                visualizeRoadPoints(road);
            }

            if (setParent)
            {
                road.transform.SetParent(this.tile.transform, false);
            }
            return road;
        }

        //public Road Build(Road road, bool setParent = false)
        //{

        //}


        public static RoadBuilder UnBuild(Road road, bool buildEagerly = false)
        {
            RoadBuilder rb = new RoadBuilder(buildEagerly)
                .SetName(road.gameObject.name)
                .SetType(road.Type)
                .SetDriveable(road.Driveable)
                .SetWidth(road.Width)
                .SetNumLanes(road.NumLanes)
                .SetMapzenID(road.MapzenID)
                .SetDescription(road.Description)
                .SetTile(road.Tile)
                .SetMaterial(road.GetComponent<MeshRenderer>().material);
            rb.sortRank = road.transform.localPosition.y * 10000f;
            foreach (Vector3 p in road.RoadPoints)
            {
                rb.AddPoint(p);
            }

            return rb;
        }

        public void ResetRoadPoints()
        {
            this.roadPoints = new List<Vector3>();
            this.md = new MeshData();
        }

        public void ReverseRoad()
        {
            List<Vector3> rPoints = this.roadPoints;
            rPoints.Reverse();
            ResetRoadPoints();
            foreach (Vector3 p in rPoints)
            {
                AddPoint(p);
            }
        }

        public Vector3 Hd()
        {
            return this.roadPoints[0];
        }

        public Vector3 Tl()
        {
            return this.roadPoints[this.roadPoints.Count - 1];
        }

        public void Append(RoadBuilder other, bool skipFirst = true)
        {
            //if (this.width != other.width)
            //{
            //    throw new ArgumentException("Cannot merge RoadBuilders with differing widths");
            //}
            //if (this.sortRank != other.sortRank)
            //{
            //    throw new ArgumentException("Cannot merge RoadBuilders with differing sortRanks");
            //}
            this.name += "__" + other.name;
            this.mapzenId += "__" + other.mapzenId;
            this.description += "\n\n" + other.description;
            this.buildEagerly = this.buildEagerly && other.buildEagerly;
            this.driveable = this.driveable && other.driveable;

            Vector3d tileDiff = other.tile.TileCenter - this.tile.TileCenter;
            Vector3 offset = new Vector3((float)tileDiff.x, (float)tileDiff.y, (float)tileDiff.z);

            for (int i = (skipFirst) ? 1 : 0; i < other.roadPoints.Count; i++)
            {
                this.AddPoint(other.roadPoints[i] + offset);
            }
        }
    }
}
﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using DDSim.Settings;

namespace DDSim
{
    public class IntersectionBuilder
    {
        public string name; // gameobject name
        private List<Road> roads;
        private Vector3d v; // position, in mercator
        private Material material;
        //private List<Vector3> intersectionPoints;

        private GameObject stopObjectPrefab;
        private float stopObjectMaxWidth;
        private int stopObjPosition; //TODO: make enum

        bool fake = false; // TODO: FIX, HACKS
        bool mixedIntersection;
        bool debug;

        // Builder properties
        bool buildEagerly;

        public IntersectionBuilder(bool buildEagerly = false, bool debug = false)
        {
            roads = new List<Road>();
            this.buildEagerly = buildEagerly;
            this.debug = debug;
        }

        // Creates a new IntersectionBuilder and copies each instance variable for this builder
        public IntersectionBuilder Clone()
        {
            IntersectionBuilder clone = (IntersectionBuilder)this.MemberwiseClone();
            clone.roads = new List<Road>();
            clone.AddRoads(roads);
            return clone;
        }

        public IntersectionBuilder SetName(string name)
        {
            this.name = name;
            return this;
        }

        public IntersectionBuilder SetPoint(Vector3d v)
        {
            this.v = v;
            return this;
        }

        public IntersectionBuilder AddRoad(Road r)
        {
            this.roads.Add(r);
            return this;
        }

        public IntersectionBuilder AddRoads(List<Road> rs)
        {
            rs.ForEach(r => this.AddRoad(r));
            return this;
        }

        public IntersectionBuilder SetMaterial(Material material)
        {
            this.material = material;
            return this;
        }

        public IntersectionBuilder MakeFake()
        {
            this.fake = true;
            return this;
        }

        public IntersectionBuilder AddStopObject(ResourceAndPositioning stopObjectPrefab, float maxWidth)
        {
            this.stopObjectPrefab = stopObjectPrefab.Obj;
            this.stopObjectMaxWidth = maxWidth;

            // TODO: move to ResourceAndPositioning
            float rand = UnityEngine.Random.Range(0f, 1f);
            float[] probs = new float[]{
                stopObjectPrefab.ProbFrontLeft,
                stopObjectPrefab.ProbFrontRight,
                stopObjectPrefab.ProbBackLeft,
                stopObjectPrefab.ProbBackRight,
            };


            float cumProb = 0f;
            int i;
            for (i = 0; cumProb < rand; i++)
            {
                cumProb += probs[i];
            }
            this.stopObjPosition = i - 1;
            return this;
        }

        private static void visualizeIntersectionCenter(Intersection intersect)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(intersect.transform);
            cube.transform.localPosition = Vector3.up * 0.5f;
            cube.transform.localScale *= 4f;
        }

        private bool intersectsAtHd(Road r)
        {
            double distHd = (r.ToMercator(r.Hd()) - v).sqrMagnitude;
            double distTl = (r.ToMercator(r.Tl()) - v).sqrMagnitude;
            bool atHd = distHd < distTl;
            return atHd;
        }

        private Pair<Road> getBestThroughway()
        {
            // TODO: replace with roads[0].IsConnected(roads[j]), and choose pair with max width ???
            // currently picks the pair of roads with the largest angle between them (filtering pairs w/differing widths)
            Pair<Road> best = new Pair<Road>(roads[0], roads[1]);
            float maxAngle = 0f;
            for (int i = 0; i < roads.Count; i++)
            {
                Vector3 a;
                if (intersectsAtHd(roads[i]))
                {
                    a = roads[i].RoadPoints[1] - roads[i].Hd();
                }
                else
                {
                    a = roads[i].RoadPoints[roads[i].RoadPoints.Length - 2] - roads[i].Tl();
                }
                for (int j = i + 1; j < roads.Count; j++)
                {
                    if (roads[i].Width != roads[j].Width) continue;
                    Vector3 b;
                    if (intersectsAtHd(roads[j]))
                    {
                        b = roads[j].RoadPoints[1] - roads[j].Hd();
                    }
                    else
                    {
                        b = roads[j].RoadPoints[roads[j].RoadPoints.Length - 2] - roads[j].Tl();
                    }
                    float angle = Vector3.Angle(a, b);
                    if (angle > maxAngle)
                    {
                        maxAngle = angle;
                        best = new Pair<Road>(roads[i], roads[j]);
                    }
                    
                }
            }

            return best;
        }

        private bool hasActualIntersection(Pair<Road> throughway)
        {
            if (throughway.First.CanIntersect(throughway.Second))
            {
                return true;
            }
            else
            {
                foreach (Road r in roads.Where(r => r != throughway.First && r != throughway.Second))
                {
                    if (r.CanIntersect(throughway.First) || r.CanIntersect(throughway.Second))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void moveRoadsAway(List<Vector3> endpoints, Pair<Road> throughway, bool moveThroughway)
        {
            foreach (Road r in roads.Where(r => moveThroughway || (r != throughway.First && r != throughway.Second)))
            {
                r.MoveForIntersection(endpoints, intersectsAtHd(r));
            }
        }

        private List<Vector3> getEndpoints(Pair<Road> throughway)
        {
            IEnumerable<float> widths = roads.Where(r => r != throughway.First && r != throughway.Second).Select(r => r.Width);
            float halfWidth = (widths.Any()) ? widths.Max() / 2f : throughway.First.Width / 2f;
            //float halfWidth = (this.mixedIntersection) ? roads.Select(r => r.Width).Min() / 2f : roads.Select(r => r.Width).Max() / 2f;
            List<Vector3> verts = new List<Vector3>();
            Vector3[] rVerts = throughway.First.GetComponent<MeshFilter>().mesh.vertices;
            if (intersectsAtHd(throughway.First))
            {
                Vector3 offset = (throughway.First.RoadPoints[1] - throughway.First.RoadPoints[0]).normalized * halfWidth;
                verts.Add(rVerts[0] + offset);
                verts.Add(rVerts[1] + offset);
            }
            else
            {
                Vector3 offset = (throughway.First.RoadPoints[throughway.First.RoadPoints.Length - 2] - throughway.First.RoadPoints[throughway.First.RoadPoints.Length - 1]).normalized * halfWidth;
                verts.Add(rVerts[rVerts.Length - 1] + offset);
                verts.Add(rVerts[rVerts.Length - 2] + offset);
            }

            Vector3[] rVerts2 = throughway.Second.GetComponent<MeshFilter>().mesh.vertices;
            if (intersectsAtHd(throughway.Second))
            {
                Vector3 offset = (throughway.Second.RoadPoints[1] - throughway.Second.RoadPoints[0]).normalized * halfWidth;
                verts.Add(rVerts2[0] + offset);
                verts.Add(rVerts2[1] + offset);
            }
            else
            {
                Vector3 offset = (throughway.Second.RoadPoints[throughway.Second.RoadPoints.Length - 2] - throughway.Second.RoadPoints[throughway.Second.RoadPoints.Length - 1]).normalized * halfWidth;
                verts.Add(rVerts2[rVerts2.Length - 1] + offset);
                verts.Add(rVerts2[rVerts2.Length - 2] + offset);
            }

            return verts;
        }

        private List<Vector2> convert2Vector2(List<Vector3> ps)
        {
            return ps.Select(p => new Vector2(p.x, p.z)).ToList();
        }

        private MeshData makeMeshData(List<Vector3> endpoints)
        {
            MeshData md = new MeshData();

            // Use the triangulator to get indices for creating triangles
            Triangulator tr = new Triangulator(convert2Vector2(endpoints).ToArray());
            md.Indices = tr.Triangulate().ToList();
            Vector3 p = getLocalCenter();
            md.Vertices = endpoints.Select(p1 => p1 - p).ToList();

            return md;
        }

        private void buildMesh(Intersection intersect, Pair<Road> throughway, List<Vector3> intersectionPoints)
        {
            MeshData md = makeMeshData(intersectionPoints);
            Mesh mesh = intersect.GetComponent<MeshFilter>().mesh;
            mesh.vertices = md.Vertices.ToArray();
            //md.Indices.Reverse();
            mesh.triangles = md.Indices.ToArray();
            //mesh.SetUVs(0, md.UV);
            mesh.RecalculateNormals();

            Material mat = (this.material != null) ? this.material : throughway.First.GetComponent<MeshRenderer>().material;
            intersect.GetComponent<MeshRenderer>().material = mat;
        }

        private void handleConstruction(Intersection intersect)
        {
            Pair<Road> bestThroughway = getBestThroughway();
            bool hasIntersectionMesh = hasActualIntersection(bestThroughway);

            List<Vector3> endpoints = getEndpoints(bestThroughway);
            if (hasIntersectionMesh)
            {
                moveRoadsAway(endpoints, bestThroughway, true);
                buildMesh(intersect, bestThroughway, endpoints);
                intersect.MoveRoadsAway = true;
                intersect.IntersectionPoints = endpoints;
            }
            else
            {
                moveRoadsAway(endpoints, bestThroughway, false);
            }
        }

        private Tile getTile()
        {
            return this.roads[0].Tile;
        }

        private Vector3 getLocalCenter()
        {
            return this.roads[0].FromMercator(v);
        }

        public void buildStopObjects(Intersection intersect)
        {
            if (this.stopObjectPrefab == null) return;

            foreach (Road r in this.roads.Where(r => r.Width <= this.stopObjectMaxWidth))
            {
                GameObject go = UnityEngine.Object.Instantiate(this.stopObjectPrefab, Vector3.zero, Quaternion.identity);
                if (go.GetComponent<StopObject>() == null)
                {
                    go.AddComponent<StopObject>();
                }
                StopObject stopObj = go.GetComponent<StopObject>();
                stopObj.transform.SetParent(intersect.transform, false);

                Vector3 left, right;
                Vector3[] rVerts = r.GetComponent<MeshFilter>().mesh.vertices;
                if (intersectsAtHd(r))
                {
                    left = rVerts[1];
                    right = rVerts[0];
                }
                else
                {
                    left = rVerts[rVerts.Length - 2];
                    right = rVerts[rVerts.Length - 1];
                }

                Vector3 middle = Vector3.Lerp(left, right, 0.5f);

                //Vector3 target = middle + r.transform.position;
                Vector3 target = middle;
                //Debug.Log("Looking at " + target + " from " + stopObj.transform.position, stopObj);
                stopObj.transform.LookAt(target);

                float offsetMultiplier = UnityEngine.Random.Range(1.1f, 1.7f);
                switch (this.stopObjPosition)
                {
                    case 0: // front left
                        stopObj.transform.localPosition += offsetMultiplier * (left - getLocalCenter());
                        break;
                    case 1: // front right
                        stopObj.transform.localPosition += offsetMultiplier * (right - getLocalCenter());
                        break;
                    case 2: // back left
                        stopObj.transform.localPosition += offsetMultiplier * (getLocalCenter() - right);
                        break;
                    case 3: // back right
                        stopObj.transform.localPosition += offsetMultiplier * (getLocalCenter() - left);
                        break;
                }
            }
        }


        // TODO: differentiate between Fake intersection and mix-intersections
        // TODO: clear space of smaller width roads in mix-intersections, currently causing path bugs
        public Intersection Build(bool setParent = false)
        {
            if (this.roads.Select(r => r.Width).Distinct().Count() > 1)
            {
                this.mixedIntersection = true;
            }

            Intersection intersect = new GameObject(this.name).AddComponent<Intersection>();
            if (this.mixedIntersection && this.fake)
            {
                Debug.LogWarning("FAKE AND MIXED, can cause issues with intersection mesh", intersect);
            }

            intersect.v = this.v;
            intersect.Tile = getTile();
            intersect.transform.localPosition = getLocalCenter();
            intersect.Fake = this.fake;

            handleConstruction(intersect);
            //intersect.IntersectionPoints = this.intersectionPoints;
            intersect.AddRoads(this.roads);

            if (!this.fake)
            {
                buildStopObjects(intersect);
            }

            if (this.debug)
            {
                visualizeIntersectionCenter(intersect);
            }
            if (setParent)
            {
                intersect.transform.SetParent(intersect.Tile.transform, false);
            }
            return intersect;
        }

        public static bool CanMakeIntersection(List<RoadBase> roadables)
        {
            bool allRoads = roadables.TrueForAll(rb => rb is Road);
            if (!allRoads)
            {
                return false;
            }

            for (int i = 0; i < roadables.Count; i++)
            {
                Road a = (Road)roadables[i];
                for (int j = i + 1; j < roadables.Count; j++)
                {
                    Road b = (Road)roadables[j];
                    if (a.CanIntersect(b))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using DDSim.Settings;

namespace DDSim
{
    public class RoadNeighborBuilder
    {
        private bool debug;

        private string name;
        private Road road;
        private float width;
        private Material material;

        private Vector3 roadBottomLeft, roadBottomRight, roadTopLeft, roadTopRight;

        private bool onLeft;
        private Vector3 bottomLeft, bottomRight, topLeft, topRight;
        private Vector3 center;
        private MeshData md;

        private List<RoadSideObject> roadSideObjects;

        public RoadNeighborBuilder(bool debug = false)
        {
            md = new MeshData();
            this.debug = debug;

            this.roadSideObjects = new List<RoadSideObject>();
        }

        public RoadNeighborBuilder Clone()
        {
            RoadNeighborBuilder clone = (RoadNeighborBuilder)this.MemberwiseClone();
            clone.md = new MeshData();
            clone.roadSideObjects = new List<RoadSideObject>(this.roadSideObjects);
            return clone;
        }

        public RoadNeighborBuilder SetName(string name)
        {
            this.name = name;
            return this;
        }

        public RoadNeighborBuilder SetRoadVertices(Vector3 bottomLeft, Vector3 bottomRight, Vector3 topLeft, Vector3 topRight)
        {
            this.roadBottomLeft = bottomLeft;
            this.roadBottomRight = bottomRight;
            this.roadTopLeft = topLeft;
            this.roadTopRight = topRight;
            return this;
        }

        public RoadNeighborBuilder SetSide(bool onLeft)
        {
            this.onLeft = onLeft;
            return this;
        }

        public RoadNeighborBuilder SetRoad(Road road)
        {
            this.road = road;
            return this;
        }

        public RoadNeighborBuilder SetWidth(float width)
        {
            this.width = width;
            return this;
        }

        public RoadNeighborBuilder SetMaterial(Material material)
        {
            this.material = material;
            return this;
        }

        public RoadNeighborBuilder AddObject(RoadSideObject obj)
        {
            roadSideObjects.Add(obj);
            return this;
        }

        private Vector3 getBase()
        {
            if (this.onLeft)
            {
                return this.bottomRight;
            }
            else
            {
                return this.bottomLeft;
            }
        }

        private Vector3 getTangent()
        {
            if (this.onLeft)
            {
                return this.topRight - this.bottomRight;
            }
            else
            {
                return this.topLeft - this.bottomLeft;
            }
        }

        private Vector3 getNormal()
        {
            if (this.onLeft)
            {
                return Vector3.Normalize(Quaternion.Euler(0, -90, 0) * getTangent());
            }
            else
            {
                return Vector3.Normalize(Quaternion.Euler(0, 90, 0) * getTangent());
            }
            //if (this.onLeft)
            //{
            //    return Vector3.Normalize(this.bottomLeft - this.bottomRight);
            //}
            //else
            //{
            //    return Vector3.Normalize(this.bottomRight - this.bottomLeft);
            //}
        }

        private void updateVertices()
        {
            if (onLeft)
            {
                this.bottomRight = roadBottomLeft;
                this.topRight = roadTopLeft;

                this.bottomLeft = width * Vector3.Normalize(roadBottomLeft - roadBottomRight) + roadBottomLeft;
                this.topLeft = width * Vector3.Normalize(roadTopLeft - roadTopRight) + roadTopLeft;

            }
            else
            {
                this.bottomLeft = roadBottomRight;
                this.topLeft = roadTopRight;

                this.bottomRight = width * Vector3.Normalize(roadBottomRight - roadBottomLeft) + roadBottomRight;
                this.topRight = width * Vector3.Normalize(roadTopRight - roadTopLeft) + roadTopRight;
            }
            this.center = (bottomLeft + bottomRight + topLeft + topRight) / 4f;
            md.Vertices = new List<Vector3> { this.center, this.topLeft, this.topRight, this.bottomLeft, this.bottomRight };
        }

        private void buildMesh(RoadNeighbor rNeighbor)
        {
            updateVertices();

            var length = Vector3.Magnitude(getTangent());
            //if (orientedLeft)
            //{
                //md.UV = new List<Vector2> {
                //    new Vector2 (0.5f, 0.5f*length),
                //    new Vector2 (1f, 0f*length),
                //    new Vector2 (1f, 1f*length),
                //    new Vector2 (0f, 0f*length),
                //    new Vector2 (0f, 1f*length)
                //};
            //}
            //else
            //{
            md.UV = new List<Vector2> {
                new Vector2 (0.5f, 0.5f*length),
                    new Vector2 (0f, 1f*length),
                    new Vector2 (1f, 1f*length),
                    new Vector2 (0f, 0f*length),
                    new Vector2 (1f, 0f*length),
                };
            //}
            Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
            //tangents = new Vector4[] { tangent, tangent, tangent, tangent, tangent };
            md.Indices = new List<int> {
                0, 1, 2,
                0, 2, 4,
                0, 4, 3,
                0, 3, 1
            };

            Mesh mesh = rNeighbor.GetComponent<MeshFilter>().mesh;
            mesh.vertices = md.Vertices.ToArray();
            mesh.triangles = md.Indices.ToArray();
            mesh.SetUVs(0, md.UV);
            mesh.RecalculateNormals();

            rNeighbor.GetComponent<MeshCollider>().sharedMesh = mesh;
            rNeighbor.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            rNeighbor.GetComponent<MeshRenderer>().material = this.material;
        }

        //private bool checkOverlapRoad(GameObject go)
        //{
        //    Bounds bounds = go.GetAllBounds();
        //    Collider[] cols = Physics.OverlapSphere(go.transform.position, bounds.extents.magnitude);
        //    foreach (Collider col in cols.Where(col => col.gameObject.GetComponent<Road>() != null))
        //    {
        //        if (col.gameObject == go)
        //        {
        //            continue;
        //        }
        //        if (bounds.Intersects(col.gameObject.GetComponent<MeshCollider>().bounds))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        private bool checkOverlapRoad(GameObject go)
        {
            Bounds bounds = go.GetAllBounds();
            Collider[] cols = Physics.OverlapSphere(go.transform.position, bounds.extents.magnitude);
            foreach (Collider col in cols)
            {
                Road r = col.gameObject.GetComponent<Road>();
                Intersection intersect = col.gameObject.GetComponent<Intersection>();
                if ((r != null && r.Contains(go.transform)) || (intersect != null && intersect.Contains(go.transform)))
                {
                    return true;
                }
            }
            return false;
        }

        public void constructRoadSideObjects(RoadNeighbor rn)
        {
            foreach (RoadSideObject rso in this.roadSideObjects)
            {
                GameObject go = UnityEngine.Object.Instantiate(rso.Obj, Vector3.zero, Quaternion.identity);
                go.transform.SetParent(rn.transform, false);

                // Position it
                Vector3 baseVec = getBase();
                Vector3 tangentVec = getTangent();
                Vector3 normalVec = getNormal();

                float minDist = (rso.MinDistance >= 0) ? rso.MinDistance : width;
                if (minDist > rso.MaxDistance) continue;

                float x = UnityEngine.Random.Range(minDist, rso.MaxDistance);
                float y = UnityEngine.Random.Range(0f, 1f);
                go.transform.localPosition = baseVec + normalVec * x + tangentVec * y;

                int attempts = 0, maxAttempts = 5;
                while (checkOverlapRoad(go) && attempts < maxAttempts)
                {
                    x = UnityEngine.Random.Range(minDist, rso.MaxDistance);
                    y = UnityEngine.Random.Range(0f, 1f);
                    go.transform.localPosition = baseVec + normalVec * x + tangentVec * y;
                    attempts++;
                }
                if (attempts >= maxAttempts)
                {
                    Debug.Log("Destroying pesky roadside object");
                    UnityEngine.Object.Destroy(go);
                    continue;
                }

                // Rotate it towards the road if needed
                if (rso.FacesRoad)
                {
                    go.transform.rotation = Quaternion.LookRotation(baseVec + tangentVec * y - go.transform.localPosition);
                    go.transform.Rotate(0f, -90f, 0f);
                }

            }
        }

        public RoadNeighbor Build(bool setParent = false)
        {
            RoadNeighbor rNeighbor = new GameObject(this.name).AddComponent<RoadNeighbor>();

            rNeighbor.Tile = this.road.Tile;

            if (setParent)
            {
                rNeighbor.transform.SetParent(this.road.transform, false);
            }
            buildMesh(rNeighbor);
            constructRoadSideObjects(rNeighbor);


            rNeighbor.transform.localPosition -= Vector3.up * 0.01f;

            return rNeighbor;
        }
    }
}

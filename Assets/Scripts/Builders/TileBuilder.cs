﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System.Linq;
using UniRx;
using DDSim.Helpers;


namespace DDSim {
    public class TileBuilder
    {

        //private string _url;
        private List<BaseFactory> _factories;
        private Pair<int> _tms;
        private int _zoom;
        private Pair<int> _originTMS;

        public TileBuilder SetOriginTMS(Pair<int> originTMS)
        {
            _originTMS = originTMS;
            return this;
        }

        public TileBuilder SetFactories(List<BaseFactory> factories)
        {
            _factories = factories;
            return this;
        }

        public TileBuilder SetTMS(Pair<int> tms)
        {
            _tms = tms;
            return this;
        }

        public TileBuilder SetZoom(int zoom)
        {
            _zoom = zoom;
            return this;
        }

        public Tile Build()
        {
            Debug.Log("Creating tile: " + _tms);
            RectD rect = CoordinateConverter.TMSBoundsInMercator(_tms, _zoom);
            Tile tile = new GameObject("tile_" + _tms.First + "_" + _tms.Second).AddComponent<Tile>();

            tile.Tms = _tms;
            tile.TileCenter = new Vector3d(rect.center, true);
            tile.Rect = rect;

            World world = World.GetInstance();
            Vector2 p = CoordinateConverter.TMS2MercatorOffset(_tms, _originTMS, _zoom);
            tile.transform.position = new Vector3(p.x, 0f, p.y);
            tile.transform.SetParent(world.transform, false);
            LoadMapzenData(tile);

            return tile;
        }

        public void LoadMapzenData(Tile tile)
        {
            World world = World.GetInstance();
            string url = world.GetMapzenURL(tile.Tms, World.Zoom);
            string tilePath = world.GetTileCachePath(tile.Tms);

            if (File.Exists(tilePath))
            {
                using (var r = new StreamReader(tilePath, Encoding.Default))
                {
                    string data = r.ReadToEnd();
                    InitializeTile(data, tile);
                }
            }
            else
            {
                ObservableWWW.Get(url).Subscribe(
                    data =>
                    {
                        var sr = File.CreateText(tilePath);
                        sr.Write(data);
                        sr.Close();
                        InitializeTile(data, tile);
                    },
                    error =>
                    {
                        Debug.LogError(error);
                    });
            }
        }

        protected void InitializeTile(string text, Tile tile)
        {
            var heavyMethod = Observable.Start(() => new JSONObject(text));

            heavyMethod.ObserveOnMainThread().Subscribe(data =>
            {
                if (!tile) // checks if tile still exists and haven't destroyed yet
                    return;
                tile.Data = data;

                foreach (var factory in _factories)
                {
                    factory.Process(tile);
                    factory.PostProcess(tile);
                }
            });
        }

    }
}

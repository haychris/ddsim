﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using DDSim.Settings;

namespace DDSim
{
    public class IntersectionBuilderOld
    {
        public string name; // gameobject name
        private List<Road> roads;
        private Vector3d v; // position, in mercator
        private Material material;
        private List<Vector3> intersectionPoints;

        private GameObject stopObjectPrefab;
        private int stopObjPosition; //TODO: make enum

        bool fake = false; // TODO: FIX, HACKS
        bool mixedIntersection;
        bool debug;

        // Builder properties
        bool buildEagerly;

        public IntersectionBuilderOld(bool buildEagerly = false, bool debug = false)
        {
            roads = new List<Road>();
            this.buildEagerly = buildEagerly;
            this.debug = debug;
        }

        // Creates a new IntersectionBuilderOld and copies each instance variable for this builder
        public IntersectionBuilderOld Clone()
        {
            IntersectionBuilderOld clone = (IntersectionBuilderOld)this.MemberwiseClone();
            clone.roads = new List<Road>();
            clone.AddRoads(roads);
            return clone;
        }

        public IntersectionBuilderOld SetName(string name)
        {
            this.name = name;
            return this;
        }

        public IntersectionBuilderOld SetPoint(Vector3d v)
        {
            this.v = v;
            return this;
        }

        public IntersectionBuilderOld AddRoad(Road r)
        {
            this.roads.Add(r);
            return this;
        }

        public IntersectionBuilderOld AddRoads(List<Road> rs)
        {
            rs.ForEach(r => this.AddRoad(r));
            return this;
        }

        public IntersectionBuilderOld SetMaterial(Material material)
        {
            this.material = material;
            return this;
        }

        public IntersectionBuilderOld MakeFake()
        {
            this.fake = true;
            return this;
        }

        public IntersectionBuilderOld AddStopObject(ResourceAndPositioning stopObjectPrefab)
        {
            this.stopObjectPrefab = stopObjectPrefab.Obj;

            // TODO: move to ResourceAndPositioning
            float rand = UnityEngine.Random.Range(0f, 1f);
            float[] probs = new float[]{
                stopObjectPrefab.ProbFrontLeft,
                stopObjectPrefab.ProbFrontRight,
                stopObjectPrefab.ProbBackLeft,
                stopObjectPrefab.ProbBackRight,
            };


            float cumProb = 0f;
            int i;
            for (i = 0; cumProb < rand; i++)
            {
                cumProb += probs[i];
            }
            this.stopObjPosition = i - 1;
            return this;
        }

        private Material getMaterial()
        {
            if (this.material != null) return this.material;

            return this.roads[0].GetComponent<MeshRenderer>().material;
        }

        private static void visualizeIntersectionCenter(Intersection intersect)
        {
            GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(intersect.transform);
            cube.transform.localPosition =  Vector3.up * 0.5f;
            cube.transform.localScale *= 4f;
        }

        private bool intersectsAtHd(Road r)
        {
            double distHd = (r.ToMercator(r.Hd()) - v).sqrMagnitude;
            double distTl = (r.ToMercator(r.Tl()) - v).sqrMagnitude;
            bool atHd = distHd < distTl;
            return atHd;
        }

        private Pair<Road> getBestThroughway()
        {
            // TODO: replace with roads[0].IsConnected(roads[j]), and choose pair with max width ???
            // currently picks the pair of roads with the smallest angle between them (filtering pairs w/differing widths)
            Pair<Road> best = new Pair<Road>(roads[0], roads[1]);
            float minAngle = 90f;
            for (int i = 0; i < roads.Count; i++)
            {
                Vector3 a;
                if (intersectsAtHd(roads[i]))
                {
                    a = roads[i].RoadPoints[1] - roads[i].Hd();
                }
                else
                {
                    a = roads[i].RoadPoints[roads[i].RoadPoints.Length - 2] - roads[i].Tl();
                }
                for (int j = i + 1; j < roads.Count; j++)
                {
                    if (roads[i].Width != roads[j].Width) continue;
                    Vector3 b;
                    if (intersectsAtHd(roads[j]))
                    {
                        b = roads[j].RoadPoints[1] - roads[j].Hd();
                    }
                    else
                    {
                        b = roads[j].RoadPoints[roads[j].RoadPoints.Length - 2] - roads[j].Tl();
                    }
                    float angle = Vector3.Angle(a, b);
                    float angle2 = Mathf.Min(angle, 180f - angle);
                    if (angle2 < minAngle)
                    {
                        minAngle = angle2;
                        best = new Pair<Road>(roads[i], roads[j]);
                    }
                }
            }

            return best;
        }

        private List<Vector3> calculateEndpoints()
        {
            Pair<Road> best = getBestThroughway();

            //Vector3 p = getLocalCenter();
            float maxHalfWidth = roads.Select(r => r.Width).Max() / 2f;
            List<Vector3> verts = new List<Vector3>();
            Vector3[] rVerts = best.First.GetComponent<MeshFilter>().mesh.vertices;
            if (intersectsAtHd(best.First))
            {
                Vector3 offset = (best.First.RoadPoints[1] - best.First.RoadPoints[0]).normalized * maxHalfWidth;
                verts.Add(rVerts[0] + offset);
                verts.Add(rVerts[1] + offset);
            }
            else
            {
                Vector3 offset = (best.First.RoadPoints[best.First.RoadPoints.Length - 2] - best.First.RoadPoints[best.First.RoadPoints.Length - 1]).normalized * maxHalfWidth;
                verts.Add(rVerts[rVerts.Length - 1] + offset);
                verts.Add(rVerts[rVerts.Length - 2] + offset);
            }

            Vector3[] rVerts2 = best.Second.GetComponent<MeshFilter>().mesh.vertices;
            if (intersectsAtHd(best.Second))
            {
                Vector3 offset = (best.Second.RoadPoints[1] - best.Second.RoadPoints[0]).normalized * maxHalfWidth;
                verts.Add(rVerts2[0] + offset);
                verts.Add(rVerts2[1] + offset);
            }
            else
            {
                Vector3 offset = (best.Second.RoadPoints[best.Second.RoadPoints.Length - 2] - best.Second.RoadPoints[best.Second.RoadPoints.Length - 1]).normalized * maxHalfWidth;
                verts.Add(rVerts2[rVerts2.Length - 1] + offset);
                verts.Add(rVerts2[rVerts2.Length - 2] + offset);
            }

            return verts;
        }


        // MOVED TO Intersection.cs since roads can be added to the intersection after the initial construction
        // Move every road back by half the max width 
        // in order to clear space for the intersection
        //private void clearSpace(List<Vector3> endpoints)
        //{
            //float maxWidth = roads.Select(r => r.Width).Max();
            //foreach (Road r in roads)
            //{
            //    double distHd = (r.ToMercator(r.Hd()) - v).sqrMagnitude;
            //    double distTl = (r.ToMercator(r.Tl()) - v).sqrMagnitude;
            //    bool atHd = distHd < distTl;
            //    r.MoveEndpointBack(maxWidth / 2.0f, atHd);
            //}

            // Now done in Intersection.cs...
            //foreach (Road r in roads)
            //{
            //    r.MoveForIntersection(endpoints, intersectsAtHd(r));
            //}
        //}

        // TODO: get tile offset and translate points
        private List<Vector3> getEndpointVertices()
        {
            //Vector3 p = getLocalCenter();
            List<Vector3> verts = new List<Vector3>();
            foreach (Road r in roads)
            {
                double distHd = (r.ToMercator(r.Hd()) - v).sqrMagnitude;
                double distTl = (r.ToMercator(r.Tl()) - v).sqrMagnitude;
                bool atHd = distHd < distTl;
                Vector3[] rVerts = r.GetComponent<MeshFilter>().mesh.vertices;
                if (atHd)
                {
                    verts.Add(rVerts[0]);
                    verts.Add(rVerts[1]);
                }
                else
                {
                    verts.Add(rVerts[rVerts.Length - 1]);
                    verts.Add(rVerts[rVerts.Length - 2]);
                }
            }

            return verts;
        }

        private List<Vector2> convert2Vector2(List<Vector3> ps)
        {
            return ps.Select(p => new Vector2(p.x, p.z)).ToList();
        }

        private MeshData makeMeshData(List<Vector3> endpoints)
        {
            MeshData md = new MeshData();
            //List<Vector3> endpoints = getEndpointVertices();

            // Use the triangulator to get indices for creating triangles
            Triangulator tr = new Triangulator(convert2Vector2(endpoints).ToArray());
            md.Indices = tr.Triangulate().ToList();
            Vector3 p = getLocalCenter();
            md.Vertices = endpoints.Select(p1 => p1 - p).ToList();

            //md.Vertices.Add(getLocalCenter());
            //for (int i = 0; i < endpoints.Count; i += 2)
            //{
            //    md.Vertices.Add(endpoints[i]);
            //    md.Vertices.Add(endpoints[i + 1]);

            //    md.Indices.Add(md.Vertices.Count - 1);
            //    md.Indices.Add(md.Vertices.Count - 2);

            //    md.Indices.Add(0);
            //}

            return md;
        }

        private void updateIntersectionPoints()
        {
            this.intersectionPoints = calculateEndpoints();
        }

        private void buildMesh(Intersection intersect)
        {
            //clearSpace(this.intersectionPoints);

            MeshData md = makeMeshData(this.intersectionPoints);
            Mesh mesh = intersect.GetComponent<MeshFilter>().mesh;
            mesh.vertices = md.Vertices.ToArray();
            //md.Indices.Reverse();
            mesh.triangles = md.Indices.ToArray();
            //mesh.SetUVs(0, md.UV);
            mesh.RecalculateNormals();

            intersect.GetComponent<MeshRenderer>().material = getMaterial();
        }

        private Tile getTile()
        {
            return this.roads[0].Tile;
        }

        private Vector3 getLocalCenter()
        {
            return this.roads[0].FromMercator(v);
        }

        public void buildStopObjects(Intersection intersect)
        {
            if (this.stopObjectPrefab == null) return;

            foreach (Road r in this.roads)
            {
                GameObject go = UnityEngine.Object.Instantiate(this.stopObjectPrefab, Vector3.zero, Quaternion.identity);
                if (go.GetComponent<StopObject>() == null)
                {
                    go.AddComponent<StopObject>();
                }
                StopObject stopObj = go.GetComponent<StopObject>();
                stopObj.transform.SetParent(intersect.transform, false);

                Vector3 left, right;
                Vector3[] rVerts = r.GetComponent<MeshFilter>().mesh.vertices;
                if (intersectsAtHd(r))
                {
                    left = rVerts[1];
                    right = rVerts[0];
                }
                else
                {
                    left = rVerts[rVerts.Length - 2];
                    right = rVerts[rVerts.Length - 1];
                }

                Vector3 middle = Vector3.Lerp(left, right, 0.5f);

                //Vector3 target = middle + r.transform.position;
                Vector3 target = middle;
                //Debug.Log("Looking at " + target + " from " + stopObj.transform.position, stopObj);
                stopObj.transform.LookAt(target);

                float offsetMultiplier = UnityEngine.Random.Range(1.1f, 1.7f);
                switch (this.stopObjPosition)
                {
                    case 0: // front left
                        stopObj.transform.localPosition += offsetMultiplier * (left - getLocalCenter());
                        break;
                    case 1: // front right
                        stopObj.transform.localPosition += offsetMultiplier * (right - getLocalCenter());
                        break;
                    case 2: // back left
                        stopObj.transform.localPosition += offsetMultiplier * (getLocalCenter() - right);
                        break;
                    case 3: // back right
                        stopObj.transform.localPosition += offsetMultiplier * (getLocalCenter() - left);
                        break;
                }
            }

            //foreach (Vector3 p in this.intersectionPoints)
            //{
            //    Vector3 loc = 1.5f * (p - getLocalCenter());
            //    GameObject go = UnityEngine.Object.Instantiate(this.stopObjectPrefab, loc, Quaternion.identity);
            //    if (go.GetComponent<StopObject>() == null)
            //    {
            //        go.AddComponent<StopObject>();
            //    }
            //    StopObject stopObj = go.GetComponent<StopObject>();
            //    stopObj.transform.SetParent(intersect.transform, false);
            //}
        }


        // TODO: differentiate between Fake intersection and mix-intersections
        // TODO: clear space of smaller width roads in mix-intersections, currently causing path bugs
        public Intersection Build(bool setParent = false)
        {
            updateIntersectionPoints();
            if (this.roads.Select(r => r.Width).Distinct().Count() > 1)
            {
                //this.fake = true;
                this.mixedIntersection = true;
            }

            Intersection intersect = new GameObject(this.name).AddComponent<Intersection>();

            intersect.v = this.v;
            intersect.Tile = getTile();
            intersect.transform.localPosition = getLocalCenter();
            intersect.Fake = this.fake;

            if (!this.fake)
            {
                buildMesh(intersect);
            }
            intersect.IntersectionPoints = this.intersectionPoints;
            intersect.AddRoads(this.roads);

            if (!this.fake)
            {
                buildStopObjects(intersect);
            }

            if (this.debug)
            {
                visualizeIntersectionCenter(intersect);
            }
            if (setParent)
            {
                intersect.transform.SetParent(intersect.Tile.transform, false);
            }
            return intersect;
        }

        //public static bool CanMakeIntersection(List<RoadBase> roadbases, Road newRoad)
        //{
        //    bool allRoads = roadbases.TrueForAll(rb => rb is Road);
        //    if (!allRoads)
        //    {
        //        return false;
        //    }
        //    bool canIntersect = roadbases.Exists(rb => newRoad.CanIntersect((Road)rb));

        //    return canIntersect;
        //}

        public static bool CanMakeIntersection(List<RoadBase> roadables)
        {
            bool allRoads = roadables.TrueForAll(rb => rb is Road);
            if (!allRoads)
            {
                return false;
            }

            for (int i = 0; i < roadables.Count; i++)
            {
                Road a = (Road)roadables[i];
                for (int j = i + 1; j < roadables.Count; j++)
                {
                    Road b = (Road)roadables[j];
                    if (a.CanIntersect(b))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using DDSim.Settings.Enums;
using TriangleNet;
using TriangleNet.Geometry;

namespace DDSim
{
    public class BuildingBuilder
    {
        private string name;
        private BuildingType type;
        private string description;
        private string mapzenId;
        private string kind;
        private float sortRank;
        private string buildingName;
        private List<Vector3> buildingCorners;
        private Tile tile;
        private float height;
        private float minHeight;
        private Vector2 minP;
        private bool isVolumetric;

        // Mesh properties
        private Material material;
        private MeshData md;
        private bool useTriangulationNet;

        // Builder properties
        private bool debug;

        public BuildingBuilder(bool debug = false)
        {
            buildingCorners = new List<Vector3>();
            md = new MeshData();
            this.debug = debug;
        }

        public BuildingBuilder SetName(string name)
        {
            this.name = name;
            return this;
        }

        public BuildingBuilder SetType(BuildingType type)
        {
            this.type = type;
            return this;
        }

        public BuildingBuilder SetMapzenID(string mapzenID)
        {
            this.mapzenId = mapzenID;
            return this;
        }

        public BuildingBuilder UseTriangulationNet()
        {
            this.useTriangulationNet = true;
            return this;
        }

        public BuildingBuilder SetIsVolumetric(bool isVolumetric)
        {
            this.isVolumetric = isVolumetric;
            return this;
        }

        public BuildingBuilder SetProperties(JSONObject properties)
        {
            this.SetMapzenID(properties["id"].ToString());
            this.kind = properties["kind"].ToString();
            this.sortRank = properties["sort_rank"].f;
            if (properties.HasField("name"))
            {
                this.buildingName = properties["name"].str;
            }
            return this;
        }

        public BuildingBuilder SetDescription(string description)
        {
            this.description = description;
            return this;
        }

        public BuildingBuilder SetHeight(float height)
        {
            this.height = height;
            return this;
        }

        public BuildingBuilder SetMinHeight(float minHeight)
        {
            this.minHeight = minHeight;
            return this;
        }

        public BuildingBuilder AddBuildingCorner(Vector3 p)
        {
            this.buildingCorners.Add(p);

            if (p.x < minP.x) minP.x = p.x;
            if (p.y < minP.y) minP.y = p.y;
            //if (p.x > maxx) maxx = p.x;
            //if (p.y > maxy) maxy = p.y;

            return this;
        }

        public BuildingBuilder SetTile(Tile tile)
        {
            this.tile = tile;
            return this;
        }

        public BuildingBuilder SetMaterial(Material material)
        {
            this.material = material;
            return this;
        }

        private Vector3 changeToRelativePositions(List<Vector3> buildingCorners)
        {
            Vector3 buildingCenter = this.buildingCorners.Aggregate((acc, cur) => acc + cur) / buildingCorners.Count;
            for (int i = 0; i < this.buildingCorners.Count; i++)
            {
                //using corner position relative to building center
                this.buildingCorners[i] = this.buildingCorners[i] - buildingCenter;
            }
            return buildingCenter;
        }

        private static int buildRoofClass(List<Vector3> corners, float height, MeshData data)
        {
            var vertsStartCount = data.Vertices.Count;
            var tris = new Triangulator(corners.Select(p => new Vector2(p.x, p.z)).ToArray()); // TODO: make more efficient
            data.Vertices.AddRange(corners.Select(x => new Vector3(x.x, height, x.z)).ToList());
            data.Indices.AddRange(tris.Triangulate().Select(x => vertsStartCount + x));
            return vertsStartCount;
        }

        private int buildRoofTriangulation(List<Vector3> corners, float height, MeshData data)
        {
            TriangleNet.Mesh mesh = new TriangleNet.Mesh();
            var inp = new InputGeometry(corners.Count);
            for (int i = 0; i < corners.Count; i++)
            {
                var v = corners[i];
                inp.AddPoint(v.x, v.z);
                inp.AddSegment(i, (i + 1) % corners.Count);
            }
            mesh.Behavior.Algorithm = TriangulationAlgorithm.SweepLine;
            mesh.Behavior.Quality = true;
            mesh.Triangulate(inp);

            var vertsStartCount = data.Vertices.Count;
            data.Vertices.AddRange(corners.Select(x => new Vector3(x.x, height, x.z)).ToList());

            foreach (var tri in mesh.Triangles)
            {
                data.Indices.Add(vertsStartCount + tri.P1);
                data.Indices.Add(vertsStartCount + tri.P0);
                data.Indices.Add(vertsStartCount + tri.P2);
            }
            return vertsStartCount;
        }

        private void buildMesh(Building building)
        {
            var vertsStartCount = useTriangulationNet
                    ? buildRoofTriangulation(this.buildingCorners, this.height, md)
                    : buildRoofClass(this.buildingCorners, this.height, md);


            foreach (var c in this.buildingCorners)
            {
                md.UV.Add(new Vector2((c.x - this.minP.x), (c.z - this.minP.y)));
            }

            if (this.isVolumetric)
            {
                float d = 0f;
                Vector3 v1;
                Vector3 v2;
                int ind = 0;
                for (int i = 1; i < this.buildingCorners.Count; i++)
                {
                    v1 = md.Vertices[vertsStartCount + i - 1];
                    v2 = md.Vertices[vertsStartCount + i];
                    ind = md.Vertices.Count;
                    md.Vertices.Add(v1);
                    md.Vertices.Add(v2);
                    md.Vertices.Add(new Vector3(v1.x, this.minHeight, v1.z));
                    md.Vertices.Add(new Vector3(v2.x, this.minHeight, v2.z));

                    d = (v2 - v1).magnitude;

                    md.UV.Add(new Vector2(0, 0));
                    md.UV.Add(new Vector2(d, 0));
                    md.UV.Add(new Vector2(0, this.height));
                    md.UV.Add(new Vector2(d, this.height));

                    md.Indices.Add(ind);
                    md.Indices.Add(ind + 2);
                    md.Indices.Add(ind + 1);

                    md.Indices.Add(ind + 1);
                    md.Indices.Add(ind + 2);
                    md.Indices.Add(ind + 3);
                }

                v1 = md.Vertices[vertsStartCount];
                v2 = md.Vertices[vertsStartCount + this.buildingCorners.Count - 1];
                ind = md.Vertices.Count;
                md.Vertices.Add(v1);
                md.Vertices.Add(v2);
                md.Vertices.Add(new Vector3(v1.x, this.minHeight, v1.z));
                md.Vertices.Add(new Vector3(v2.x, this.minHeight, v2.z));

                d = (v2 - v1).magnitude;

                md.UV.Add(new Vector2(0, 0));
                md.UV.Add(new Vector2(d, 0));
                md.UV.Add(new Vector2(0, this.height));
                md.UV.Add(new Vector2(d, this.height));

                md.Indices.Add(ind);
                md.Indices.Add(ind + 1);
                md.Indices.Add(ind + 2);

                md.Indices.Add(ind + 1);
                md.Indices.Add(ind + 3);
                md.Indices.Add(ind + 2);
            }

            UnityEngine.Mesh mesh = building.GetComponent<MeshFilter>().mesh;
            building.GetComponent<MeshRenderer>().material = this.material;

            mesh.vertices = md.Vertices.ToArray();
            mesh.triangles = md.Indices.ToArray();
            mesh.SetUVs(0, md.UV);
            mesh.RecalculateNormals();
        }

        public Building Build(bool setParent = false)
        {
            Building building = new GameObject(this.name).AddComponent<Building>();
            building.Type = this.type;
            building.Tile = this.tile;
            building.MapzenID = this.mapzenId;
            building.Description = this.description;
            building.BuildingName = this.buildingName;
            building.Height = this.height;

            var buildingCenter = changeToRelativePositions(this.buildingCorners);
            building.transform.localPosition = buildingCenter;
            building.transform.localPosition += Vector3.up * (float)this.sortRank / 10000f;


            buildMesh(building);

            if (setParent)
            {
                building.transform.SetParent(this.tile.transform, false);
            }

            return building;
        }
    }
}

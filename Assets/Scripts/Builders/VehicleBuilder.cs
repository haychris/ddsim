﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;

namespace DDSim
{
    public class VehicleBuilder
    {
        private string name;
        private bool debug;
        private bool recording = false;
        private Vector3 startingPoint;
        private Vector3 lookTowardsPoint;
        private string sensorDataRelativePath;
        private string sensorDataPath;
        private double sessionNumber = -1;

        private float percentToRecord;

        private GameObject cloneBase;

        public VehicleBuilder SetName(string name)
        {
            this.name = name;
            return this;
        }

        public VehicleBuilder SetDebugMode(bool debugMode)
        {
            this.debug = debugMode;
            return this;
        }

        public VehicleBuilder MakeRecordSensors()
        {
            this.recording = true;
            return this;
        }

        public VehicleBuilder SetSensorRecordingPath(string filepath)
        {
            this.sensorDataRelativePath = filepath;
            return this;
        }

        public VehicleBuilder SetPercentFramesToRecord(float percent)
        {
            this.percentToRecord = percent;
            return this;
        }

        public VehicleBuilder SetPoint(Vector3 p)
        {
            this.startingPoint = p;
            return this;
        }

        public VehicleBuilder LookTowards(Vector3 p2)
        {
            this.lookTowardsPoint = p2;
            return this;
        }

        public VehicleBuilder SetSessionNumber(double session)
        {
            this.sessionNumber = session;
            return this;
        }

        // TODO: make work
        public VehicleBuilder SetPrefab(GameObject cloneBase)
        {
            this.cloneBase = cloneBase;
            return this;
        }

        private double timeNow()
        {
            return DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;
        }

        private void createDataPathIfNecessary()
        {
#if UNITY_ANDROID || UNITY_IPHONE
		    this.sensorDataPath = Path.Combine(Application.persistentDataPath, this.sensorDataRelativePath);
#else
            this.sensorDataPath = Path.Combine(Application.dataPath, this.sensorDataRelativePath);
#endif
            object[] formatters = { this.name, this.sessionNumber };
            this.sensorDataPath = string.Format(this.sensorDataPath, formatters);
            if (!Directory.Exists(this.sensorDataPath))
            {
                Directory.CreateDirectory(this.sensorDataPath);
            }
        }

        private Transform addHelperWaypoint(Vehicle vehicle)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(vehicle.transform, false);
            go.transform.localPosition = Vector3.forward * 8.4f;
            return go.transform;
        }

        private void addAIControl(Vehicle vehicle, Transform target)
        {
            DDSimCarAIControl aiController = vehicle.gameObject.AddComponent<DDSimCarAIControl>();
            aiController.SetTarget(target);
        }

        private void addProgressTracker(Vehicle vehicle, Transform target)
        {
            DDSimWaypointProgressTracker progressTracker = vehicle.gameObject.AddComponent<DDSimWaypointProgressTracker>();
            progressTracker.target = target;
        }

        public Vehicle Build()
        {
            if (this.sessionNumber < 0)
            {
                this.sessionNumber = timeNow();
            }
            if (this.recording)
            {
                createDataPathIfNecessary();
            }

            GameObject go = UnityEngine.Object.Instantiate(this.cloneBase, this.startingPoint, Quaternion.identity);
            if (go.GetComponent<Vehicle>() == null)
            {
                go.AddComponent<Vehicle>();
            }
            Vehicle vehicle = go.GetComponent<Vehicle>();
            if (vehicle.GetComponent<DDSimCarAIControl>() == null)
            {
                Transform waypoint = addHelperWaypoint(vehicle);
                addAIControl(vehicle, waypoint);
                addProgressTracker(vehicle, waypoint);
            }

            vehicle.name = this.name;
            vehicle.DebugMode = this.debug;
            vehicle.Recording = this.recording;
            vehicle.SensorDataPath = this.sensorDataPath;
            vehicle.NumFramesPerRecording = (this.percentToRecord <= 0) ? 1 : (int) Mathf.Round(1f / this.percentToRecord);

            //SphereCollider roadDetector = vehicle.gameObject.AddComponent<SphereCollider>();
            //roadDetector.center = Vector3.zero;
            //roadDetector.radius = 2f;

            //DDSimWaypointCircuit circuit = new GameObject("waypoint circuit").AddComponent<DDSimWaypointCircuit>();
            WaypointRoute route = new GameObject("waypoint route").AddComponent<WaypointRoute>();
            route.SmoothRoute = false; // TODO: make option
            route.transform.SetParent(vehicle.transform);
            vehicle.GetComponent<DDSimWaypointProgressTracker>().route = route;
            //vehicle.RoadNetwork = this.roadNetwork; // TODO: should we give it direct access? or use a fleet manager?
            FleetManager fm = GameObject.FindObjectOfType<FleetManager>();
            fm.AddVehicle(vehicle);


            vehicle.transform.SetParent(fm.transform);
            vehicle.transform.localPosition += Vector3.up;
            Vector3 newDir = Vector3.RotateTowards(vehicle.transform.forward, lookTowardsPoint - startingPoint, float.MaxValue, 0.0F);
            vehicle.transform.rotation = Quaternion.LookRotation(newDir);
            return vehicle;
        }
    }
}
